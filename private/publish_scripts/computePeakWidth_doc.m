%% computePeakWidth
% computePeakWidth is a function to measure peak width based on horizontal
% intersection at a given value but restricted by local minima
%
%% Syntax
%   idx = computePeakWidth(y, loc, value, prom, WidthOptions)
%   [idx, base] = computePeakWidth(y, loc, value, prom, WidthOptions)
%
%% Description
% [idx, base] = computePeakWidth(y, loc, value, prom, WidthOptions)
% detects the width of a peak by finding the intersection of the curve
% with a horizontal line at a specific value. The width can then be
% further restricted in case of strong local minima between intersection
% and peak.
%
%% Example
% Create example signal of two overlapping sin waves
%
x = linspace(0,2*pi,100);
y = sin(x*2) + sin(x*4);

%%%
% Set width parameters so that the width is measured at 25% of the
% corrected height.
%
widthOpt.reference = 'heightCorr';
widthOpt.refLevel = 0.75;
widthOpt.significantLocalDepth = 0.2;
widthOpt.significantMaxHeight = 0.1;

%%%
% A peak is at index 58 with a prominence of 3.5145 (use findoeaks to
% detect peaks)
%
loc = 58;
prom = 3.5145;
val = y(loc);
[idx, base] = computePeakWidth(y, loc, val, prom, widthOpt)

%%%
% Plot the results and mark the detected peak and its width
%
plot(x,y);
hold on;
plot(x(loc), val, 'ro');
plot(x(idx), repmat(rowVect(val - base.*widthOpt.refLevel), [2,1]));
legend('y', 'peak values', 'width restricted by local minima', 'Location', 'best');

%% Input
%
% <html>
% <table>
%   <tr>
%     <th>y -- input signal</th>
%   </tr>
%   <tr>
%     <td>double vector</td>
%   </tr>
%   <tr>
%     <td>y is the vector for which peaks should and measured.</td>
%   </tr>
% </table>
% </html>
%
%%
% <html>
% <table>
%   <tr>
%     <th>loc -- peak index</th>
%   </tr>
%   <tr>
%     <td>scalar positive integer</td>
%   </tr>
%   <tr>
%     <td>loc is the scalar location index of the peak</td>
%   </tr>
% </table>
% </html>
%
%%
% <html>
% <table>
%   <tr>
%     <th>value -- peak value</th>
%   </tr>
%   <tr>
%     <td>double scalar</td>
%   </tr>
%   <tr>
%     <td>value is the scalar peak value</td>
%   </tr>
% </table>
% </html>
%
%%
% <html>
% <table>
%   <tr>
%     <th>prom -- peak prominence</th>
%   </tr>
%   <tr>
%     <td>double scalar</td>
%   </tr>
%   <tr>
%     <td>prom is the scalar prominence of the peak</td>
%   </tr>
% </table>
% </html>
%
%%
% <html>
% <table>
%   <tr>
%     <th>widthOptions -- width measurement parameter struct</th>
%   </tr>
%   <tr>
%     <td>struct</td>
%   </tr>
%   <tr>
%     <td>widthOptions is structure with the following fields:
%       <ul>
%         <li>reference: a string identifying the type of reference for the
%           width measurement, possible values are height, prominence, or
%           heightCorr for the height above the minimum of y.</li>
%         <li>refLevel: a scalar indicating the height level of the
%           reference for which the width is measured, from the top
%           position downward. Example: the reference type is height and
%           the detected peak has a height of 100. A reference level of 0.5
%           would measure the width at half height or 50, a reference level
%           of 0.75 would measure the width at quarter height or 25.</li>
%         <li>significantLocalDepth: the width measurement is restricted by
%           strong local minima. This scalar corresponds to the depth, i.e.
%           the difference in value between the minima and its smallest
%           neighboring maxima. Minima shallower than this value are not
%           restricting the width measurement.</li>
%         <li>significantMaxHeight: a scalar indicating the minimum height
%           difference for a neighboring local maxima to properly define
%           depth in noisy conditions. Must be smaller than
%           significantLocalDepth.</li>
%       </ul>
%     </td>
%   </tr>
% </table>
% </html>
%
%% Ouput
%
% <html>
% <table>
%   <tr>
%     <th>idx -- left and right starting index of peaks</th>
%   </tr>
%   <tr>
%     <td>2-element vector</td>
%   </tr>
%   <tr>
%     <td>idx is 1x2 vector with the left and right start of the width
%       measurement</td>
%   </tr>
% </table>
% </html>
%
%%
% <html>
% <table>
%   <tr>
%     <th>base -- reference value</th>
%   </tr>
%   <tr>
%     <td>double scalar</td>
%   </tr>
%   <tr>
%     <td>base is the computed reference value of the peak</td>
%   </tr>
% </table>
% </html>
%
%% See Also
% <findpeaksWrapper.html findpeaksWrapper>, <matlab:doc('findpeaks')
% findpeaks>

%%
% Copyright (C) David Dreher 2017
