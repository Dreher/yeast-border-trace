%% findpeaksWrapper
% FINDPEAKSWRAPPER wrapper for the built-in Matlab function findpeaks
% extended with a custom width measurement routine.
%
%% Syntax
%   maxVal = findpeaksWrapper( y, finderOptions, widthOptions )
%   [maxVal, maxLoc] = findpeaksWrapper(___)
%   [maxVal, maxLoc, maxWidth] = findpeaksWrapper(___)
%   [maxVal, maxLoc, maxWidth, maxBase] = findpeaksWrapper(___)
%
%% Description
% [ maxVal, maxLoc, maxWidth, maxBase ] = findpeaksWrapper( y,
% finderOptions, widthOptions ) identifies peaks in the data using
% findpeaks with any of its parameters and measures their width the
% custom function computePeakWidth. Width measurements are based around
% the perecentage of a reference level (height, prominence) while being
% restricted by strong local minima. 
% This function also has a GUI, see findpeaksWrapperGUI for more
% information.
%
%% Example
% Create example signal of two overlapping sin waves
%
x = linspace(0,2*pi,100);
y = sin(x*2) + sin(x*4);

%%
% Set parameters to detect only the larger peaks
%
findOpt.MinPeakHeight = 0.5;

%%
% Set width parameters so that the width is measured at 25% of the
% corrected height and detect the peaks
%
widthOpt.reference = 'heightCorr';
widthOpt.refLevel = 0.75;
widthOpt.significantLocalDepth = 0.2;
widthOpt.significantMaxHeight = 0.1;
[val, loc, width, base] = findpeaksWrapper(y, findOpt, widthOpt );

%%
% Plot the results and mark the detected peaks and their width
%
plot(x,y);
hold on;
plot(x(loc), val, 'ro');
plot(x(width'), repmat(rowVect(val - base.*widthOpt.refLevel), [2,1]));
legend('y', 'peak values', 'width restricted by local minima', 'Location', 'best');

%% Input
%
% <html>
% <table>
%   <tr>
%     <th>y -- input signal</th>
%   </tr>
%   <tr>
%     <td>double vector</td>
%   </tr>
%   <tr>
%     <td>y is the vector for which peaks should be detected and measured.</td>
%   </tr>
% </table>
% </html>
%
%%
% <html>
% <table>
%   <tr>
%     <th>finderOptions -- findpeaks parameter struct or cell</th>
%   </tr>
%   <tr>
%     <td>struct | cell</td>
%   </tr>
%   <tr>
%     <td>finderOptions is a cell array with the findpeaks parameter
%       name-value pairs or a structure where fieldnames correspond to
%       parameter names and entries to parameter values. All possible
%       findpeaks parameter are available and can be added or omitted at
%       will.</td>
%   </tr>
% </table>
% </html>
%
%%
% <html>
% <table>
%   <tr>
%     <th>widthOptions -- width measurement parameter struct</th>
%   </tr>
%   <tr>
%     <td>struct</td>
%   </tr>
%   <tr>
%     <td>widthOptions is structure with the following fields:
%       <ul>
%         <li>reference: a string identifying the type of reference for the
%           width measurement, possible values are height, prominence, or
%           heightCorr for the height above the minimum of y.</li>
%         <li>refLevel: a scalar indicating the height level of the
%           reference for which the width is measured, from the top
%           position downward. Example: the reference type is height and
%           the detected peak has a height of 100. A reference level of 0.5
%           would measure the width at half height or 50, a reference level
%           of 0.75 would measure the width at quarter height or 25.</li>
%         <li>significantLocalDepth: the width measurement is restricted by
%           strong local minima. This scalar corresponds to the depth, i.e.
%           the difference in value between the minima and its smallest
%           neighboring maxima. Minima shallower than this value are not
%           restricting the width measurement.</li>
%         <li>significantMaxHeight: a scalar indicating the minimum height
%           difference for a neighboring local maxima to properly define
%           depth in noisy conditions. Must be smaller than
%           significantLocalDepth.</li>
%       </ul>
%     </td>
%   </tr>
% </table>
% </html>
%
%% Output
%
% <html>
% <table>
%   <tr>
%     <th>maxVal -- peak values</th>
%   </tr>
%   <tr>
%     <td>double vector</td>
%   </tr>
%   <tr>
%     <td>maxVal is a Nx1 vector with the peak values of N detected peaks</td>
%   </tr>
% </table>
% </html>
%
%%
% <html>
% <table>
%   <tr>
%     <th>maxLoc -- peak index</th>
%   </tr>
%   <tr>
%     <td>double vector</td>
%   </tr>
%   <tr>
%     <td>maxLoc is a Nx1 vector with the location of the N peaks as index
%       of y</td>
%   </tr>
% </table>
% </html>
%
%%
% <html>
% <table>
%   <tr>
%     <th>maxWidth -- left and right starting index of peaks</th>
%   </tr>
%   <tr>
%     <td>Nx2 double matrix</td>
%   </tr>
%   <tr>
%     <td>maxWidth is a Nx2 matrix with the left and right starting point
%       of each width measurement for N peaks</td>
%   </tr>
% </table>
% </html>
%
%%
% <html>
% <table>
%   <tr>
%     <th>maxBase -- reference level of width measurement</th>
%   </tr>
%   <tr>
%     <td>double vector</td>
%   </tr>
%   <tr>
%     <td>maxBase is a Nx1 vector with the reference level for which the
%       percentage height for width measurement was taken. The actual
%       height for the horizontal width measurement follows as: peak value
%       - base*reference level</td>
%   </tr>
% </table>
% </html>
%
%% See Also
% <spotFinder.html spotFinder>, <matlab:doc('findpeaks') findpeaks>,
% <findpeaksWrapperGUI.html findpeaksWrapperGUI>, <computePeakWidth.html
% computePeakWidth>

%%
% Copyright (C) David Dreher 2017
