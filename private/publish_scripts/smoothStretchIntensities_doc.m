%% smoothStretchIntensities
% smoothStretchIntensities smoothes and stretches the intensity results
% for each cell with a given filter and into a range from [0, 999]
%
%% Syntax
%   ySmooth = smoothStretchIntensities( intensResult, FilterOptions, intensType)
%   ySmooth = smoothStretchIntensities(___, plotFlag)
%   ySmooth = smoothStretchIntensities(___, plotFlag, outFigName)
%   [ySmooth, yUpsampled] = smoothStretchIntensities(___)
%   [ySmooth, yUpsampled, objectLabels] = smoothStretchIntensities(___)
%   [ySmooth, yUpsampled, objectLabels, scaling] = smoothStretchIntensities(___)
%
%% Description
% [ ySmooth, yUpsampled, objectLabels, scaling] =
% smoothStretchIntensities( intensResult, FilterOptions, intensType,
% plotFlag, outFigName) interpolates the measured intensity values
% linearly to a common scale of 1000 elements and then filters them with
% a given filter (Average, gaussian, lowpass and no filtering are
% available). Optionally it can save figures illustrating the filtered
% intensity results.
%
%% Example
% Load previously generated intensity results
%
testData = load('../testData/testData.mat');
intRes = testData.intensityResults(1);
%%%
% Set parameters
%
FilterOptions.type = 'lowpass';
FilterOptions.frequencyCutoff = 0.025;
intensType = 'max';
plotFlag = 'heatmap';
outFigName = 'testResults/heatmaps';
%%%
% Interpolate and filer signal
%
[ySm, yUp] = smoothStretchIntensities(intRes, FilterOptions, intensType, plotFlag, outFigName);
%%%
% Plot results of smoothed and non-smoothed intensity value
%
subplot(2,1,1);
plot(ySm(5,:));
title('Smoothed intensity values')
ylabel('Intensity values')
subplot(2,1,2);
plot(yUp(5,:));
title('Upsampled non-smoothed intensity values');
ylabel('Intensity values')
xlabel('Normalized length of cells');
% 
%% Input
% <html>
% <table>
%   <tr>
%     <th>intensResults -- result structure from computeBorderIntensities</th>
%   </tr>
%   <tr>
%     <td>scalar struct</td>
%   </tr>
%   <tr>
%     <td>Scalar result structure containing the results from
%         computeBorderIntensities of one image: border intensities and
%         metadata of objects. Necessary field for execution is
%         borderIntensity with either subfield maxIntensity or
%         meanIntensity depending on the chosen intensType. If plots are
%         requested the field signalImageName must be also supplied. Note:
%         Only a scalar entity of the structure is passed.</td>
%   </tr>
% </table>
% </html>
%
%%
% <html>
% <table>
%   <tr>
%     <th>FilterOptions -- parameter struct for filtering</th>
%   </tr>
%   <tr>
%     <td>scalar struct</td>
%   </tr>
%   <tr>
%     <td>Structure with the settings for filtering the intensity values.
%         Commong field is:
%         <ul>
%           <li>type: Type of filter: 'average', 'gaussian', 'lowpass' or
%           'none'</li>
%         </ul>
%         Additional fields depend on the filter type:
%         For average filtering:
%         <ul>
%           <li>length: Filter length in integer</li>
%         </ul>
%         For gaussian filtering:
%         <ul>
%           <li>length: Filter length in integer</li>
%           <li>sigma: Standard deviation of the gaussian (Optional
%           defaults to 0.5)</li>
%         </ul>
%         For lowpass filtering:
%         <ul>
%           <li>'filterOrder': Order of the filter (Optional defaults to
%           300)</li>
%           <li>frequencyCutoff': Cutoff frequency (Optional defaults to 0.5)</li>
%         </ul>
%     </td>
%   </tr>
% </table>
% </html>
%
%%
% <html>
% <table>
%   <tr>
%     <th>intensType -- Type of intensity</th>
%   </tr>
%   <tr>
%     <td>character vector</td>
%   </tr>
%   <tr>
%     <td>String identifying which type of intensity should be used, either
%       'max' or 'mean'.</td>
%   </tr>
% </table>
% </html>
%
%%
% <html>
% <table>
%   <tr>
%     <th>plotFlag -- Type of plot</th>
%   </tr>
%   <tr>
%     <td>'none' (default) | character vector</td>
%   </tr>
%   <tr>
%     <td>String identifying whether and which type of plot should be
%       generated. Possible values are 'XY' for an X-Y plot of smoothed
%       intensity values, 'heatmap' for a heatmap like plot or 'none' (the
%       default) for no plots.</td>
%   </tr>
% </table>
% </html>
%
%%
% <html>
% <table>
%   <tr>
%     <th>outFigName -- filepath to saving location with base name</th>
%   </tr>
%   <tr>
%     <td>[] (default) | character vector</td>
%   </tr>
%   <tr>
%     <td>String containing the basic path information for saving plots. If
%       left empty plots are generated but not saved or closed.</td>
%   </tr>
% </table>
% </html>
%
%% Output
% <html>
% <table>
%   <tr>
%     <th>ySmooth -- smoothed intensity values</th>
%   </tr>
%   <tr>
%     <td>Mx1000 double matrix</td>
%   </tr>
%   <tr>
%     <td>A Mx1000 matrix wih the upsampled and smoothed intensity values
%       for M objects of tbe current image.</td>
%   </tr>
%   <tr>
%     <td></td>
%   </tr>
% </table>
% </html>
%
%%
% <html>
% <table>
%   <tr>
%     <th>yUpsampled -- non-smoothed, interpolated intensity values</th>
%   </tr>
%   <tr>
%     <td>Mx1000 double matrix</td>
%   </tr>
%   <tr>
%     <td>A Mx1000 matrix with upsampled but unsmoothed intensity values
%     for M objects of the current image.</td>
%   </tr>
% </table>
% </html>
%
%%
% <html>
% <table>
%   <tr>
%     <th>objectLabels --  origininal labels of objects</th>
%   </tr>
%   <tr>
%     <td>Mx1 double vector</td>
%   </tr>
%   <tr>
%     <td>A Mx1 vector with the indices of the objects for which a
%       intensity profile was measured.</td>
%   </tr>
% </table>
% </html>
%
%%
% <html>
% <table>
%   <tr>
%     <th>scaling -- scaling factors of cell boundaries</th>
%   </tr>
%   <tr>
%     <td>Mx1 double vector</td>
%   </tr>
%   <tr>
%     <td>scaling is a Mx1 vector with the scaling factors between actual
%       size and common size.</td>
%   </tr>
% </table>
% </html>
%
%% See Also
% <computeSpotResults.html computeSpotResults>, <matlab:doc('filtfilt')
% filtfilt>, <matlab:doc('conv') conv>, <matlab:doc('fir1') fir1>,
% <matlab:doc('fspecial') fspecial>

%%
% Copyright (C) David Dreher 2017



