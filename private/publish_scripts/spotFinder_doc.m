%% spotFinder
% spotFinder Identifies and classifies spots, i.e. high intensity regions on
% the membrane using a custom supplied detection algorithm and computes
% polarization and bipolarity ratios.
%
%% Syntax
%   spots = spotFinder(ySmoothed, yUpsampled, objectLabels, PeakOptions, polarWeights)
%   spots = spotFinder(___, polarIntTypes)
%
%% Description
% spots = spotFinder( ySmoothed, yUpsampled, objectLabels, PeakOptions,
% polarWeights, polarIntTypes) identifies and classifies spots into two
% classes depending on their relative location on the membrane: polar/tip
% located and side spots.
% Spots are identified as high intensity regions via a peak detection
% algorithm. A wrapper for the Matlab built-in findpeaks function is
% supplied but any custom generated function can be supplied to adapt the
% detection to your experiment conditions. Additionally to the normal
% findpeaks operation this function supplies a custom peak width
% measurement function to allow for more differentiated with measurements
% than only half-height and half-prominence as in findpeaks.
% Each spot has measurements associated with it: Size, location, mean and
% max intensity, roughness (approximated by the standard deviation of the
% derivative). With these measurements an importance score is calculated
% for each spot depending on the weights.
% With $\mu$ as mean intensity, $L$ as size, $R$ as roughness, $M$ as max
% intensity and $w_{\mu}, w_L, w_R, w_M$ as corresponding weights the
% score is computed as:
%
% $$S = \mu^{w_{\mu}} * L^{w_L} * \frac{1}{R^{w_R}} * M^{w_M}$$
%
% Polarization ratio is then computed as the ratio of scores of polar
% spots and total score of all spots.
% Bipolarity ratio is defined as ratio of difference in scores between
% both polar spots normlized to 0-1 range, with 1 being equal scores and
% 0 being only one spot.
%
%% Example
% Load prepared intensity results and parameters
%
testData = load('../testData/testData.mat');
peakOpt = testData.PeakOptions;
ySm = testData.ySmoothed;
yUp = testData.yUpsampled;
%%%
% Create dummy object labels
%
objectLabels = 1:size(ySm,1);
%%%
% Set weights
%
polarWeights = [1,1,0,0];
%%%
% Detect spots and plot histogram of polarization ratios
%
spots = spotFinder( ySm, yUp, objectLabels, peakOpt, polarWeights)
histogram([spots.ratioPolarization], 25);

%% Input
%
% <html>
% <table>
%   <tr>
%     <th>ySmoothed -- smoothed intensity values</th>
%   </tr>
%   <tr>
%     <td>Mx1000 double matrix</td>
%   </tr>
%   <tr>
%     <td>A Mx1000 matrix with the smoothed and interpolated intensity
%       values. Columns correspond to position on the membrane and rows to
%       the M objects found.</td>
%   </tr>
% </table>
% </html>
%
%%
% <html>
% <table>
%   <tr>
%     <th>yUpsampled -- unsmoothed intensity values</th>
%   </tr>
%   <tr>
%     <td>Mx1000 double matrix</td>
%   </tr>
%   <tr>
%     <td>A Mx1000 matrix with the upsampled but unsmoothed intensity values.
%     Columns correspond to position on the membrane and rows to the M
%     objects found.</td>
%   </tr>
% </table>
% </html>
% 
%%
% <html>
% <table>
%   <tr>
%     <th>objectLabels -- object IDs</th>
%   </tr>
%   <tr>
%     <td>Mx1 double vector</td>
%   </tr>
%   <tr>
%     <td>A M-element vector holding the original object ID</td>
%   </tr>
% </table>
% </html>
%     
%%
% <html>
% <table>
%   <tr>
%     <th>PeakOptions --  parameters struct for peak detection</th>
%   </tr>
%   <tr>
%     <td>struct</td>
%   </tr>
%   <tr>
%     <td>Struct with parameters to to detect the spots or peaks in the
%       signal intensity profile. It must contain the fields:
%       <ul>
%         <li>function: Name of the detection function that must accept
%           three arguments: the intensity profile, the detection
%           parameters and the width measurement parameters and returns: [
%           the maximum value for each spot, the location of each spot, the
%           width of each spot and the baselevel of each spot for which the
%           width was computed]. See findpeaksWrapper for an example.</li>
%         <li>FinderOptions: Parameters struct for spot detection</li>
%         <li>WidthOptions: Parameters struct for spot size measurement</li>
%       </ul>
%       Optional fields are:
%       <ul>
%         <li>correctBaseline: Bool flag for normalization of each
%         intensity profile so that min(I)=0</li>
%         <li>GUI: The name of a GUI for the peak detetion function, to
%           allow displaying a user interface for parameter tuning. The GUI
%           must return two outputs: the FinderOptions and the
%           WidthOptions, if it returns empty arrays, it will be
%           interpreted as a cancel of change of parameters and the
%           previous parameters are retained.</li>
%       </ul>
%     </td>
%   </tr>
% </table>
% </html>
%
%%
% <html>
% <table>
%   <tr>
%     <th>polarWeights -- weight vector or cell array of weight vectors</th>
%   </tr>
%   <tr>
%     <td>4-element vector | cell array with 4-element vectors</td>
%   </tr>
%   <tr>
%     <td>Either a 4-element vector or cell array of 4-element vectors. In
%     case of a cell array polarization and bipolarity ratios are
%     computed for each weight vector. Each describe an exponential
%     weight for the following measurements:
%       <ol>
%         <li>Size of the spoty</li>
%         <li>Mean intensity of the spot</li>
%         <li>Smoothness of the spot</li>
%         <li>Max intensity of the spot</li>
%       </ol>
%     </td>
%   </tr>
% </table>
% </html>
%
%%
% <html>
% <table>
%   <tr>
%     <th>polarIntTypes -- string identifying type of intensity for scoring</th>
%   </tr>
%   <tr>
%     <td>'SUS' (default) | 3-element character vector of 'S' or 'U'</td>
%   </tr>
%   <tr>
%     <td>A 3-element string conisting of 'S's or 'U's identifying the type
%       of intensity that should be used for the different measurements.
%       'S' refers to smoothed/filtered intensities, 'U' refers to
%       unsmoothed/unfiltered intensities. The first letter corresponds to
%       the intensity values used for mean intensity computation, the
%       second one to intensity values for smoothness and the third for max
%       intensity.</td>
%   </tr>
% </table>
% </html>
%
%% Output
%
% spots -- a Mx1 structure holding for each of the M objects the following
% fields:
%
% <html>
% <table>
%   <tr>
%     <th>Fields</th>
%     <th>Description</th>
%   </tr>
%   <tr>
%     <td>polarSpots</td>
%     <td>structure with the measurements for P spots classified
%       as polar spots</td>
%   </tr>
%   <tr>
%     <td>sideSpots</td>
%     <td>structure with the measurements for P spots classified
%       as side spots</td>
%   </tr>
%   <tr>
%     <td> </td>
%     <td>Each of these holds the following measurements as fields in the
%       structure:
%       <table>
%         <tr>
%           <th>Fields</th>
%           <th>Description</th>
%         </tr>
%         <tr>
%           <td>count</td>
%           <td>number of spots of this type on this object</td>
%         </tr>
%         <tr>
%           <td>sizes</td>
%           <td>vector with the measured spot sizes</td>
%         </tr>
%         <tr>
%           <td>meanSm</td>
%           <td>vector with mean intensities of each spot
%             computed from the smoothed intensity values</td>
%         </tr>
%         <tr>
%           <td>meanUp</td>
%           <td>vector with mean intensities of each spot
%             computed from the unsmoothed intensity values</td>
%         </tr>
%         <tr>
%           <td>leftMins</td>
%           <td>vector with left boundaries of spots</td>
%         </tr>
%         <tr>
%           <td>rightMins</td>
%           <td>vector with right boundaries of spots</td>
%         </tr>
%         <tr>
%           <td>peakLocs</td>
%           <td>vector with locatiosns of peak values of spots</td>
%         </tr>
%         <tr>
%           <td>intensityValuesSm</td>
%           <td>vector with the smoothed intensity values</td>
%         </tr>
%         <tr>
%           <td>intensityValuesUp</td>
%           <td>vector with the unsmoothed intensity values</td>
%         </tr>
%         <tr>
%           <td>peakBase</td>
%           <td>vector of values from which the base reference
%             for width measurements is computed</td>
%         </tr>
%         <tr>
%           <td>peakVal</td>
%           <td>vector of peak values of spots</td>
%         </tr>
%       </table>
%     </td>
%   </tr>
%   <tr>
%     <td>totalCount</td>
%     <td>total count of spots for each object</td>
%   </tr>
%   <tr>
%     <td>ratioPolarization</td>
%     <td>Ratio of accumulated scores of polar spots and total score of all
%       spots. If multiple weight vectors are supplied the result of the
%       first one will be reported here, while the others will be reported
%       in custom fields that follow the following convention:
%       ratioPolarization_(w_mu)_(w_L)_(w_R)_(w_M) in case of
%       non-integer values the digit (.) will be replaced by a 'd'.</td>
%   </tr>
%   <tr>
%     <td>ratioBipolarity</td>
%     <td>Imbalance between the two polar spots scores normalized to 0-1,
%       with 1 being perfectly balanced and 0 being imbalanced completely
%       to one side. If multiple weight vectors are supplied the result of
%       the first one will be reported here, while the others will be
%       reported in custom fields that follow the same convention as for
%       the polarization ratio.</td>
%   </tr>
%   <tr>
%     <td>label</td>
%     <td>Original label of the object for which spots where identified.</td>
%   </tr>
% </table>
% </html>
%
%% See Also
% <computeSpotResults.html computeSpotResults>, <findpeaksWrapper.html
% findpeaksWrapper>
%%
% Copyright (C) David Dreher 2017