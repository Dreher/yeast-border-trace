%% measureBorderIntensities
% measureBorderIntensities computes average intensities across the border of
% roundish shapes
%% Syntax
%   borderIntensity = measureBorderIntensities(segmenImg, signalImg, widthMembrane, pxlsToFit)
%   borderIntensity = measureBorderIntensities(___, rescaleFlag)
%   [borderIntensity, ConnComp] = measureBorderIntensities(___)
%   [borderIntensity, ConnComp, RegionStats] = measureBorderIntensities(___)
%   [borderIntensity, ConnComp, RegionStats, warnState] = measureBorderIntensities(___)
%
%% Description
% [intensRes, ConnComp, RegionStats, warnState] =
% measureBorderIntensities( segmenImg, signalImg, widthMembrane,
% pxlsToFit, rescaleFlag ) computes the intensities along a membrane
% derived from a segmentation image.
% From the object segmentation a membrane image is computed based on
% pixel estimate of the membrane width. From this membrane image a middle
% ring is computed. For each pixel on this middle ring a parameter curve
% is fitted to a small part of the ring. Then a line perpendicular to
% this curve going through the pixel of interest is computed and
% intensities across this line are measured and inscribed as result for
% this point. Both mean and max intensities across this line are saved.
% For additional analysis different measurements like the major points on
% the middle ring are available in the output.
% The function expects objects that can be sufficiently fitted with an
% ellipse and have a smooth, i.e. not jagged, border.
% A larger segmentation can be accomodated by increasing the membrane
% with, but the funtion will fail with smaller segmentations.
% 
% This function utilizes the Parallel Processing Toolbox when available
% for parallelisation across objects.
%
%% Example
% Read in signal and segmentation image

segmenImg = imread('../testData/Segmentation/Test_20170101_1_007_nodrug_25_f0.tif')>0;
signalImg = imread('../testData/Signal/Test_20170101_1_007_nodrug_25_f0.tif');
widthMembrane = 3;
pxlsToFit = 4;
rescaleIntens = false;
[intensRes, CC, RegionStats] = measureBorderIntensities(segmenImg, signalImg, widthMembrane, pxlsToFit)

%%% 
% Plot an example cell and its intensity profile
figure;
subplot(1,2,1);
imshow(RegionStats(5).signalImage, []);
subplot(1,2,2);
plot(intensRes(5).maxIntensities)

%% Input
%
% <html>
% <table>
%   <tr>
%     <th>segmenImg -- segmentation image</th>
%   </tr>
%   <tr>
%     <td>binary 2D image</td>
%   </tr>
%   <tr>
%     <td>2 dimensional array with values 0 for background and >0 for
%         objects. Dimensions of both images must be identical.</td>
%   </tr>
%   <tr>
%     <td>Data Types: single | double | int16 | uint8 | uint16 | uint32</td>
%   </tr>
% </table>
% </html>
%
%%
% 
% <html>
% <table>
%   <tr>
%     <th>signalImg -- signal image</th>
%   </tr>
%   <tr>
%     <td>grayscale 2D image</td>
%   </tr>
%   <tr>
%     <td>Image with the intensity values that should be measured.
%         Dimensions of both images must be identical.</td>
%   </tr>
%   <tr>
%     <td>Data Types: single | double | int16 | uint8 | uint16 | uint32</td>
%   </tr>
% </table>
% </html>
%
%%
% <html>
% <table>
%   <tr>
%     <th>widthMembrane -- estimated width of the membrane</th>
%   </tr>
%   <tr>
%     <td>scalar positive integer</td>
%   </tr>
%   <tr>
%     <td>Estimation of membrane width in pixel values inwards from the
%         segmentation.</td>
%   </tr>
%   <tr>
%     <td>Data Types: single | double </td>
%   </tr>
% </table>
% </html>
%
%%
%
% <html>
% <table>
%   <tr>
%     <th>pxlsToFit -- pixels for parametric curve fit</th>
%   </tr>
%   <tr>
%     <td>scalar positive integer</td>
%   </tr>
%   <tr>
%     <td>number of pixels to the left and right of the pixel of interest
%         for which the parametric curve is fitted.</td>
%   </tr>
%   <tr>
%     <td>Data Types: single | double</td>
%   </tr>
% </table>
% </html>
%
%%
%
% <html>
% <table>
%   <tr>
%     <th>rescaleFlag -- optional flag to trigger rescaling</th>
%   </tr>
%   <tr>
%     <td>false (default) | bool scalar</td>
%   </tr>
%   <tr>
%     <td>If set to true each image will be rescaled so that 1 corresponds
%         to the image maximum intensity.</td>
%   </tr>
%   <tr>
%     <td>Data Types: bool | single | double</td>
%   </tr>
% </table>
% </html>
%
%% Output
%
% <html>
% <table>
%   <tr>
%     <th>borderIntensity -- Mx1 structure for M objects their intensity measurements</th>
%   </tr>
%   <tr>
%     <td>struct</td>
%   </tr>
%   <tr>
%     <td> For each of the M objects in the image and the structure holds the
%     measured mean and max intensities perpendicular to the middle ring
%     in the two fields:
%       <ul>
%         <li>meanIntensities</li>
%         <li>maxIntensities</li>
%       </ul>
%     </td>
%   </tr>
% </table>
% </html>
%
%%
%
% <html>
% <table>
%   <tr>
%     <th>ConnComp -- connected components</th>
%   </tr>
%   <tr>
%     <td>struct</td>
%   </tr>
%   <tr>
%     <td>Connected components structure as returned from BWCONNCOMP</td>
%   </tr>
% </table>
% </html>
%
%%
%
% <html>
% <table>
%   <tr>
%     <th>RegionStats -- Mx1 structure for M objects their region properties</th>
%   </tr>
%   <tr>
%     <td>struct</td>
%   </tr>
%   <tr>
%     <td>For eacht fo the M objects in the image the function computes the
%     following region properties and stores them as fields in the
%     RegionStats structure.
%       <ul>
%         <li>Area: Area of the object</li>
%         <li>Centroid: Centroid of the object</li>
%         <li>BoundingBox: Bounding box of the object</li>
%         <li>MajorAxisLength: Major axis of inertia ellipse</li>
%         <li>MinorAxisLength: Minor axis of inertia ellipse</li>
%         <li>Eccentricity: eccentricity of inertia ellipse</li>
%         <li>Orientation: Angle of major axis of inertia ellipse</li>
%         <li>Image: Subsampled image of object segmentation</li>
%         <li>PixelValues: Intensity values of object</li>
%         <li>MeanIntensity: Mean intensity of whole object</li>
%         <li>overlayImage: Overlay image of outer and middle border ring</li>
%         <li>majorPoints: Intersection of the inertia ellipse axis with
%             the estimted middle line of the membrane as a 4x2 matrix.
%             Rows are the points in the order 2 intersections with the
%             major axis and then 2 intersections with the minor axis.
%             Colums are X and Y coordinates.</li>
%         <li>majorPointsPerimeter: Intersection of the inertia ellipse
%             axis with the outer perimeter of the object, in the same
%             form as above.</li>
%         <li>signalImage: Subsampled signal image of the object</li>
%         <li>skipped: String identifying the reason if an object has been
%             skipped, is empty otherwise</li>
%         <li>boundary: X-Y coordinates of the estimated middle ring of the
%             membrane</li>
%         <li>boundaryNpixels: Number of pixels that constitue the middle ring</li>
%         <li>boundaryLength: Euclidean length of the middle ring</li>
%       </ul>
%     </td>
%   </tr>
% </table>
% </html>
%
%%
%
% <html>
% <table>
%   <tr>
%     <th>warnState -- Warning indicator</th>
%   </tr>
%   <tr>
%     <td>bool scalar</td>
%   </tr>
%   <tr>
%     <td>Indicator whether a warning has been generated, useful for proper
%         progress report when batch executing.</td>
%   </tr>
% </table>
% </html>
%
%% See Also
% <computeBorderIntensities.html computeBorderIntensities>
%
%%
% Copyright (C) David Dreher 2017
