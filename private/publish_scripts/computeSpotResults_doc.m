%% computeSpotResults
% computeSpotResults batch executes smoothStretchIntensities and spotFinder
% on a set of computed border intensity values.
%% Syntax
%   spotResults = computeSpotResults(intensResults, FilterOptions, PeakOptions, resultFilepath, plotIntensFlag, polarWeights)
%   spotResults = computeSpotResults(___, polarIntTypes)
%   spotResults = computeSpotResults(___, polarIntTypes, useGUI)
%   [spotResults, FilterOptions] = computeSpotResults(___)
%   [spotResults, FilterOptions, PeakOptions] = computeSpotResults(___)
%   [spotResults, FilterOptions, PeakOptions, polarWeights, polarIntTypes] = computeSpotResults(___)
%   [spotResults, FilterOptions, PeakOptions, polarWeights, polarIntTypes] = computeSpotResults(___)
%
%% Description
% [spotResults, FilterOptions, PeakOptions, polarWeights, polarIntTypes] =
% resultFilepath, plotIntensFlag, polarWeights, polarIntTypes, useGUI)
% computes the spot classification for one to multiple images. It saves
% its results automatically to the given result folder. If the user wants
% an interactive GUI can be started to tune the spot detection.
%
% This function utilizes the Parallel Processing Toolbox when available
% for parallelisation across images.
%
%% Example
% Load previously generated intensity results and parameters
%
testData = load('../testData/testData.mat');
intRes = testData.intensityResults;

%%%
% Set filter to lowpass and and a cutoff frequency between 0 and 1
%
filtOpt = struct('type', 'lowpass', 'frequencyCutoff', 0.025);

%%%
% Load peak detection algorithm to supplied findpeaks wrapper with some
% test parameters and use the GUI which lets us pick parameters
%
peakOpt = testData.PeakOptions;
useGUI = true;

%%%
% Set output parameters
%
resFile = '../testResults/spotResults';
plotF = 'none';
polarWeights = [1,1,0,0];
intTypes = 'SUS';

%%%
% Start processing with interactive GUI to change parameters
%
computeSpotResults( intRes, filtOpt, peakOpt, resFile, plotF, polarWeights, intTypes, useGUI )

%% Input
%
% <html>
% <table>
%   <tr>
%     <th>intensResults -- result structure from computeBorderIntensities</th>
%   </tr>
%   <tr>
%     <td>struct</td>
%   </tr>
%   <tr>
%     <td>Nx1 result structure for each of N files containing the results
%     from computeBorderIntensities: border intensities and metadata of
%     objects.</td>
%   </tr>
% </table>
% </html>
%
%%
% <html>
% <table>
%   <tr>
%     <th>FilterOptions -- parameters struct for filtering of raw intensity signal. </th>
%   </tr>
%   <tr>
%     <td>struct</td>
%   </tr>
%   <tr>
%     <td>Struct with parameters for the filtering of the raw intensity
%     signal. It must contain a field 'type' to identify the kind of filter
%     used, types currently supported: 'gaussian', 'average', 'lowpass' and
%     'none'. Further fields can be supplied to pass additional parameters
%     depending on the filter type, see the documentation of
%     smoothStretchIntensities for more information.</td>
%   </tr>
% </table>
% </html>
%
%%
% <html>
% <table>
%   <tr>
%     <th>PeakOptions --  parameters struct for peak detection</th>
%   </tr>
%   <tr>
%     <td>struct</td>
%   </tr>
%   <tr>
%     <td>Struct with parameters to to detect the spots or peaks in the
%       signal intensity profile. It must contain the fields:
%       <ul>
%         <li>function: Name of the detection function that must accept
%           three arguments: the intensity profile, the detection
%           parameters and the width measurement parameters and returns: [
%           the maximum value for each spot, the location of each spot, the
%           width of each spot and the baselevel of each spot for which the
%           width was computed]. See findpeaksWrapper for an example.</li>
%         <li>FinderOptions: Parameters struct for spot detection</li>
%         <li>WidthOptions: Parameters struct for spot size measurement</li>
%       </ul>
%       Optional fields are:
%       <ul>
%         <li>correctBaseline: Bool flag for normalization of each
%         intensity profile so that min(I)=0</li>
%         <li>GUI: The name of a GUI for the peak detetion function, to
%           allow displaying a user interface for parameter tuning. The GUI
%           must return two outputs: the FinderOptions and the
%           WidthOptions, if it returns empty arrays, it will be
%           interpreted as a cancel of change of parameters and the
%           previous parameters are retained.</li>
%       </ul>
%     </td>
%   </tr>
% </table>
% </html>
%
%%
% <html>
% <table>
%   <tr>
%     <th>resultFilepath -- filepath to saving location with base name</th>
%   </tr>
%   <tr>
%     <td>character vector</td>
%   </tr>
%   <tr>
%     <td>Filepath to the disk location where results should be stored,
%       given as path with a basic filename. Execution parameters will be
%       saved in a second file with the same basic filename and _parameters
%       appended.</td>
%   </tr>
% </table>
% </html>
%
%%
% <html>
% <table>
%   <tr>
%     <th>plotIntensFlag -- plot type</th>
%   </tr>
%   <tr>
%     <td>character vector</td>
%   </tr>
%   <tr>
%     <td>String identifying whether intermediate plots of intensities
%       should be saved to disk. Possible entries are 'xy' for an xy-plot,
%       'heatmap' for a heatmap with the intensities or 'none' for no
%       plot.</td>
%   </tr>
% </table>
% </html>
%
%%
% <html>
% <table>
%   <tr>
%     <th>polarWeights -- weight vector or cell array of weight vectors</th>
%   </tr>
%   <tr>
%     <td>4-element vector | cell array with 4-element vectors</td>
%   </tr>
%   <tr>
%     <td>Either a 4-element vector or cell array of 4-element vectors. In
%     case of a cell array polarozation and bipolarity ratios are
%     computed for each weight vector. Each describe an exponential
%     weight for the following measurements:
%       <ol>
%         <li>Size of the spoty</li>
%         <li>Mean intensity of the spot</li>
%         <li>Smoothness of the spot</li>
%         <li>Max intensity of the spot</li>
%       </ol>
%     </td>
%   </tr>
% </table>
% </html>
%
%%
% <html>
% <table>
%   <tr>
%     <th>polarIntTypes -- string identifying type of intensity for scoring</th>
%   </tr>
%   <tr>
%     <td>'SUS' (default) | 3-element character vector of 'S' or 'U'</td>
%   </tr>
%   <tr>
%     <td>A 3-element string conisting of 'S's or 'U's identifying the type
%       of intensity that should be used for the different measurements.
%       'S' refers to smoothed/filtered intensities, 'U' refers to
%       unsmoothed/unfiltered intensities. The first letter corresponds to
%       the intensity values used for mean intensity computation, the
%       second one to intensity values for smoothness and the third for max
%       intensity.</td>
%   </tr>
% </table>
% </html>
%
%%
% <html>
% <table>
%   <tr>
%     <th>useGUI -- start GUI</th>
%   </tr>
%   <tr>
%     <td>false (default) | bool scalar</td>
%   </tr>
%   <tr>
%     <td>Bool flag whether the interactive GUI should be started for
%       parameter tuning.</td>
%   </tr>
% </table>
% </html>
%
%% Output
%
% spotResults -- Nx1 structure for N image files with M objects in each
% containing the following fields:
%
% <html>
% <table>
%   <tr>
%     <th>Fields</th>
%     <th>Description</th>
%   </tr>
%   <tr>
%     <td>ySmoothed</td>
%     <td>Filtered/smoothed intensity values as a Mx1000 matrix</td>
%   </tr>
%   <tr>
%     <td>yUpsampled</td>
%     <td>Filtered/smoothed intensity values as a Mx1000 matrix</td>
%   </tr>
%   <tr>
%     <td>objectLabels</td>
%     <td>Mx1 vector with the IDs of the cells for which a proper
%         spot detection could be computed (For degenerated cells a spot
%         computation may fail resulting in a skipped row but to allow for
%         later identification in the</td>
%   </tr>
%   <tr>
%     <td>totalCount</td>
%     <td>Total number of spots detected</td>
%   </tr>
%   <tr>
%     <td>scaling</td>
%     <td>Ratio of actual actual border length and the shared interpolation
%         length as Mx1 vector</td>
%   </tr>
%   <tr>
%     <td>spots</td>
%     <td>Result structure from spotFinder, see its documentation
%         for detailed information. Contains the following fields:
%         polarSpots, sideSpots, totalCount, ratioPolarization,
%         ratioBipolarity, label and additional fields reporting
%         polarization and bipolarity ratios in case more than one weight
%         vector was supplied.</td>
%   </tr>
% </table>
% </html>
%
%%
% 
% Additionally to its results it also outputs the actual used parameters
% for FilterOptions, PeakOptions, polarWeights, polarIntTypes, since the
% user has the possibility to change them through the GUI after executing
% the function. Also the parameters are stored in  a MAT-file
% for documentation purposes.
% 
%% See Also
% <computeBorderIntensities.html computeBorderIntensities>,
% <smoothStretchIntensities.html smoothStretchIntensities>,
% <spotFinder.html spotFinder>

%%
% Copyright (C) David Dreher 2017 
