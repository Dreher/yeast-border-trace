%% manageOutput
% manageOutput restructures the output of computeSpotResults by grouping
% similar conditions as identified by filenames and creating a more human
% readable format with the additional option of creating Excel sheets.
%
%% Syntax
%   [outputSpots, outputObjects] = manageOutput(intensRes, spotRes, searchExpr, tokenKeys, outFilepath,  xlOutput, thresholdPolar)
%   [outputSpots, outputObjects] = manageOutput(___, tokenKeyNumbering)
%
%% Description
% [outputSpots, outputObjects] = manageOutput(intensRes, spotRes,
% searchExpr, tokenKeys, outFilepath,  xlOutput, thresholdPolar,
% tokenKeyNumbering) restructures the results from
% computeBorderIntensities and computeSpotResults into two large cell
% arrays outputSpots and outputObjects with the results for individual
% spots and total cells respectively. The first row of the cell array
% holds table like headers to identify the data. Metadata is extracted
% based on tokens of a regular expression search in the filenames and
% same metadata conditions are grouped together. 
% Additionally Excel sheets can be created from the output.
%
%% Example 
% Load previously computed results from computeSpotResults and computeBorderIntensities
% 
testData = load('../testData/testData.mat');
intRes = testData.intensityResults;
spotRes = testData.spotResults;

%%%
% Create regular expression to match metadata encoded in filenames
%
searchExpr = 'Test_\d{8}_(\d)_(\d+)_(.*)_(\d\d)_.*.tif';
tokenKeys = {'Timepoint', 'Strain No', 'Treatment', 'Temperature'};

%%%
% Setup tokenKeyNumbering for the third metadata 'Treatment'. Table
% contents will be numbers for matches into tokenKeyNumbering instead of
% strings encoded in filenames:
%
tokenKeyNumbering = {{}, {}, {'nodrug', 'DMSO', 'BFA'}, {}};

%%%
% Set other parameters and create output cell arrays and plot an example
% output (histogram of polarization ratios).
%
outFilepath = '../testResults/testOutput';
xlOutput = false;
thrshPol = 0.75;
[outputSpots, outputObjects] = manageOutput(intRes, spotRes, ...
    searchExpr, tokenKeys, outFilepath,  xlOutput, thrshPol, ...
    tokenKeyNumbering);
outputSpots(1:3,:)
outputObjects(1:3,:)
histogram([outputObjects{2:end, strcmpi(outputObjects(1,:), 'Polarization ratio')}])

%% Input
%
% <html>
% <table>
%   <tr>
%     <th>intensResults -- result structure from computeBorderIntensities</th>
%   </tr>
%   <tr>
%     <td>struct</td>
%   </tr>
%   <tr>
%     <td>Nx1 result structure for each of N files containing the results
%     from computeBorderIntensities: border intensities and metadata of
%     objects.</td>
%   </tr>
% </table>
% </html>
%
%%
% <html>
% <table>
%   <tr>
%     <th>spotRes -- spot detection result structure</th>
%   </tr>
%   <tr>
%     <td>struct</td>
%   </tr>
%   <tr>
%     <td>Nx1 result structure from computeSpotResults. It holds the spot
%       measurements and their classification and spot based measurements
%       like polarization ratios for all N-files in the data set.</td>
%   </tr>
% </table>
% </html>
%
%%
% <html>
% <table>
%   <tr>
%     <th>searchExpr -- regular expression for filenames metadata</th>
%   </tr>
%   <tr>
%     <td>character vector</td>
%   </tr>
%   <tr>
%     <td>Regular expression identifying metadata from the signal
%       filenames. The regular expression must generate a proper match for
%       all filenames, while the tokens are extracted as metadata. All
%       kinds of metadata is supported. The function will try to convert
%       numerical metadata to doubles and otherwise resort to string
%       representation</td>
%   </tr>
% </table>
% </html>
%
%%
% <html>
% <table>
%   <tr>
%     <th>tokenKeys -- Header strings for filename metadata</th>
%   </tr>
%   <tr>
%     <td>cell array of character vectors</td>
%   </tr>
%   <tr>
%     <td>Cell array with description for the type of metadata for the
%       found tokens. These are used for populating the header row of the
%       cell arrays.</td>
%   </tr>
% </table>
% </html>
%
%%
% <html>
% <table>
%   <tr>
%     <th>outFilepath -- filepath to saving location with base name</th>
%   </tr>
%   <tr>
%     <td>character vector</td>
%   </tr>
%   <tr>
%     <td>Filepath to the disk location where results should be stored,
%       given as path with a basic filename. </td>
%   </tr>
% </table>
% </html>
%
%%
% <html>
% <table>
%   <tr>
%     <th>xlOutput -- flag for excel spreadsheet creation</th>
%   </tr>
%   <tr>
%     <td>bool scalar</td>
%   </tr>
%   <tr>
%     <td>Bool flag whether Excel spreadsheets should be created (True for
%       Excel sheets, false for none).</td>
%   </tr>
% </table>
% </html>
%
%%
% <html>
% <table>
%   <tr>
%     <th>thresholdPolar -- Threshold for polar classification</th>
%   </tr>
%   <tr>
%     <td>scalar in range 0-1</td>
%   </tr>
%   <tr>
%     <td>Scalar threshold for classification of whole objects as polar or
%       non-polar. Objects with a polarization ratio below will get a false
%       flag in their polar classification column.</td>
%   </tr>
% </table>
% </html>
%
%%
% <html>
% <table>
%   <tr>
%     <th>tokenKeyNumbering -- Cell with comparision strings for numbering</th>
%   </tr>
%   <tr>
%     <td>cell array of character vectors</td>
%   </tr>
%   <tr>
%     <td>Cell array with strings for comparision and numbering of tokens.
%       In case the metadata in filenames is non-numeric but a numeric
%       representation is requested the comparisions strings can be
%       supplied and the corresponding metadata holds the index into the
%       tokenKeyNumbering cell array.</td>
%   </tr>
% </table>
% </html>
%
%% Output
%
% *outputSpots* is a table like cell array with a row for each spot holding
% all singular spot measurements as well as associated object
% measurements. It containts the following columns:
%
% <html>
% <table>
%   <tr>
%     <th>Column</th>
%     <th>Description</th>
%   </tr>
%   <tr>
%     <td>Condition/Combined metadata</td>
%     <td>Combined tokens separated by an underscore</td>
%   </tr>
%   <tr>
%     <td>Token metadata columns</td>
%     <td>Entries from the regular expression matching</td>
%   </tr>
%   <tr>
%     <td>Object No</td>
%     <td>Progressing counter of objects</td>
%   </tr>
%   <tr>
%     <td>Object label</td>
%     <td>ID label of corresponding object in original image</td>
%   </tr>
%   <tr>
%     <td>Spot No</td>
%     <td>Progressing counter of spots</td>
%   </tr>
%   <tr>
%     <td>Position</td>
%     <td>Scalar identifying the location of spot (1=Polar, 2= Side)</td>
%   </tr>
%   <tr>
%     <td>Size of spot</td>
%     <td>Size of spot in normalized coordinates</td>
%   </tr>
%   <tr>
%     <td>Mean intensity of spot-smoothed</td>
%     <td>Mean of smoothed intensity vectors</td>
%   </tr>
%   <tr>
%     <td>Mean intensity of spot-unsmoothed</td>
%     <td>Mean of unsmoothed intensity vectors</td>
%   </tr>
%   <tr>
%     <td>Mean intensity of object</td>
%     <td>Mean of total object intensities</td>
%   </tr>
%   <tr>
%     <td>Mean intensity along the membrane of object-smoothed</td>
%     <td>Mean intensity of whole smoothed membrane ring intensity values</td>
%   </tr>
%   <tr>
%     <td> Mean intensity along the membrane of object-unsmoothed</td>
%     <td>Mean intensity of whole unsmoothed membrane ring intensity values</td>
%   </tr>
%   <tr>
%     <td>Major Axis length</td>
%     <td>Distance of intersection points of major axis with membrane ring</td>
%   </tr>
%   <tr>
%     <td>Minor Axis length</td>
%     <td>Distance of intersection points of minor axis with membrane ring</td>
%   </tr>
%   <tr>
%     <td>Eccentricity</td>
%     <td>Eccentricity measurement as reported by regionstats</td>
%   </tr>
%   <tr>
%     <td>Area</td>
%     <td>Area of object in pixels</td>
%   </tr>
%   <tr>
%     <td>Deathscore</td>
%     <td>Dead cells are very bright over the whole object in comparision
%       to their alive counterparts. The deathscore is the median of the
%       z-values of the object pixels in regard to the total image pixel
%       value distribution. A large value indicates a overall brighter
%       object and hence probably a dead cell.</td>
%   </tr>
%   <tr>
%     <td>Polarization ratio</td>
%     <td>Ratio of accumulated scores of polar spots and total score of all spots.</td>
%   </tr>
%   <tr>
%     <td>Polarized</td>
%     <td>Bool to indicate whether a cell has been flagged as polarized</td>
%   </tr>
%   <tr>
%     <td>Bipolarity ratio</td>
%     <td>Imbalance between the two polar spots scores normalized to 0-1,
%       with 1 being perfectly balanced and 0 being imbalanced completely
%       to one side.</td>
%   </tr>
%   <tr>
%     <td>Number of perimeter pixels</td>
%     <td>Number of pixels that constitute the boundary</td>
%   </tr>
%   <tr>
%     <td>Euclidean length of perimeter</td>
%     <td>Euclidean length of membrane ring</td>
%   </tr>
%   <tr>
%     <td>If multiple weight vectors are supplied they will be reported in
%       additional columns with headers following the convention:</td>
%     <td></td>
%   </tr>
%   <tr>
%     <td>ratioPolarization_(w_mu)_(w_L)_(w_R)_(w_M)</td>
%     <td></td>
%   </tr>
%   <tr>
%     <td>bipolarityRatio_(w_mu)_(w_L)_(w_R)_(w_M)</td>
%     <td></td>
%   </tr>
% </table>
% </html>
%
%%
%
% *outputObjects* is a table like cell array with a row for each object
% holding all object measurements. It containts the following columns:
%
% <html>
% <table>
%   <tr>
%     <th>Column</th>
%     <th>Description</th>
%   </tr>
%   <tr>
%     <td>Condition/Combined metadata</td>
%     <td>Combined tokens separated by an underscore</td>
%   </tr>
%   <tr>
%     <td>Token metadata columns</td>
%     <td>Entries from the regular expression matching</td>
%   </tr>
%   <tr>
%     <td>Object No</td>
%     <td>Progressing counter of objects</td>
%   </tr>
%   <tr>
%     <td>Object label</td>
%     <td>ID label of corresponding object in original image</td>
%   </tr>
%   <tr>
%     <td>No of spots</td>
%     <td>Total number of spots on object membrane</td>
%   </tr>
%   <tr>
%     <td>Polarization ratio</td>
%     <td>Ratio of accumulated scores of polar spots and total score of all spots.</td>
%   </tr>
%   <tr>
%     <td>Polarized</td>
%     <td>Bool to indicate whether a cell has been flagged as polarized</td>
%   </tr>
%   <tr>
%     <td>Bipolarity ratio</td>
%     <td>Imbalance between the two polar spots scores normalized to 0-1,
%       with 1 being perfectly balanced and 0 being imbalanced completely
%       to one side.</td>
%   </tr>
%   <tr>
%     <td>Major Axis length</td>
%     <td>Distance of intersection points of major axis with membrane ring</td>
%   </tr>
%   <tr>
%     <td>Minor Axis length</td>
%     <td>Distance of intersection points of minor axis with membrane ring</td>
%   </tr>
%   <tr>
%     <td>Mean intensity of object</td>
%     <td>Mean of total object intensities</td>
%   </tr>
%   <tr>
%     <td>Mean intensity along the membrane of object-smoothed</td>
%     <td>Mean intensity of whole smoothed membrane ring intensity values</td>
%   </tr>
%   <tr>
%     <td> Mean intensity along the membrane of object-unsmoothed</td>
%     <td>Mean intensity of whole unsmoothed membrane ring intensity values</td>
%   </tr>
%   <tr>
%     <td>Eccentricity</td>
%     <td>Eccentricity measurement as reported by regionstats</td>
%   </tr>
%   <tr>
%     <td>Area</td>
%     <td>Area of object in pixels</td>
%   </tr>
%   <tr>
%     <td>Deathscore</td>
%     <td>Dead cells are very bright over the whole object in comparision
%       to their alive counterparts. The deathscore is the median of the
%       z-values of the object pixels in regard to the total image pixel
%       value distribution. A large value indicates a overall brighter
%       object and hence probably a dead cell.</td>
%   </tr>
%   <tr>
%     <td>Number of perimeter pixels</td>
%     <td>Number of pixels that constitute the boundary</td>
%   </tr>
%   <tr>
%     <td>Euclidean length of perimeter</td>
%     <td>Euclidean length of membrane ring</td>
%   </tr>
%   <tr>
%     <td>If multiple weight vectors are supplied they will be reported in
%       additional columns with headers following the convention:</td>
%     <td></td>
%   </tr>
%   <tr>
%     <td>ratioPolarization_(w_mu)_(w_L)_(w_R)_(w_M)</td>
%     <td></td>
%   </tr>
%   <tr>
%     <td>bipolarityRatio_(w_mu)_(w_L)_(w_R)_(w_M)</td>
%     <td></td>
%   </tr>
% </table>
% </html>
%
%% See Also
% <computeBorderIntensities.html computeBorderIntensities>,
% <computeSpotResults.html computeSpotResults>

%%
% Copyright (C) David Dreher 2017