%% computeMajorPoints
% computeMajorPoints computes the intersection of the inertia ellipse axis
% with the given boundary
%
%% Syntax
%   majorPoints = computeMajorPoints(centroid, angle, boundary)
%
%% Description
% majorPoints = computeMajorPoints(centroid, angle, boundary) computes
% the 4 intersections of 2 perpendicular axis with a given boundary. If
% no intersection can be found for an axis corresponding values are NaN.
% 
%% Example
% Create a circular boundary
%
centroid = [0,0];
angle = 45;
circAng=0:0.01:2*pi;
xb = sin(circAng) + centroid(1);
yb = cos(circAng) + centroid(2);
boundary = [xb(:), yb(:)];

%%%
% Detect the intersection of the given axis and its perpendicular
% counterpart with the given boundary and plot the result:
%
majorPoints = computeMajorPoints(centroid, angle, boundary);
figure;
plot(xb,yb);
hold on;
axis equal;
set(gca,'Ydir','reverse')
plot(majorPoints(1,1), majorPoints(1,2), 'ro');
plot(majorPoints(2,1), majorPoints(2,2), 'rx');
plot(majorPoints(3,1), majorPoints(3,2), 'bo');
plot(majorPoints(4,1), majorPoints(4,2), 'bx');
legend('Circle data', '1st axis intersection RHP', ...
    '1st axis intersection LHP', '2nd intersection RHP', ...
    '2nd intersection LHP', 'Location', 'best');

%% Input
% <html>
% <table>
%   <tr>
%     <th>centroid -- center of axis/ellipse</th>
%   </tr>
%   <tr>
%     <td>2-element real vector</td>
%   </tr>
%   <tr>
%     <td>centroid is a 2 element vector with the Y-X coordinates of the
%       centroid (following the row - column convention of the image
%       processing toolbox).</td>
%   </tr>
% </table>
% </html>
%
%%
% <html>
% <table>
%   <tr>
%     <th>angle -- angle of major axis (in image coordinate system)</th>
%   </tr>
%   <tr>
%     <td>real scalar</td>
%   </tr>
%   <tr>
%     <td>angle is a scalar indicating the angle between the first axis and
%       the horizontal axis. The value is in degrees, ranging from -90 to
%       90 degrees. Note that it expects an image based coordinate system
%       with origin at the top left corner. So for a normal cartesian
%       coordinate system the angle must be multiplied with -1.</td>
%   </tr>
% </table>
% </html>
%
%%
% <html>
% <table>
%   <tr>
%     <th>boundary -- boundary for which intersection should be computed</th>
%   </tr>
%   <tr>
%     <td>Nx2 real matrix</td>
%   </tr>
%   <tr>
%     <td>boundary is a Nx2 matrix where each row corresponds to the Y-X
%       coordinatesof one boundary point (following the row - column
%       convention of the image processing toolbox).</td>
%   </tr>
% </table>
% </html>
%
%% Output
%
% <html>
% <table>
%   <tr>
%     <th>majorPoints -- intersection points of axis</th>
%   </tr>
%   <tr>
%     <td>4x2 matrix</td>
%   </tr>
%   <tr>
%     <td>majorPoints is a 4x2 matrix where each row corresponds to the X-Y
%       coordinates of the intersection of the axis with the boundary. The
%       order is the following:
%       <ol>
%         <li>Right half plane intersection of the first axis (given by the
%           angle)</li>
%         <li>Left half plane intersection of the first axis (given by the
%           angle)</li>
%         <li>Right half plane intersection of the second axis
%           (perpendicular to the first)</li>
%         <li>Left half plane intersection of the second axis
%           (perpendicular to the first)</li>
%       </ol>
%     </td>
%   </tr>
% </table>
% </html>
%
%%
% Copyright (C) David Dreher 2015