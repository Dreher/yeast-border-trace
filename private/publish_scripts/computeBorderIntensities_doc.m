%% computeBorderIntensities
% computeBorderIntensities batch executes measureBorderIntensities on a
% given set of image files
%% Syntax
%   intensResults = computeBorderIntensities(segmentFilepath, signalFilepath, intensResFilepath, widthMembrane)
%   intensResults = computeBorderIntensities(___, rescaleIntens)
%
%% Description
% intensResults = computeBorderIntensities( segmentFilepath,
% signalFilepath, intensResFilepath, widthMembrane) batch executes
% measureBorderIntensities on all images identified by the two given
% filepaths. measureBorderIntensities computes the intensites along the
% border of objects identified by a set of segmentation images and it's
% results based on a corresponding set of signal images and reports those
% intensities and other associated image derived measurements.
%
% intRes = computeBorderIntensities(___, rescaleIntens) optionally
% rescales the intensity of each image according to the maximum intensity
% value.
% 
% Progress of the computation is reported to the console and results are
% saved to given folder.
%
%% Example
% Setup input and output paths with image and segmentation data
%
segmentFilepath = '../testData/Segmentation/*.tif';
signalFilepath = '../testData/Signal/*.tif';
intensResFilepath = '../testResults/intensity_results';
widthMembrane = 3;
[ intensResults ] = computeBorderIntensities( segmentFilepath, ...
    signalFilepath, intensResFilepath, widthMembrane)

%%%
% Plot an exemplary cell and the corresponding border intensity profile
%
figure;
subplot(2,1,1);
imshow(intensResults(1).RegionStats(10).signalImage, []);
subplot(2,1,2);
plot(intensResults(1).borderIntensity(10).maxIntensities)

%% Input
%
% <html>
% <table>
%   <tr>
%     <th>segmentFilepath -- filepath of segmentation images</th>
%   </tr>
%   <tr>
%     <td>character vector</td>
%   </tr>
%   <tr>
%     <td>Filepath identifying all input segmentation images. Allows for '*' as
%         a wildcard. Filenames in this and signalFilepath must be
%         corresponding in alphabetical order.</td>
%   </tr>
% </table>
% </html>
%
%%
% <html>
% <table>
%   <tr>
%     <th>signalFilepath -- filepath of segmentation images</th>
%   </tr>
%   <tr>
%     <td>character vector</td>
%   </tr>
%   <tr>
%     <td>Filepath identifying all input signal images. Allows for '*' as
%         a wildcard. Filenames in this and segmentFilepath must be
%         corresponding in alphabetical order.</td>
%   </tr>
% </table>
% </html>
%
%%
% <html>
% <table>
%   <tr>
%     <th>intensResFilepath -- filepath to saving location with base name</th>
%   </tr>
%   <tr>
%     <td>character vector</td>
%   </tr>
%   <tr>
%     <td>Filepath to the disk location where results should be stored, given
%         as path with a basic filename. Execution parameters will be saved
%         in a second file with the same basic filename and _parameters
%         appended.</td>
%   </tr>
% </table>
% </html>
%
%%
% <html>
% <table>
%   <tr>
%     <th>widthMembrane -- estimated width of the membrane</th>
%     </tr>
%   <tr>
%     <td>scalar positive integer</td>
%   </tr>
%   <tr>
%     <td>Estimation of membrane width in pixel values inwards from the
%         segmentation.</td>
%   </tr>
% </table>
% </html>
% 
%%
% <html>
% <table>
%   <tr>
%     <th>rescaleIntens -- optional flag to trigger rescaling</th>
%   </tr>
%   <tr>
%     <td>false (default) | bool scalar</td>
%   </tr>
%   <tr>
%     <td>If set to true each image will be rescaled so that 1 corresponds to
%         the image maximum intensity.</td>
%   </tr>
% </table>
% </html>
%
%% Output
% intensResults -- Nx1 result structure for N files containing the
% following fields:
%
% <html>
% <table>
%   <tr>
%     <th>Fields</th>
%     <th>Description</th>
%   </tr>
%   <tr>
%     <td>signalImageName</td>
%     <td>full path of the processed signal image file</td>
%   </tr>
%   <tr>
%     <td>segmentationImageName</td>
%     <td>full path of the processed segmentation image file</td>
%   </tr>
%   <tr>
%     <td>settings</td>
%     <td>vector with [widhtMembrane, pixels used for fit,
%     rescaleIntens]</td>
%   </tr>
%   <tr>
%     <td>borderIntensity</td>
%     <td>Mx1 strcuture for M objects in the image and the mean and max
%       intensities perpendicular to the middle ring in the fields:
%       <ul>
%         <li>meanIntensities</li>
%         <li>maxIntensities</li>
%       </ul>
%     </td>
%   </tr>
%   <tr>
%     <td>ConnectedComponents</td>
%     <td>Connected Components structure as returned by bwconncomp</td>
%   </tr>
%   <tr>
%     <td>RegionStats</td>
%     <td>Mx1 measurement structure for the found objects with the fields:
%       <ul>
%         <li>Area: Area of the object</li>
%         <li>Centroid: Centroid of the object</li>
%         <li>BoundingBox: Bounding box of the object</li>
%         <li>MajorAxisLength: Major axis of inertia ellipse</li>
%         <li>MinorAxisLength: Minor axis of inertia ellipse</li>
%         <li>Eccentricity: eccentricity of inertia ellipse</li>
%         <li>Orientation: Angle of major axis of inertia ellipse</li>
%         <li>Image: Subsampled image of object segmentation</li>
%         <li>PixelValues: Intensity values of object</li>
%         <li>MeanIntensity: Mean intensity of whole object</li>
%         <li>overlayImage: Overlay image of outer and middle border ring</li>
%         <li>majorPoints: Intersection of the inertia ellipse axis with the
%             estimted middle line of the membrane as a 4x2 matrix. Rows are the
%             points in the order 2 intersections with the major axis and then 2
%             intersections with the minor axis. Colums are X and Y coordinates.</li>
%         <li>majorPointsPerimeter: Intersection of the inertia ellipse axis with
%             the outer perimeter of the object, in the same form as above.</li>
%         <li>signalImage: Subsampled signal image of the object</li>
%         <li>skipped: String identifying the reason if an object has been
%             skipped, is empty otherwise</li>
%         <li>boundary: X-Y coordinates of the estimated middle ring of the
%             membrane</li>
%         <li>boundaryNpixels: Number of pixels that constitue the middle ring</li>
%         <li>boundaryLength: Euclidean length of the middle ring</li>
%       </ul>
%     </td>
%   </tr>
% </table>
% </html>
%
%% See Also
% <measureBorderIntensities.html measureBorderIntensities>

%%
% Copyright (C) David Dreher 2017