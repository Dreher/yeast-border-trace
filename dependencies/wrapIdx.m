function [ idx ] = wrapIdx( idx, N)
%WRAPIDX wraps index into N-sized array for Python style indexing
%   IDX = WRAPIDX(IDX; N) wraps the IDX into an N-sized array so that a
%   valid Matlab IDX is generated. IDX can be any integer: 0 corresponds
%   to the last element N, -1 to the N-1 element.
%
%   Example:
%       a = 1:10;
%       a(wrapIdx(-2:2, numel(a)))
%
%   returns the elements end-2, end-1, end, 1 , 2
%        8     9    10     1     2

%   Copyright 2016-2017 David Dreher
%   david.dreher@rocketmail.com


idx =  (1 + mod(idx-1, N));
end

