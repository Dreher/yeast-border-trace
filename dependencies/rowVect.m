function [ y ] = rowVect( x )
%ROWVECT Forces a row vector
%   Y = ROWVECT(X) returns X as a row vector. X can be any numeric or cell
%   array.
%
%   Example:
%       A = magic(3)
%       rowVect(A)
%   
%   returns the row vector:
%       8     3     4     1     5     9     6     7     2
%
%   See also COLVECT

%   Copyright 2016-2017 David Dreher
%   david.dreher@rocketmail.com

assert(isnumeric(x) | iscell(x), 'rowVect:type', 'Numeric or cell array expected')

y = reshape(x, 1, numel(x));
end

