function [ y ] = colVect( x )
%COLVECT Forces a column vector
%   Y = COLVECT(X) returns X as a column vector. X can be any numeric or
%   cell array. 
%
%   Example:
%       A = magic(3)
%       colVect(A)
%   
%   returns the column vector:
%       8
%       3
%       4
%       1
%       5
%       9
%       6
%       7
%       2
%
%   See also ROWVECT

%   Copyright 2016-2017 David Dreher
%   david.dreher@rocketmail.com

assert(isnumeric(x) | iscell(x), 'colVect:type', 'Numeric or cell array expected')

y = x(:);

end

