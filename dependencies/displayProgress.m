function displayProgress(progress, message)
%displayProgress is a simple function to display progress of loops or other
%continous calculation
%   displayProgress(progress, message) prints a timestamp, a percentage of
%   progress and an optional message using the Matlab fileexchange function
%   dispstat to continously overwrite previous messages.
%   
%   DISPSTAT from <a href="www.mathworks.com/matlabcentral/fileexchange/44673-overwritable-message-outputs-to-commandline-window">dispstat</a>.
%
%   Prior to loop the state of the progress monitor must be initalized by
%   calling 'init' as progress input:
%       displayProgress('init', < 'Init message' >)
%   After initialization the progress input must be a scalar between 0 and
%   1 for progress percentage:
%       displayProgress(0.5);
%   To close the progress monitor state after the loop supply 'end' as
%   progress:
%       displayProgress('end', < 'End message' >)
%
%   Example:
%       computationName = 'sample computation';
%       displayProgress('init', computationName)
%       N = 10;
%       for t = 1:N
%           pause(0.1)
%           displayProgress(t/N)
%       end
%       displayProgress('end', computationName)
%
%   See also DISPSTAT

%   Copyright 2016-2017 David Dreher
%   david.dreher@rocketmail.com

if ~exist('message', 'var') || ~ischar(message)
    message = '';
end
    
if isscalar(progress)
    dispstat(sprintf('Processed %.1f%%', round(progress*100,1)), 'timestamp');
elseif ischar(progress)
    switch lower(progress)
        case 'init'
            dispstat('', 'init');
            dispstat(['Begin ', message], 'keepthis', 'timestamp');
        case 'end'
            dispstat(['Finished ', message], 'keepprev');
        otherwise
            error('Unknown input for percentage')
    end
else
    error('Unknown input for percentage')
end

                

end
