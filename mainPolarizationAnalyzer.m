% Copyright (C) David Dreher 2017

ybt.prepareWorkspace();
resultFolder = 'testResults/';
%% Define widthMembrane and input location
signalFilepath = 'testData/Signal/*.tif';
segmentationFilepath = 'testData/Segmentation/*.tif';
intensityResFilepath = [resultFolder, 'intensity_results'];
widthMembrane = 3; % Estimated pixel width of membrane

% Optional:
rescaleIntensities = false;
detectionDirection = 'inside';
%% Compute border intensity results and object measurements
% With all optional inputs:
intensityResults  = ybt.computeBorderIntensities( segmentationFilepath, ...
    signalFilepath, intensityResFilepath, widthMembrane, rescaleIntensities, ...
    detectionDirection );

% % No optional inputs:
% intensityResults  = computeBorderIntensities( segmentationFilepath, ...
%     signalFilepath, intensityResFilepath, widthMembrane );
% % Just one optional input:
% intensityResults  = computeBorderIntensities( segmentationFilepath, ...
%     signalFilepath, intensityResFilepath, widthMembrane, detectionDirection );
% intensityResults  = computeBorderIntensities( segmentationFilepath, ...
%     signalFilepath, intensityResFilepath, widthMembrane, rescaleIntensities );
%%
spotResFilepath = [resultFolder, 'spot_results'];
% Optionally you can adopt this to load a paramert file from previous
% computations
PeakOptions.function =  'ybt.findpeaksWrapper';
PeakOptions.GUI = 'findpeaksWrapperGUI';
PeakOptions.correctBaseline = true;

% Options for the findpeaks function, Options can be added and deleted at
% will, see: doc findpeaks
FinderOptions.MinPeakHeight = 100;
FinderOptions.MinPeakProminence = 10;
FinderOptions.MinPeakDistance = 20;
FinderOptions.MinPeakWidth = 20;
PeakOptions.FinderOptions = FinderOptions;

% Width options to compute the width of peaks
WidthOptions.reference = 'HeightCorr'; %Base for width computation, options are 'Height', 'Prominence', 'HeightCorr'
WidthOptions.refLevel = 0.8;
WidthOptions.significantLocalDepth = 20;
WidthOptions.significantMaxHeight = 3;
PeakOptions.WidthOptions = WidthOptions;

% Options to plot intensities are: 'xy', 'heatmap' and 'none'
plotSmoothedIntensities = 'none';

% Implemented filters are:
% 'average' with the parameter: length
% 'gaussian' with the parameters: length, sigma
% 'lowpass' with the parameter: frequencyCutoff
% 'none': no filtering
FilterOptions.type = 'lowpass';
FilterOptions.frequencyCutoff = 0.025; % frequency cutoff is between 0 and 1, the lower the number the more smoothing

% Polarization weights as a cell array of 4-element vectors.
% First weight reflects importance of mean intensity
% Second weight reflects importance of size of spots
% Third weight reflects importance of smoothness of spots
% Fourth weight reflects importance of max intensity
% Weights can any positive number or zero for ignoring the feature
% Values smaller than 1 correspond to diminishing returns, while values
% larger than 1 correspond to increasing returns
polarizationWeights = [1,1,0, 0];%{[1,1,0, 0], [1,1,1,0], [0,1,1,1]};

% polarizationIntensityTypes: Type of intensity used for calculating the
% polarization ratio, values: 'S' or 'U'
% 'U' triggers unsmoothed intensity values, 'S' triggers smoothed intensity
% values. The first character corresponds to the intensity values used for
% the mean computation the second character corresponds to the intensity
% values used for the smoothness computation and the third for the one used
% for the max intensity values.
polarizationIntensityTypes = 'SUS';

% Bool to choose whether to use the GUI or not
useGUI = false;

%% Run spot detection and classification
[spotResults, FilterOptions, PeakOptions, polarWeights, polarIntTypes ] = ...
    ybt.computeSpotResults( intensityResults, FilterOptions, PeakOptions, ...
    spotResFilepath, plotSmoothedIntensities, polarizationWeights, ...
    polarizationIntensityTypes, useGUI);

%% Set output settings
% Outputs are grouped by the combination of all supplied tokens. Hence
% supplied metadata tokens should capture all information for different
% conditions
searchExpr = 'Test_\d{8}_(\d)_(\d+)_(.*)_(\d\d)_.*.tif';
tokenKeys = {'Timepoint', 'Strain No', 'Treatment', 'Temperature'};
finalOutputFilepath = [resultFolder, 'Test_Output'];
% Ratio for which cells are classified as polarized
ThresholdRatioPolarization = 0.8;

% Optional inputs
tokenKeyNumbering = {{}, {}, {'nodrug', 'DMSO', 'BFA'}, {}};
additionalOutput = 'csv';
additionaOutputOptions = {'Delimiter', '\t'};

%% Create grouped output based on metadata conditions
% With all optional inputs:
[outputSpots, outputObjects] = ybt.manageOutput(intensityResults, spotResults, ...
    searchExpr, tokenKeys, finalOutputFilepath, ThresholdRatioPolarization, ...
    tokenKeyNumbering, additionalOutput, additionaOutputOptions);

% % With only token key map:
% [outputSpots, outputObjects] = manageOutput(intensityResults, spotResults, ...
%     searchExpr, tokenKeys, finalOutputFilepath, ThresholdRatioPolarization, ...
%     tokenKeyNumbering);
% % With only additional output (additional Options are also optional):
% [outputSpots, outputObjects] = manageOutput(intensityResults, spotResults, ...
%     searchExpr, tokenKeys, finalOutputFilepath, ThresholdRatioPolarization, ...
%     additionalOutput[, additionaOutputOptions]);
% % With token key map and additional output:
% [outputSpots, outputObjects] = manageOutput(intensityResults, spotResults, ...
%     searchExpr, tokenKeys, finalOutputFilepath, ThresholdRatioPolarization, ...
%     tokenKeyNumbering, additionalOutput[, additionaOutputOptions]);