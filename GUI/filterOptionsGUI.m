function varargout = filterOptionsGUI(varargin)
% FILTEROPTIONSGUI MATLAB code for filterOptionsGUI.fig
%      FILTEROPTIONSGUI, by itself, creates a new FILTEROPTIONSGUI or raises the existing
%      singleton*.
%
%      H = FILTEROPTIONSGUI returns the handle to a new FILTEROPTIONSGUI or the handle to
%      the existing singleton*.
%
%      FILTEROPTIONSGUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in FILTEROPTIONSGUI.M with the given input arguments.
%
%      FILTEROPTIONSGUI('Property','Value',...) creates a new FILTEROPTIONSGUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before filterOptionsGUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to filterOptionsGUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help filterOptionsGUI

% Last Modified by GUIDE v2.5 15-Aug-2017 16:06:08

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @filterOptionsGUI_OpeningFcn, ...
                   'gui_OutputFcn',  @filterOptionsGUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before filterOptionsGUI is made visible.
function filterOptionsGUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to filterOptionsGUI (see VARARGIN)

% Choose default command line output for filterOptionsGUI
handles.output = []; %hObject;
set(hObject,'WindowStyle','modal');
if numel(varargin) > 0
    %handles.mainGUI = varargin{1};
    handles.FilterOptions = parseDefaultFilterOpt(varargin{1});
else
    handles.FilterOptions = parseDefaultFilterOpt(struct);
end
handles.popupFilterType.Value = parseFilterType(handles.FilterOptions.type);

% Update handles structure
guidata(hObject, handles);

setUIOpts(hObject);
updateGUI(hObject);
% UIWAIT makes filterOptionsGUI wait for user response (see UIRESUME)
uiwait(handles.figFilterMain);

function output = parseFilterType(input)

if ischar(input)
    switch lower(input)
        case 'gaussian'
            output = 2;
        case 'lowpass'
            output = 1;
        case 'average'
            output = 3;
        case {'sgolay', 'savitzky-golay'}
            output = 4;
        case 'swt'
            output = 5;
        case 'none'
            output = 6;
        otherwise
            error('parseFilterType:UnknownString', 'Input must be a valid string')
    end
elseif isscalar(input)
    switch double(input)
        case 1
            output = 'lowpass';
        case 2
            output = 'gaussian';
        case 3
            output = 'average';
        case 4
            output = 'sgolay';
        case 5
            output = 'swt';
        case 6
            output = 'none';
        otherwise
            error('parseFilterType:UnknownScalar', 'Input must be a valid scalar')
    end
else
    error('parseFilterType:UnknownInput', 'Input must be a scalar or string')
end

function FilterOptions = parseDefaultFilterOpt(FilterOptions)
if ~isfield(FilterOptions, 'type')
    FilterOptions.type = 'lowpass';
elseif isscalar(FilterOptions.type)
    FilterOptions.type = parseFilterType(FilterOptions.type);
end

switch FilterOptions.type
    case 'lowpass'
        if ~isfield(FilterOptions, 'frequencyCutoff')
            FilterOptions.frequencyCutoff = 0.05;
        end
        FilterOptions = rmfield(FilterOptions, ...
            setdiff(fieldnames(FilterOptions), {'type', 'frequencyCutoff'}));
    case 'gaussian'
        if ~isfield(FilterOptions, 'length')
            FilterOptions.length = 100;
        end
        if ~isfield(FilterOptions, 'sigma')
            FilterOptions.sigma = 10;
        end
        FilterOptions = rmfield(FilterOptions, ...
            setdiff(fieldnames(FilterOptions), {'type', 'length', 'sigma'}));
    case 'average'
        if ~isfield(FilterOptions, 'length')
            FilterOptions.length = 100;
        end
        FilterOptions = rmfield(FilterOptions, ...
            setdiff(fieldnames(FilterOptions), {'type', 'length'}));
    case 'sgolay'
        if ~isfield(FilterOptions, 'framelen')
            FilterOptions.framelen = 51;
        end
        if ~isfield(FilterOptions, 'order')
            FilterOptions.order = 3;
        end
        FilterOptions = rmfield(FilterOptions, ...
            setdiff(fieldnames(FilterOptions), {'type', 'framelen', 'order'}));
    case 'swt'
        if ~isfield(FilterOptions, 'level')
            FilterOptions.level = 5;
        end
        if ~isfield(FilterOptions, 'waveletType')
            FilterOptions.waveletType = 'haar';
        end
        if ~isfield(FilterOptions, 'thresholds')
            FilterOptions.thresholds = zeros(1, FilterOptions.level);
        end
        if ~isfield(FilterOptions, 'thrshType')
            FilterOptions.thrshType = 'h';
        end
        if ~isfield(FilterOptions, 'autoThrsh')
            FilterOptions.autoThrsh = false;
        end
        if ~isfield(FilterOptions, 'autoThrshMethod')
            FilterOptions.autoThrshMethod = 'sqtwolog';
        end
        if ~isfield(FilterOptions, 'autoThrshAlfa')
            FilterOptions.autoThrshAlfa = 'one';
        end
        FilterOptions = rmfield(FilterOptions, ...
            setdiff(fieldnames(FilterOptions), {'type', 'level', 'waveletType', ...
            'thresholds', 'thrshType', 'autoThrsh', 'autoThrshMethod', 'autoThrshAlfa'}));
    case 'none'
        FilterOptions = rmfield(FilterOptions, setdiff(fieldnames(FilterOptions), {'type'}));
    otherwise
        
end

function errMsg = setFilterOption(hObject, property, value)
errMsg = {};
handles = guidata(hObject);
if strcmpi(property, 'type')
    if ~strcmpi(handles.FilterOptions.type, value)
        try 
            if isscalar(value)
                value = parseFilterType(value);
            else
                parseFilterType(value);            
            end
            handles.FilterOptions.type = value;
            handles.FilterOptions = parseDefaultFilterOpt(handles.FilterOptions);
            guidata(hObject, handles);
            setUIOpts(hObject);
            updateGUI(hObject);
        catch ME
            errMsg = {ME.identifier, ME.message};
        end
    end
    
else
    switch handles.FilterOptions.type
        case {1, 'lowpass'}
            errMsg = setLowpassFilterOpt(hObject, property, value);
        case {2, 'gaussian'}
            errMsg = setGaussFilterOpt(hObject, property, value);
        case {3, 'average'}
            errMsg = setAvgFilterOpt(hObject, property, value);
        case {4, 'sgolay'}
            errMsg = setSgolayFilterOpt(hObject, property, value);
        case {5, 'swt'}
            errMsg = setSWTFilterOpt(hObject, property, value);
        case {6, 'none'}
            error('setFilterOption:setNone', 'Cannot set property of no filtering');
        otherwise
            error('setFilterOption:unknownType', 'Filter type is not known, internal error');
    end
end

function errMsg = setLowpassFilterOpt(hObject, property, value)
errMsg = {};
handles = guidata(hObject);
validateattributes(property, {'char'}, {'nonempty'}, 'setLowpassFilterOpt', 'property', 2);
property = validatestring(property, {'frequencyCutoff'}, 'setLowpassFilterOpt', 'property', 2);
if strcmpi(property, 'frequencyCutoff')
    try
        if ischar(value)
            value = str2double(value);
        end
        attr = {'scalar', 'real', 'finite', '>', 0, '<', 1};
        validateattributes(value, {'numeric'}, attr);
        handles.FilterOptions.(property) = value;
    catch
        errMsg = {'Frequency cutoff must be a positive, real, finite scalar between 0 and 1'};
    end
else
    error('setLowpassFilterOpt:unknownProperty', 'Internal error: unknown property')
end
guidata(hObject, handles);

function errMsg = setGaussFilterOpt(hObject, property, value)
errMsg = {};
handles = guidata(hObject);
validateattributes(property, {'char'}, {'nonempty'}, 'setGaussFilterOpt', 'property', 2);
property = validatestring(property, {'length', 'sigma'}, 'setGaussFilterOpt', 'property', 2);
if strcmpi(property, 'length')
    try
        if ischar(value)
            value = str2double(value);
        end
        attr = {'scalar', 'real', 'finite', 'positive', 'integer'};
        validateattributes(value, {'numeric'}, attr);
        handles.FilterOptions.(property) = value;
    catch
        errMsg = {'Filter length must be a positive integer'};
    end
elseif strcmpi(property, 'sigma')
    try
        if ischar(value)
            value = str2double(value);
        end
        attr = {'scalar', 'real', 'finite', 'positive'};
        validateattributes(value, {'numeric'}, attr);
        handles.FilterOptions.(property) = value;
    catch
        errMsg = {'Sigma must be a positive, real, finite scalar'};
    end
else
    error('setGaussFilterOpt:unknownProperty', 'Internal error: unknown property')
end
guidata(hObject, handles);

function errMsg = setAvgFilterOpt(hObject, property, value)
errMsg = {};
handles = guidata(hObject);
validateattributes(property, {'char'}, {'nonempty'}, 'setAvgFilterOpt', 'property', 2);
property = validatestring(property, {'length'}, 'setAvgFilterOpt', 'property', 2);
if strcmpi(property, 'length')
    try
        if ischar(value)
            value = str2double(value);
        end
        attr = {'scalar', 'real', 'finite', 'positive', 'integer'};
        validateattributes(value, {'numeric'}, attr);
        handles.FilterOptions.(property) = value;
    catch
        errMsg = {'Filter length must be a positive integer'};
    end
else
    error('setAvgFilterOpt:unknownProperty', 'Internal error: unknown property')
end
guidata(hObject, handles);

function errMsg = setSgolayFilterOpt(hObject, property, value)
errMsg = {};
handles = guidata(hObject);
validateattributes(property, {'char'}, {'nonempty'}, 'setSgolayFilterOpt', 'property', 2);
property = validatestring(property, {'framelen', 'order'}, 'setSgolayFilterOpt', 'property', 2);
if strcmpi(property, 'framelen')
    try
        if ischar(value)
            value = str2double(value);
        end
        attr = {'scalar', 'real', 'finite', 'positive', 'integer', 'odd'};
        validateattributes(value, {'numeric'}, attr);
        handles.FilterOptions.(property) = value;
    catch
        errMsg = {'Frame length must be a positive, odd integer'};
    end
elseif strcmpi(property, 'order')
    try
        if ischar(value)
            value = str2double(value);
        end
        attr = {'scalar', 'real', 'finite', 'positive', 'integer'};
        validateattributes(value, {'numeric'}, attr);
        handles.FilterOptions.(property) = value;
    catch
        errMsg = {'Order must be a positive integer'};
    end
else
    error('setSgolayFilterOpt:unknownProperty', 'Internal error: unknown property')
end
guidata(hObject, handles);

function errMsg = setSWTFilterOpt(hObject, property, value)
errMsg = {};
handles = guidata(hObject);
validateattributes(property, {'char'}, {'nonempty'}, 'setSgolayFilterOpt', 'property', 2);
property = validatestring(property, {'level', 'waveletType', 'thresholds', ...
    'thrshType', 'autoThrsh', 'autoThrshMethod', 'autoThrshAlfa'}, 'setSgolayFilterOpt', 'property', 2);
switch property
    case 'level'
        try
            if ischar(value)
                value = str2double(value);
            end
            attr = {'scalar', 'real', 'finite', 'positive', 'integer', '<=', 8};
            validateattributes(value, {'numeric'}, attr);
            handles.FilterOptions.(property) = value;
            currThrsh = handles.FilterOptions.thresholds;
            if numel(currThrsh) < value
                handles.FilterOptions.thresholds = ...
                    [currThrsh(:).', zeros(1, value - numel(currThrsh))];
            else
                handles.FilterOptions.thresholds = currThrsh(1:value);
            end
        catch
            errMsg = {'Level must be a positive integer between 1 and 8'};
        end
    case 'waveletType'
        try
            attr = {'nonempty'};
            validateattributes(value, {'char'}, attr);
            % Check if we can create the filter
            wfilters(value);
            handles.FilterOptions.(property) = value;
        catch
            errMsg = {'Wavelet type must be a valid wavelet type, see wfilters for details'};
        end
    case 'thresholds'
        try
            if iscell(value)
                value = cell2mat(value);
            end
            attr = {'real', 'nonnegative', 'nonnan', 'numel', handles.FilterOptions.level};
            validateattributes(value, {'numeric'}, attr);
            handles.FilterOptions.(property) = value;
        catch
            errMsg = {'Thresholds must be a vector with real valued entries one for each level'};
        end
    case 'thrshType'
        try
            if isnumeric(value) || islogical(value)
                if value == 0
                    value = 's';
                elseif value == 1
                    value = 'h';
                else
                    error('setSWTFilterOpt:invalidThrshType', 'Internal error: invalid thrshType value')
                end
            end
            attr = {'nonempty', 'scalar'};
            validateattributes(value, {'char'}, attr);
            value = validatestring(value, {'h', 's'});
            handles.FilterOptions.(property) = value;
        catch
            errMsg = {'thrshType must be either ''h'' (hard) or ''s'' (soft)'};
        end
        
    case 'autoThrsh'
        try
            if ischar(value)
                value = str2double(value);
            end
            attr = {'scalar', 'binary'};
            validateattributes(value, {'numeric', 'logical'}, attr);
            handles.FilterOptions.(property) = value;
        catch
            errMsg = {'autoThrsh must be true or false'};
        end
    case 'autoThrshMethod'
        try
            attr = {'nonempty'};
            validateattributes(value, {'char'}, attr);
            validMethods = {'sqtwolog', 'rigrsure', 'heursure', 'minimaxi', ...
                'penalhi', 'penalme', 'penallo'};
            % Check if we can create the filter
            value = validatestring(value, validMethods);
            handles.FilterOptions.(property) = value;
        catch
            errMsg = {['autoThrshMethod must be one of the following methods: ', ...
                'sqtwolog, rigrsure, heursure, minimaxi, penalhi, penalme, penallo']};
        end
    case 'autoThrshAlfa'
        try
            validSCAL = {'one', 'sln', 'mln'};
            if ischar(value) && ~any(strcmpi(value, validSCAL))
                value = str2double(value);
                attr = {'scalar', 'real', '>', 1};
                validateattributes(value, {'numeric'}, attr);
            end
            handles.FilterOptions.(property) = value;
        catch
            errMsg = {['autoThrshAlfa type must be a either one of the ', ...
                'following: one, sln, mln or a real scalar larger than 1']};
        end
    otherwise
        error('setSWTFilterOpt:unknownProperty', 'Internal error: unknown property')
end
guidata(hObject, handles);

function setUIOpts(hObject)
handles = guidata(hObject);
switch handles.FilterOptions.type
    case {1, 'lowpass'}
        setLowpassUIOpts(hObject);
    case {2, 'gaussian'}
        setGaussUIOpts(hObject);
    case {3, 'average'}
        setAvgUIOpts(hObject);
    case {4, 'sgolay'}
        setSgolayUIOpts(hObject);
    case {5, 'swt'}
        setSwtUIOpts(hObject);
    case {6, 'none'}
        handles.UIOpts = struct();
        guidata(hObject, handles);
    otherwise
        error('setFilterOption:unknownType', 'Filter type is not known, internal error');
end

function setLowpassUIOpts(hObject)
handles = guidata(hObject);
newStringOpts = struct('Description', {'Frequency'}, ...
    'PropertyName', {'frequencyCutoff'});
newUIOpts = struct('StringOpts', newStringOpts);
handles.UIOpts = newUIOpts;
guidata(hObject, handles);

function setGaussUIOpts(hObject)
handles = guidata(hObject);
newStringOpts = struct('Description', {'Length', 'Sigma'}, ...
    'PropertyName', {'length', 'sigma'});
newUIOpts = struct('StringOpts', newStringOpts);
handles.UIOpts = newUIOpts;
guidata(hObject, handles);

function setAvgUIOpts(hObject)
handles = guidata(hObject);
newStringOpts = struct('Description', {'Length'}, ...
    'PropertyName', {'length'});
newUIOpts = struct('StringOpts', newStringOpts);
handles.UIOpts = newUIOpts;
guidata(hObject, handles);

function setSgolayUIOpts(hObject)
handles = guidata(hObject);
newStringOpts = struct('Description', {'Frame length', 'Order'}, ...
    'PropertyName', {'framelen', 'order'});
newUIOpts = struct('StringOpts', newStringOpts);
handles.UIOpts = newUIOpts;
guidata(hObject, handles);

function setSwtUIOpts(hObject)
handles = guidata(hObject);
newStringOpts = struct('Description', {'Wavelet type', 'Level', ...
    'Auto thrsh method', 'Alpha/SCAL'}, ...
    'PropertyName', {'waveletType', 'level', 'autoThrshMethod', 'autoThrshAlfa'});
newCheckOpts = struct('Description', {'Hard thrsh', 'Auto threshold'}, ...
    'PropertyName', {'thrshType', 'autoThrsh'}, 'ConvertFunc', ...
    {@(x) strcmpi(x,'h'), @(x) x~=0});
newTableOpts = struct('Description', {'Thresholds'}, 'PropertyName',{'thresholds'});
newUIOpts = struct('StringOpts', newStringOpts, 'CheckOpts', newCheckOpts, 'TableOpts', newTableOpts);
handles.UIOpts = newUIOpts;
guidata(hObject, handles);



% function setCurrUIOpt(handles, type, number, arguments)
% validateattributes(type, {'char'}, {'numel', 1}, 'setCurrOpt', 'type', 1);
% validateattributes(number, {'numeric'}, {'integer', 'positive'}, 'setCurrOpt', 'number', 2);
% validateattributes(arguments, {'cell'}, {'nonempty', 'numel', 2}, 'setCurrOpt', 'arguments', 3);
% % assert(numel(arguments)>=4, 'setCurrOpt:tooFewArguments', ...
% %     ['Too few arguments supplied for setting current options, must be at '...
% %     'least:Description, Value, Property name and validate function'])
% switch lower(type)
%     case 's'
%         propName = 'StringOpts';
%     case 'c'
%         propName = 'CheckOpts';
%     case 't'
%         propName = 'TableOpts';
%     otherwise
%         error('setCurrOpt:unknownOpttype', ...
%                     'An unknown type for Opt/UI elements was supplied')
% end
% handles.UIopts.(propName)(number).Description = arguments{1};
% handles.UIopts.(propName)(number).PropertyName = arguments{2};

function c = countUIControls(handles, type)
hCell=struct2cell(handles);
c=0;
for i=1:length(hCell)
    if isa(hCell{i}, 'matlab.ui.control.UIControl') && strcmp(get(hCell{i},'Style'), type)
        c=c+1;
    elseif strcmpi(type, 'table') && isa(hCell{i}, 'matlab.ui.control.Table') 
        c = c+1;
    end
end

    

function updateGUI(hObject)
handles = guidata(hObject);
FilterOptions = handles.FilterOptions;
UIOpts = handles.UIOpts;
nStringOpts = countUIControls(handles, 'edit');
for iO = 1:nStringOpts
    try
        handles.(sprintf('txtStringOpt%d', iO)).String = UIOpts.StringOpts(iO).Description;
        handles.(sprintf('txtStringOpt%d', iO)).Enable = 'on';
        if isfield(UIOpts.StringOpts(iO), 'ConvertFunc')
            val = FilterOptions.(UIOpts.StringOpts(iO).PropertyName);
            func = UIOpts.StringOpts(iO).ConvertFunc;
            handles.(sprintf('editTxtOpt%d', iO)).String = func(val);
        else
            handles.(sprintf('editTxtOpt%d', iO)).String = ...
                num2str( FilterOptions.(UIOpts.StringOpts(iO).PropertyName));
        end
        handles.(sprintf('editTxtOpt%d', iO)).Enable = 'on';
    catch ME
        handles.(sprintf('txtStringOpt%d', iO)).String = '-';
        handles.(sprintf('txtStringOpt%d', iO)).Enable = 'off';
        handles.(sprintf('editTxtOpt%d', iO)).String = '-';
        handles.(sprintf('editTxtOpt%d', iO)).Enable = 'off';
    end
end

nTableOpts = countUIControls(handles, 'table');
for iO = 1:nTableOpts
    try
        handles.(sprintf('txtTableOpt%d', iO)).String = UIOpts.TableOpts(iO).Description;
        handles.(sprintf('txtTableOpt%d', iO)).Enable = 'on';
        vals = FilterOptions.(UIOpts.TableOpts(iO).PropertyName);
        if isnumeric(vals)
            handles.(sprintf('tableOpt%d', iO)).ColumnFormat = {'numeric'};
        else
            handles.(sprintf('tableOpt%d', iO)).ColumnFormat = {'char'};
        end
        if isfield(UIOpts.TableOpts(iO), 'ConvertFunc')
            func = UIOpts.TableOpts(iO).ConvertFunc;
        else
            func = @(x) reshape(x, [numel(x), 1]);
        end
        handles.(sprintf('tableOpt%d', iO)).Data = func(vals);
        handles.(sprintf('tableOpt%d', iO)).Enable = 'on';
        handles.(sprintf('tableOpt%d', iO)).ColumnEditable = true;
    catch ME
        handles.(sprintf('txtTableOpt%d', iO)).String = '-';
        handles.(sprintf('txtTableOpt%d', iO)).Enable = 'off';
        handles.(sprintf('tableOpt%d', iO)).Data = {};
        handles.(sprintf('tableOpt%d', iO)).ColumnEditable = false;
        handles.(sprintf('tableOpt%d', iO)).Enable = 'off';
    end
end

nCheckOpts = countUIControls(handles, 'checkbox');
for iO = 1:nCheckOpts
    try
        handles.(sprintf('txtCheckOpt%d', iO)).String = UIOpts.CheckOpts(iO).Description;
        handles.(sprintf('txtCheckOpt%d', iO)).Enable = 'on';
        if isfield(UIOpts.CheckOpts(iO), 'ConvertFunc')
            val = FilterOptions.(UIOpts.CheckOpts(iO).PropertyName);
            func = UIOpts.CheckOpts(iO).ConvertFunc;
            handles.(sprintf('checkOpt%d', iO)).Value = func(val);
        else
            handles.(sprintf('checkOpt%d', iO)).Value = FilterOptions.(UIOpts.CheckOpts(iO).PropertyName);
        end
        handles.(sprintf('checkOpt%d', iO)).Enable = 'on';
    catch ME
        handles.(sprintf('txtCheckOpt%d', iO)).String = '-';
        handles.(sprintf('txtCheckOpt%d', iO)).Enable = 'off';
        handles.(sprintf('checkOpt%d', iO)).Value = 0;
        handles.(sprintf('checkOpt%d', iO)).Enable = 'off';
    end
end

% switch handles.popupFilterType.Value
%     case 1
%         handles.txtStringOpt1.String = 'Cutoff frequency:';
%         handles.txtStringOpt1.Enable = 'on';
%         handles.editTxtOpt1.String = num2str(handles.FilterOptions.frequencyCutoff);
%         handles.editTxtOpt1.Enable = 'on';
%         
%         handles.txtStringOpt2.String = '-';
%         handles.txtStringOpt2.Enable = 'off';
%         handles.editTxtOpt2.String = '-';
%         handles.editTxtOpt2.Enable = 'off';
%     case 2
%         handles.txtStringOpt1.String = 'Length:';
%         handles.txtStringOpt1.Enable = 'on';
%         handles.editTxtOpt1.String = num2str(handles.FilterOptions.length);
%         handles.editTxtOpt1.Enable = 'on';
%         
%         handles.txtStringOpt2.String = 'Sigma:';
%         handles.txtStringOpt2.Enable = 'on';
%         handles.editTxtOpt2.String = num2str(handles.FilterOptions.sigma);
%         handles.editTxtOpt2.Enable = 'on';
%     case 3
%         handles.txtStringOpt1.String = 'Length:';
%         handles.txtStringOpt1.Enable = 'on';
%         handles.editTxtOpt1.String = num2str(handles.FilterOptions.length);
%         handles.editTxtOpt1.Enable = 'on';
%         
%         handles.txtStringOpt2.String = '-';
%         handles.txtStringOpt2.Enable = 'off';
%         handles.editTxtOpt2.String = '-';
%         handles.editTxtOpt2.Enable = 'off';
%     case 4
%         handles.txtStringOpt1.String = '-';
%         handles.txtStringOpt1.Enable = 'off';
%         handles.editTxtOpt1.String = '-';
%         handles.editTxtOpt1.Enable = 'off';
%         
%         handles.txtStringOpt2.String = '-';
%         handles.txtStringOpt2.Enable = 'off';
%         handles.editTxtOpt2.String = '-';
%         handles.editTxtOpt2.Enable = 'off';
%     otherwise
%         error('updateGUI:UnknownValue', 'Popmenu returns unknown value');
% end

% function disableUnusedOpts(handles, varargin)
% 
% for iInp = 1:numel(varargin)
%     currID = varargin{iInp};
%     typeChar = currID(1);
%     uiElemList = checkValidOptUI(currID(2:end));
%     for iElem = 1:numel(uiElemList)
%         switch lower(typeChar)
%             case 's' % String boxes
%                 txtName = sprintf('txtOpt%d', uiElemList(iElem));
%                 editTxtName = sprintf('editTxtOpt%d', uiElemList(iElem));
%                 handles.(txtName).String = '-';
%                 handles.(txtName).Enable = 'off';
%                 handles.(editTxtName).String = '-';
%                 handles.(editTxtName).Enable = 'off';
%             case 'c' % Check boxes
%                 txtName = sprintf('txtOptCheck%d', uiElemList(iElem));
%                 boxName = sprintf('checkOpt%d', uiElemList(iElem));
%                 handles.(txtName).String = '-';
%                 handles.(txtName).Enable = 'off';
%                 handles.(boxName).Enable = 'off';
%             case 't' % Table 
%                 txtName = sprintf('txtOptTable%d', uiElemList(iElem));
%                 boxName = sprintf('tableOpt%d', uiElemList(iElem));
%                 handles.(txtName).String = '-';
%                 handles.(txtName).Enable = 'off';
%                 handles.(boxName).Data = cell(1,1);
%                 handles.(boxName).ColumnEditable = false;
%                 handles.(boxName).Enable = 'off';
%             otherwise
%                 error('disableUnusedOpts:unknownUItype', ...
%                     'An unknown type for UI elements was supplied')
%         end
%             
%     end
% end

% function validNumbers = checkValidOptUI(strA)
% isvalid = all(isstrprop(strA, 'digit') | ...
%     strA==':' | strA==' ' | strA==',' | strA=='[' | strA==']');
% if isvalid
%     try 
%         validNumbers = eval(strA);
%     catch
%         error('checkValidOptUI:unknownUIOptList', ...
%             'An unknown opt list for ui elements was supplied')
%     end
% else
%     error('checkValidOptUI:unknownUIOptList', ...
%             'An unknown opt list for ui elements was supplied')
% end


function setStringOpt(hObject, ii)
handles = guidata(hObject);
validateattributes(ii, {'numeric'}, {'scalar', 'integer', 'positive', ...
    '<=', countUIControls(handles, 'edit')}, 'setStringOpt', 'ii', 2);
newStr = get(hObject,'String');
UIOpts = handles.UIOpts;
errMsg = setFilterOption(hObject, UIOpts.StringOpts(ii).PropertyName, newStr);
if ~isempty(errMsg)
    h = errordlg(errMsg{:}, 'Invalid value', 'modal');
    uiwait(h);
end
updateGUI(hObject);

function setTableOpt(hObject, ii)
handles = guidata(hObject);
validateattributes(ii, {'numeric'}, {'scalar', 'integer', 'positive', ...
    '<=', countUIControls(handles, 'table')}, 'setStringOpt', 'ii', 2);
newData = get(hObject,'Data');
UIOpts = handles.UIOpts;
errMsg = setFilterOption(hObject, UIOpts.TableOpts(ii).PropertyName, newData);
if ~isempty(errMsg)
    h = errordlg(errMsg{:}, 'Invalid value', 'modal');
    uiwait(h);
end
updateGUI(hObject);

function setCheckOpt(hObject, ii)
handles = guidata(hObject);
validateattributes(ii, {'numeric'}, {'scalar', 'integer', 'positive', ...
    '<=', countUIControls(handles, 'checkbox')}, 'setStringOpt', 'ii', 2);
newValue = get(hObject,'Value');
UIOpts = handles.UIOpts;
errMsg = setFilterOption(hObject, UIOpts.CheckOpts(ii).PropertyName, newValue);
if ~isempty(errMsg)
    h = errordlg(errMsg{:}, 'Invalid value', 'modal');
    uiwait(h);
end
updateGUI(hObject);

% --- Outputs from this function are returned to the command line.
function varargout = filterOptionsGUI_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;
delete(hObject);


% --- Executes on selection change in popupFilterType.
function popupFilterType_Callback(hObject, eventdata, handles)
% hObject    handle to popupFilterType (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupFilterType contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupFilterType
errMsg = setFilterOption(hObject, 'type', hObject.Value);
if ~isempty(errMsg)
    error(errMsg{:});
end


% --- Executes during object creation, after setting all properties.
function popupFilterType_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupFilterType (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function editTxtOpt1_Callback(hObject, eventdata, handles)
% hObject    handle to editTxtOpt1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editTxtOpt1 as text
%        str2double(get(hObject,'String')) returns contents of editTxtOpt1 as a double
setStringOpt(hObject, 1);



% --- Executes during object creation, after setting all properties.
function editTxtOpt1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editTxtOpt1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function editTxtOpt2_Callback(hObject, eventdata, handles)
% hObject    handle to editTxtOpt2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editTxtOpt2 as text
%        str2double(get(hObject,'String')) returns contents of editTxtOpt2 as a double
setStringOpt(hObject, 2);

% --- Executes during object creation, after setting all properties.
function editTxtOpt2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editTxtOpt2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushBtnCancel.
function pushBtnCancel_Callback(hObject, eventdata, handles)
% hObject    handle to pushBtnCancel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
close(handles.figFilterMain);


% --- Executes on button press in pushBtnOK.
function pushBtnOK_Callback(hObject, eventdata, handles)
% hObject    handle to pushBtnOK (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

handles.output = handles.FilterOptions;
guidata(hObject, handles);
close(handles.figFilterMain);



% --- Executes when user attempts to close figFilterMain.
function figFilterMain_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figFilterMain (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if isequal(get(hObject, 'waitstatus'), 'waiting')
    % The GUI is still in UIWAIT, us UIRESUME
    uiresume(hObject);
else
    % The GUI is no longer waiting, just close it
    delete(hObject);
end



function editTxtOpt3_Callback(hObject, eventdata, handles)
% hObject    handle to editTxtOpt3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editTxtOpt3 as text
%        str2double(get(hObject,'String')) returns contents of editTxtOpt3 as a double
setStringOpt(hObject, 3);

% --- Executes during object creation, after setting all properties.
function editTxtOpt3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editTxtOpt3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function editTxtOpt4_Callback(hObject, eventdata, handles)
% hObject    handle to editTxtOpt4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editTxtOpt4 as text
%        str2double(get(hObject,'String')) returns contents of editTxtOpt4 as a double
setStringOpt(hObject, 4);

% --- Executes during object creation, after setting all properties.
function editTxtOpt4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editTxtOpt4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function editTxtOpt5_Callback(hObject, eventdata, handles)
% hObject    handle to editTxtOpt5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editTxtOpt5 as text
%        str2double(get(hObject,'String')) returns contents of editTxtOpt5 as a double
setStringOpt(hObject, 5);

% --- Executes during object creation, after setting all properties.
function editTxtOpt5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editTxtOpt5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in checkOpt1.
function checkOpt1_Callback(hObject, eventdata, handles)
% hObject    handle to checkOpt1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkOpt1
setCheckOpt(hObject, 1);

% --- Executes on button press in checkOpt2.
function checkOpt2_Callback(hObject, eventdata, handles)
% hObject    handle to checkOpt2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkOpt2
setCheckOpt(hObject, 2);


function editTxtOpt6_Callback(hObject, eventdata, handles)
% hObject    handle to editTxtOpt6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editTxtOpt6 as text
%        str2double(get(hObject,'String')) returns contents of editTxtOpt6 as a double
setStringOpt(hObject, 6);

% --- Executes during object creation, after setting all properties.
function editTxtOpt6_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editTxtOpt6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function editTxtOpt7_Callback(hObject, eventdata, handles)
% hObject    handle to editTxtOpt7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editTxtOpt7 as text
%        str2double(get(hObject,'String')) returns contents of editTxtOpt7 as a double
setStringOpt(hObject, 7);

% --- Executes during object creation, after setting all properties.
function editTxtOpt7_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editTxtOpt7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes when entered data in editable cell(s) in tableOpt1.
function tableOpt1_CellEditCallback(hObject, eventdata, handles)
% hObject    handle to tableOpt1 (see GCBO)
% eventdata  structure with the following fields (see MATLAB.UI.CONTROL.TABLE)
%	Indices: row and column indices of the cell(s) edited
%	PreviousData: previous data for the cell(s) edited
%	EditData: string(s) entered by the user
%	NewData: EditData or its converted form set on the Data property. Empty if Data was not changed
%	Error: error string when failed to convert EditData to appropriate value for Data
% handles    structure with handles and user data (see GUIDATA)
setTableOpt(hObject, 1);
