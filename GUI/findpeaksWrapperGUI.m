function varargout = findpeaksWrapperGUI(varargin)
% FINDPEAKSWRAPPERGUI MATLAB code for findpeaksWrapperGUI.fig
%      FINDPEAKSWRAPPERGUI, by itself, creates a new FINDPEAKSWRAPPERGUI or raises the existing
%      singleton*.
%
%      H = FINDPEAKSWRAPPERGUI returns the handle to a new FINDPEAKSWRAPPERGUI or the handle to
%      the existing singleton*.
%
%      FINDPEAKSWRAPPERGUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in FINDPEAKSWRAPPERGUI.M with the given input arguments.
%
%      FINDPEAKSWRAPPERGUI('Property','Value',...) creates a new FINDPEAKSWRAPPERGUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before findpeaksWrapperGUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to findpeaksWrapperGUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help findpeaksWrapperGUI

% Last Modified by GUIDE v2.5 03-Feb-2017 15:25:13

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @findpeaksWrapperGUI_OpeningFcn, ...
                   'gui_OutputFcn',  @findpeaksWrapperGUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before findpeaksWrapperGUI is made visible.
function findpeaksWrapperGUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to findpeaksWrapperGUI (see VARARGIN)

% Choose default command line output for findpeaksWrapperGUI
handles.output = false;
set(hObject,'WindowStyle','modal');
if numel(varargin) > 0
    %handles.mainGUI = varargin{1};
    handles.FinderOptions = parseDefaultFinderOptions(varargin{1});
    handles.WidthOptions = parseDefaultWidthOptions(varargin{2});
else
    handles.FinderOptions = parseDefaultFinderOptions(struct);
    handles.WidthOptions = parseDefaultWidthOptions(struct);
end

updateGUI(handles);

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes findpeaksWrapperGUI wait for user response (see UIRESUME)
uiwait(handles.figFindPeaksGUI);


% --- Outputs from this function are returned to the command line.
function varargout = findpeaksWrapperGUI_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if handles.output
    if isinf(handles.FinderOptions.NPeaks)
        handles.FinderOptions.NPeaks = [];
    end
    varargout{1} = handles.FinderOptions;
    varargout{2} = handles.WidthOptions;
else
    varargout{1} = [];
    varargout{2} = [];
end
delete(hObject);



function WidthOptions = parseDefaultWidthOptions(WidthOptions)

if ~isfield(WidthOptions, 'reference') || isempty(WidthOptions.reference)
    WidthOptions.reference = 'heightCorr';
end
if ~isfield(WidthOptions, 'refLevel') || isempty(WidthOptions.refLevel)
   WidthOptions.refLevel = 0.8; 
end
if ~isfield(WidthOptions, 'significantLocalDepth') || isempty(WidthOptions.significantLocalDepth)
    WidthOptions.significantLocalDepth = 20;
end
if ~isfield(WidthOptions, 'significantMaxHeight') || isempty(WidthOptions.significantMaxHeight)
    WidthOptions.significantMaxHeight = 5;
end


function FinderOptions = parseDefaultFinderOptions(FinderOptions)

if ~isfield(FinderOptions, 'MinPeakHeight') || isempty(FinderOptions.MinPeakHeight)
    FinderOptions.MinPeakHeight = -inf;
end
if ~isfield(FinderOptions, 'MinPeakProminence') || isempty(FinderOptions.MinPeakProminence)
    FinderOptions.MinPeakProminence = 0;
end
if ~isfield(FinderOptions, 'Threshold') || isempty(FinderOptions.Threshold)
    FinderOptions.Threshold = 0;
end
if ~isfield(FinderOptions, 'WidthReference') || isempty(FinderOptions.WidthReference)
    FinderOptions.WidthReference = 'halfprom';
end

if ~isfield(FinderOptions, 'MinPeakDistance') || isempty(FinderOptions.MinPeakDistance)
    FinderOptions.MinPeakDistance = 0;
end
if ~isfield(FinderOptions, 'MinPeakWidth') || isempty(FinderOptions.MinPeakWidth)
    FinderOptions.MinPeakWidth = 0;
end
if ~isfield(FinderOptions, 'MaxPeakWidth') || isempty(FinderOptions.MaxPeakWidth)
    FinderOptions.MaxPeakWidth = inf;
end
if ~isfield(FinderOptions, 'NPeaks') || isempty(FinderOptions.NPeaks)
    FinderOptions.NPeaks = inf;
end


function output = parseFinderRefType(input)

if ischar(input)
    switch lower(input)
        case 'halfprom'
            output = 1;
        case 'halfheight'
            output = 2;
        otherwise
            error('parseReferenceType:UnknownString', 'Input must be a valid string')
    end
elseif isscalar(input)
    switch double(input)
        case 1
            output = 'halfprom';
        case 2
            output = 'halfheight';
        otherwise
            error('parseReferenceType:UnknownScalar', 'Input must be a valid scalar')
    end
else
    error('parseReferenceType:UnknownInput', 'Input must be a scalar or string')
end


function output = parseWidthRefType(input)

if ischar(input)
    switch lower(input)
        case 'height'
            output = 1;
        case 'prominence'
            output = 2;
        case 'heightcorr'
            output = 3;
        otherwise
            error('parseReferenceType:UnknownString', 'Input must be a valid string')
    end
elseif isscalar(input)
    switch double(input)
        case 1
            output = 'height';
        case 2
            output = 'prominence';
        case 3
            output = 'heightCorr';
        otherwise
            error('parseReferenceType:UnknownScalar', 'Input must be a valid scalar')
    end
else
    error('parseReferenceType:UnknownInput', 'Input must be a scalar or string')
end


function updateGUI(handles)
handles.popupFinderWidthRef.Value = parseFinderRefType(handles.FinderOptions.WidthReference);
handles.popupWidthRefType.Value = parseWidthRefType(handles.WidthOptions.reference);

handles.editTxtMinPeakHeight.String = num2str(handles.FinderOptions.MinPeakHeight);
handles.editTxtMinPeakProm.String = num2str(handles.FinderOptions.MinPeakProminence);
handles.editTxtThreshold.String = num2str(handles.FinderOptions.Threshold);
handles.editTxtMinPeakDistance.String = num2str(handles.FinderOptions.MinPeakDistance);
handles.editTxtMinPeakWidth.String = num2str(handles.FinderOptions.MinPeakWidth);
handles.editTxtMaxPeakWidth.String = num2str(handles.FinderOptions.MaxPeakWidth);
handles.editTxtNPeaks.String = num2str(handles.FinderOptions.NPeaks);

handles.editTxtWidthRefLevel.String = num2str(handles.WidthOptions.refLevel);
handles.editTxtWidthLocalDepth.String = num2str(handles.WidthOptions.significantLocalDepth);
handles.editTxtWidthMaxHeight.String = num2str(handles.WidthOptions.significantMaxHeight);


function editTxtMinHeight_Callback(hObject, eventdata, handles)
% hObject    handle to editTxtMinHeight (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editTxtMinHeight as text
%        str2double(get(hObject,'String')) returns contents of editTxtMinHeight as a double


% --- Executes during object creation, after setting all properties.
function editTxtMinHeight_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editTxtMinHeight (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function editTxtMinProm_Callback(hObject, eventdata, handles)
% hObject    handle to editTxtMinProm (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editTxtMinProm as text
%        str2double(get(hObject,'String')) returns contents of editTxtMinProm as a double


% --- Executes during object creation, after setting all properties.
function editTxtMinProm_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editTxtMinProm (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function editTxtThrsh_Callback(hObject, eventdata, handles)
% hObject    handle to editTxtThrsh (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editTxtThrsh as text
%        str2double(get(hObject,'String')) returns contents of editTxtThrsh as a double


% --- Executes during object creation, after setting all properties.
function editTxtThrsh_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editTxtThrsh (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function editTxtMinDist_Callback(hObject, eventdata, handles)
% hObject    handle to editTxtMinDist (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editTxtMinDist as text
%        str2double(get(hObject,'String')) returns contents of editTxtMinDist as a double


% --- Executes during object creation, after setting all properties.
function editTxtMinDist_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editTxtMinDist (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function editTxtMinWidth_Callback(hObject, eventdata, handles)
% hObject    handle to editTxtMinWidth (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editTxtMinWidth as text
%        str2double(get(hObject,'String')) returns contents of editTxtMinWidth as a double


% --- Executes during object creation, after setting all properties.
function editTxtMinWidth_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editTxtMinWidth (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function editTxtMaxWidth_Callback(hObject, eventdata, handles)
% hObject    handle to editTxtMaxWidth (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editTxtMaxWidth as text
%        str2double(get(hObject,'String')) returns contents of editTxtMaxWidth as a double


% --- Executes during object creation, after setting all properties.
function editTxtMaxWidth_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editTxtMaxWidth (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function editTxtNPeaks_Callback(hObject, eventdata, handles)
% hObject    handle to editTxtNPeaks (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editTxtNPeaks as text
%        str2double(get(hObject,'String')) returns contents of editTxtNPeaks as a double
newStr = get(hObject,'String');
try 
    newVal = str2double(newStr);
    attr = {'scalar', 'nonnegative'};
    validateattributes(newVal, {'numeric'}, attr);
    if ~isinf(newVal)
        attr = {'integer'};
        validateattributes(newVal, {'numeric'}, attr);
    end
    handles.FinderOptions.NPeaks = newVal;
catch
    h = errordlg('NPeaks must be a nonnegative integer scalar or inf', ...
        'Invalid value', 'modal');
    uiwait(h);
end
updateGUI(handles);
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function editTxtNPeaks_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editTxtNPeaks (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenu2.
function popupmenu2_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu2 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu2


% --- Executes during object creation, after setting all properties.
function popupmenu2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushBtnCancel.
function pushBtnCancel_Callback(hObject, eventdata, handles)
% hObject    handle to pushBtnCancel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
close(handles.figFindPeaksGUI);

% --- Executes on button press in pushBtnOK.
function pushBtnOK_Callback(hObject, eventdata, handles)
% hObject    handle to pushBtnOK (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.output = true;
guidata(hObject, handles);
close(handles.figFindPeaksGUI);


function editTxtWidthMaxHeight_Callback(hObject, eventdata, handles)
% hObject    handle to editTxtWidthMaxHeight (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editTxtWidthMaxHeight as text
%        str2double(get(hObject,'String')) returns contents of editTxtWidthMaxHeight as a double
newStr = get(hObject,'String');
try 
    newVal = str2double(newStr);
    attr = {'scalar', 'real', 'nonnegative', '<', handles.WidthOptions.significantLocalDepth};
    validateattributes(newVal, {'numeric'}, attr);
    handles.WidthOptions.significantMaxHeight = newVal;
catch
    h = errordlg({'significant neighboring extrema height must be a nonnegative real scalar ', ...
        'and smaller than the minimum extrema depth'}, 'Invalid value', 'modal');
    uiwait(h);
end
updateGUI(handles);
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function editTxtWidthMaxHeight_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editTxtWidthMaxHeight (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function editTxtWidthRefLevel_Callback(hObject, eventdata, handles)
% hObject    handle to editTxtWidthRefLevel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editTxtWidthRefLevel as text
%        str2double(get(hObject,'String')) returns contents of editTxtWidthRefLevel as a double
newStr = get(hObject,'String');
try 
    newVal = str2double(newStr);
    attr = {'scalar', 'real', '>', 0, '<', 1};
    validateattributes(newVal, {'numeric'}, attr);
    handles.WidthOptions.refLevel = newVal;
catch
    h = errordlg('Reference level must be a real scalar between 0 and 1', ...
        'Invalid value', 'modal');
    uiwait(h);
end
updateGUI(handles);
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function editTxtWidthRefLevel_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editTxtWidthRefLevel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function editTxtWidthLocalDepth_Callback(hObject, eventdata, handles)
% hObject    handle to editTxtWidthLocalDepth (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editTxtWidthLocalDepth as text
%        str2double(get(hObject,'String')) returns contents of editTxtWidthLocalDepth as a double
newStr = get(hObject,'String');
try 
    newVal = str2double(newStr);
    attr = {'scalar', 'real', 'nonnegative', '>', handles.WidthOptions.significantMaxHeight};
    validateattributes(newVal, {'numeric'}, attr);
    handles.WidthOptions.significantLocalDepth = newVal;
catch
    h = errordlg({'significant local extrema depth must be a nonnegative real' ...
        'scalar and larger than the significant neighboring height'}, ...
        'Invalid value', 'modal');
    uiwait(h);
end
updateGUI(handles);
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function editTxtWidthLocalDepth_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editTxtWidthLocalDepth (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupWidthRefType.
function popupWidthRefType_Callback(hObject, eventdata, handles)
% hObject    handle to popupWidthRefType (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupWidthRefType contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupWidthRefType
handles.WidthOptions.reference = parseWidthRefType(hObject.Value);
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function popupWidthRefType_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupWidthRefType (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function editTxtMinPeakHeight_Callback(hObject, eventdata, handles)
% hObject    handle to editTxtMinPeakHeight (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editTxtMinPeakHeight as text
%        str2double(get(hObject,'String')) returns contents of editTxtMinPeakHeight as a double
newStr = get(hObject,'String');
try 
    newVal = str2double(newStr);
    attr = {'scalar', 'real'};
    validateattributes(newVal, {'numeric'}, attr);
    handles.FinderOptions.MinPeakHeight = newVal;
catch
    h = errordlg('MinPeakHeight must be a real scalar', 'Invalid value', 'modal');
    uiwait(h);
end
updateGUI(handles);
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function editTxtMinPeakHeight_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editTxtMinPeakHeight (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function editTxtMinPeakProm_Callback(hObject, eventdata, handles)
% hObject    handle to editTxtMinPeakProm (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editTxtMinPeakProm as text
%        str2double(get(hObject,'String')) returns contents of editTxtMinPeakProm as a double
newStr = get(hObject,'String');
try 
    newVal = str2double(newStr);
    attr = {'scalar', 'real'};
    validateattributes(newVal, {'numeric'}, attr);
    handles.FinderOptions.MinPeakProminence = newVal;
catch
    h = errordlg('MinPeakProminence must be a real scalar', 'Invalid value', 'modal');
    uiwait(h);
end
updateGUI(handles);
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function editTxtMinPeakProm_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editTxtMinPeakProm (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function editTxtThreshold_Callback(hObject, eventdata, handles)
% hObject    handle to editTxtThreshold (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editTxtThreshold as text
%        str2double(get(hObject,'String')) returns contents of editTxtThreshold as a double
newStr = get(hObject,'String');
try 
    newVal = str2double(newStr);
    attr = {'scalar', 'real', 'nonnegative'};
    validateattributes(newVal, {'numeric'}, attr);
    handles.FinderOptions.Threshold = newVal;
catch
    h = errordlg('Threshold must be a nonnegative real scalar', 'Invalid value', 'modal');
    uiwait(h);
end
updateGUI(handles);
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function editTxtThreshold_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editTxtThreshold (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupFinderWidthRef.
function popupFinderWidthRef_Callback(hObject, eventdata, handles)
% hObject    handle to popupFinderWidthRef (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupFinderWidthRef contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupFinderWidthRef
handles.FinderOptions.WidthReference = parseFinderRefType(hObject.Value);
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function popupFinderWidthRef_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupFinderWidthRef (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function editTxtMinPeakWidth_Callback(hObject, eventdata, handles)
% hObject    handle to editTxtMinPeakWidth (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editTxtMinPeakWidth as text
%        str2double(get(hObject,'String')) returns contents of editTxtMinPeakWidth as a double
newStr = get(hObject,'String');
try 
    newVal = str2double(newStr);
    attr = {'scalar', 'real', 'nonnegative'};
    validateattributes(newVal, {'numeric'}, attr);
    handles.FinderOptions.MinPeakWidth = newVal;
catch
    h = errordlg('MinPeakWidth must be a nonnegative real scalar', ...
        'Invalid value', 'modal');
    uiwait(h);
end
updateGUI(handles);
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function editTxtMinPeakWidth_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editTxtMinPeakWidth (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function editTxtMaxPeakWidth_Callback(hObject, eventdata, handles)
% hObject    handle to editTxtMaxPeakWidth (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editTxtMaxPeakWidth as text
%        str2double(get(hObject,'String')) returns contents of editTxtMaxPeakWidth as a double
newStr = get(hObject,'String');
try 
    newVal = str2double(newStr);
    attr = {'scalar', 'real', 'nonnegative'};
    validateattributes(newVal, {'numeric'}, attr);
    handles.FinderOptions.MaxPeakWidth = newVal;
catch
    h = errordlg('MaxPeakWidth must be a nonnegative real scalar', ...
        'Invalid value', 'modal');
    uiwait(h);
end
updateGUI(handles);
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function editTxtMaxPeakWidth_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editTxtMaxPeakWidth (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function editTxtMinPeakDistance_Callback(hObject, eventdata, handles)
% hObject    handle to editTxtMinPeakDistance (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editTxtMinPeakDistance as text
%        str2double(get(hObject,'String')) returns contents of editTxtMinPeakDistance as a double
newStr = get(hObject,'String');
try 
    newVal = str2double(newStr);
    attr = {'scalar', 'real', 'nonnegative'};
    validateattributes(newVal, {'numeric'}, attr);
    handles.FinderOptions.MinPeakDistance = newVal;
catch
    h = errordlg('MinPeakDistance must be a nonnegative real scalar', ...
        'Invalid value', 'modal');
    uiwait(h);
end
updateGUI(handles);
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function editTxtMinPeakDistance_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editTxtMinPeakDistance (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes when user attempts to close figFindPeaksGUI.
function figFindPeaksGUI_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figFindPeaksGUI (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if isequal(get(hObject, 'waitstatus'), 'waiting')
    % The GUI is still in UIWAIT, us UIRESUME
    uiresume(hObject);
else
    % The GUI is no longer waiting, just close it
    delete(hObject);
end
