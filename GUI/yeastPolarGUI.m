function varargout = yeastPolarGUI(varargin)
% YEASTPOLARGUI MATLAB code for yeastPolarGUI.fig
%      YEASTPOLARGUI, by itself, creates a new YEASTPOLARGUI or raises the existing
%      singleton*.
%
%      H = YEASTPOLARGUI returns the handle to a new YEASTPOLARGUI or the handle to
%      the existing singleton*.
%
%      YEASTPOLARGUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in YEASTPOLARGUI.M with the given input arguments.
%
%      YEASTPOLARGUI('Property','Value',...) creates a new YEASTPOLARGUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before yeastPolarGUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to yeastPolarGUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help yeastPolarGUI

% Last Modified by GUIDE v2.5 12-Apr-2018 12:39:24

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @yeastPolarGUI_OpeningFcn, ...
                   'gui_OutputFcn',  @yeastPolarGUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before yeastPolarGUI is made visible.
function yeastPolarGUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to yeastPolarGUI (see VARARGIN)

% Choose default command line output for yeastPolarGUI
handles.output = false;

if numel(varargin) ~= 6
    error('InitGUI:MissingVals', 'GUI must be initialized with all the needed inputs')
end
handles.intResults = varargin{1};
handles.typeIntensity = varargin{2};
handles.FilterOptions = varargin{3};
handles.PeakOptions = varargin{4};
handles.polarWeights = varargin{5};
handles.polarIntTypes = varargin{6};
handles.nImgs = numel(handles.intResults);
handles.imgIdx = 1;
handles.cellIdx = 1;
handles.spotResults(handles.nImgs) = struct('ySmoothed', [], 'yUpsampled', [], 'spots', [], 'objectLabels', [], 'totalCount', []);

handles = updateIntensities(hObject, handles);
handles = updateSpots(handles);
updateAxes(hObject, handles);
updateTextFields(handles);
if isfield(handles.PeakOptions, 'correctBaseline') && handles.PeakOptions.correctBaseline
    handles.checkCorrectBaseline.Value = true;
end

% handles.objToDisable = findobj(handles.figMainGUI, 'Enable', 'on');
% Update handles structure
guidata(hObject, handles);
impixelinfo(hObject);
snapnow;
% UIWAIT makes yeastPolarGUI wait for user response (see UIRESUME)
uiwait(handles.figMainGUI);


% --- Outputs from this function are returned to the command line.
function varargout = yeastPolarGUI_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;
varargout{2} = handles.FilterOptions;
varargout{3} = handles.PeakOptions;
varargout{4} = handles.polarWeights;
varargout{5} = handles.polarIntTypes;

delete(hObject);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Graphical update functions

function handles = updateIntensities(hObject, handles)

[ ySm, yUp, objLabels] = ybt.smoothStretchIntensities( ...
    handles.intResults(handles.imgIdx), handles.FilterOptions, ...
    handles.typeIntensity, 'none');

handles.spotResults(handles.imgIdx).ySmoothed = ySm;
handles.spotResults(handles.imgIdx).yUpsampled = yUp;
handles.spotResults(handles.imgIdx).objectLabels = objLabels;

% Update handles structure
%guidata(hObject, handles);


function handles = updateSpots(handles)

imgIdx = handles.imgIdx;
ySm = handles.spotResults(imgIdx).ySmoothed;
yUp = handles.spotResults(imgIdx).yUpsampled;
objLab = handles.spotResults(imgIdx).objectLabels;
 
if ~isempty(ySm)
handles.spotResults(handles.imgIdx).spots = ybt.spotFinder( ySm, yUp, objLab, ...
    handles.PeakOptions, handles.polarWeights, handles.polarIntTypes);
else
    handles.spotResults(handles.imgIdx).spots = [];
end

if ~isempty(handles.spotResults(handles.imgIdx).spots)
    handles.spotResults(handles.imgIdx).totalCount = ...
        sum([handles.spotResults(handles.imgIdx).spots.totalCount]);
else
    handles.spotResults(handles.imgIdx).totalCount = 0;
end

% Update handles structure
%guidata(hObject, handles);


function updateTextFields(handles)
nCells = numel(handles.spotResults(handles.imgIdx).objectLabels);
nImgs = handles.nImgs;

handles.editTxtImgNo.String = sprintf('Image #%d/%d', handles.imgIdx, nImgs);
if nCells == 0
    handles.editTxtCellNo.String = sprintf('Cell #%d/%d', 0, nCells);
    handles.editTxtRatioPol.String = sprintf('%.3f', nan);
    handles.editTxtRatioBipol.String = sprintf('%.3f', nan);
else
    handles.editTxtCellNo.String = sprintf('Cell #%d/%d', handles.cellIdx, nCells);
    handles.editTxtRatioPol.String = sprintf('%.3f', handles.spotResults(handles.imgIdx).spots(handles.cellIdx).ratioPolarization);
    handles.editTxtRatioBipol.String = sprintf('%.3f', handles.spotResults(handles.imgIdx).spots(handles.cellIdx).ratioBipolarity);
end

handles.editTxtWmeanInt.String = sprintf('%.3f', handles.polarWeights{1}(1));
handles.editTxtWwidth.String = sprintf('%.3f', handles.polarWeights{1}(2));
handles.editTxtWsmoothness.String = sprintf('%.3f', handles.polarWeights{1}(3));
handles.editTxtWmaxInt.String = sprintf('%.3f', handles.polarWeights{1}(4));

status1 = strcmpi(handles.polarIntTypes(1), 's');
if handles.checkMeanInt.Value ~= status1
    handles.checkMeanInt.Value = status1;
end
status2 = strcmpi(handles.polarIntTypes(2), 's');
if handles.checkSmoothness.Value ~= status2
    handles.checkSmoothness.Value = status2;
end
status3 = strcmpi(handles.polarIntTypes(3), 's');
if handles.checkMaxInt.Value ~= status3
    handles.checkMaxInt.Value = status3;
end
%sprintf('%.20f', handles.spotResults(handles.imgIdx).spots(handles.cellIdx).ratioPolarization)

function updateAxes(hObject, handles)

imgIdx = handles.imgIdx;
cellIdx = handles.cellIdx;
if isempty(handles.spotResults(handles.imgIdx).ySmoothed)
    ySm = zeros(1, max(size( handles.spotResults(imgIdx).ySmoothed )) );
    yUp = zeros(1, max(size( handles.spotResults(imgIdx).yUpsampled )) );
    signalImg = ybt.getNoObjectImage();
    segmenImg = ybt.getNoObjectImage();
    polarSpots = struct('count', 0);
    sideSpots = struct('count', 0);
else
    cellLbl = handles.spotResults(imgIdx).objectLabels(cellIdx);
    ySm = handles.spotResults(imgIdx).ySmoothed(cellIdx, :);
    yUp = handles.spotResults(imgIdx).yUpsampled(cellIdx, :);
    signalImg = handles.intResults(imgIdx).RegionStats(cellLbl).signalImage;
    segmenImg = handles.intResults(imgIdx).RegionStats(cellLbl).Image;
    polarSpots = handles.spotResults(imgIdx).spots(cellIdx).polarSpots;
    sideSpots = handles.spotResults(imgIdx).spots(cellIdx).sideSpots;
end

L = numel(ySm);
polarPoint1 = round(L/2);
polarPoint2 = L;
polarPoint3 = polarPoint1 + polarPoint2;

ySm = padarray(ySm, [0 L], 'circular', 'post');
yUp = padarray(yUp, [0 L], 'circular', 'post');
x = 1:1:numel(ySm);
if isfield(handles.PeakOptions, 'correctBaseline') && handles.PeakOptions.correctBaseline
     ySm = ySm - min(ySm(:));
end

axes(handles.axImg);
imshow(signalImg, [], 'InitialMagnification', 100);
[~, fname] = fileparts(handles.intResults(imgIdx).signalImageName);
title(fname,'interpreter','none');

axes(handles.axSegmImg);
imshow(segmenImg, [], 'InitialMagnification', 100)
[~, fname] = fileparts(handles.intResults(imgIdx).segmentationImageName);
title(fname,'interpreter','none');

axes(handles.axSmooth);
plot(x, ySm, 'k')
hold on;
title('Smoothed intensities')
offset = 0.01;
% Set y axis maximum to a slightly larger value than the max value of
% smoothed or upsampled data.
% In case the intensities are all zeros we set it to eps.
yMax = max([ySm, yUp])*(1+offset);
yMax = max(yMax, eps);
yMin = 0; %min([ySm, yUp])*(1-offset);
xMax = 3/2 * L * (1+offset);
xMin = 1/2 * L * (1-offset);

plot([polarPoint1, polarPoint1], [yMin yMax], 'k:');
plot([polarPoint2, polarPoint2], [yMin yMax], 'k:');
plot([polarPoint3, polarPoint3], [yMin yMax], 'k:');

h = zeros(2,1);
h(1) = plot(nan, nan, 'Marker', 's', 'Color', 'red', 'MarkerFaceColor', ...
    'red', 'MarkerSize', 10, 'LineStyle', 'none');
h(2) = plot(nan, nan, 'Marker', 'd', 'Color', 'blue', 'MarkerFaceColor', ...
    'blue', 'MarkerSize', 10, 'LineStyle', 'none');
hold off;

axes(handles.axRaw);
plot(x,yUp, 'k')
hold on;
title('Unsmoothed intensities');
% And plot vertical lines that correspond to the tip regions
plot([polarPoint1, polarPoint1], [yMin yMax], 'k:');
plot([polarPoint2, polarPoint2], [yMin yMax], 'k:');
plot([polarPoint3, polarPoint3], [yMin yMax], 'k:');
axis([xMin, xMax, yMin, yMax]);
hold off;


axes(handles.axSmooth)

color = 'red';
marker = 's';
for iPol = 1:polarSpots.count
    markPeak(polarSpots.peakLocs(iPol), ySm, polarSpots.leftMins(iPol), polarSpots.rightMins(iPol) ,...
        polarSpots.peakBase(iPol), marker, color, iPol);
    
    secLoc = polarSpots.peakLocs(iPol) + L;
    secLeft = polarSpots.leftMins(iPol) + L;
    secRight = polarSpots.rightMins(iPol) + L;
    markPeak(secLoc, ySm, secLeft, secRight, polarSpots.peakBase(iPol), marker, color, iPol);
end

color = 'blue';
marker = 'd';
for iSide = 1:sideSpots.count
    markPeak(sideSpots.peakLocs(iSide), ySm, sideSpots.leftMins(iSide), sideSpots.rightMins(iSide) ,...
        sideSpots.peakBase(iSide), marker, color, iSide);
    
    secLoc = sideSpots.peakLocs(iSide) + L;
    secLeft = sideSpots.leftMins(iSide) + L;
    secRight = sideSpots.rightMins(iSide) + L;
    markPeak(secLoc, ySm, secLeft, secRight, sideSpots.peakBase(iSide), marker, color, iSide);
end

axis([xMin, xMax, yMin, yMax]);

legend(h, {'Polar', 'Side'}, 'FontSize', 14);
impixelinfo(handles.axImg);



function markPeak(loc, y, left, right, base, marker, color, number)
hold on;
val = y(loc);
border = [left, right];
N = numel(y) / 2;
minY = min(y(wrapIdx(border, N)));
[maxY, maxIdx] = max(y(wrapIdx(border, N)));
plot(loc, val, 'Marker', marker, 'Color', color, 'MarkerFaceColor', ...
    color, 'MarkerSize', 10, 'LineStyle', 'none');
%if loc >= 0.5*N && loc <= 1.5*N  
t = text(loc + 10, val, num2str(number), 'FontSize', 12, 'Color', color);
t.Clipping = 'on';
%end
plot([left right], [minY minY], 'LineWidth', 2, 'Color', color);
plot([border(maxIdx) border(maxIdx)], [minY maxY], 'LineWidth', 1, ...
    'Color', color, 'LineStyle', '--');
plot([loc, loc], [val, val - base], 'LineWidth', 2, 'Color', color, ...
    'LineStyle', ':');
% plot([loc-20, loc+20], [val - base*baseLevel, val - base*baseLevel], ...
%     'LineWidth', 2, 'Color', color, 'LineStyle', ':');
hold off;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Navigation

% --- Executes on button press in pushBtnNextCell.
function pushBtnNextCell_Callback(hObject, eventdata, handles)
% hObject    handle to pushBtnNextCell (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

handles.cellIdx = handles.cellIdx + 1;
if handles.cellIdx > numel(handles.spotResults(handles.imgIdx).objectLabels)
    if handles.imgIdx < handles.nImgs
        handles.imgIdx = handles.imgIdx + 1;
        handles.cellIdx = 1;
        handles = updateIntensities(hObject, handles);
        handles = updateSpots(handles);
    else
        handles.cellIdx = handles.cellIdx - 1;
        h = warndlg('Reached the final cell of the final image', ...
            'End reached', 'modal');
        uiwait(h);
    end
end

updateAxes(hObject, handles);
updateTextFields(handles);
guidata(hObject, handles);

% --- Executes on button press in pushBtnPrevCell.
function pushBtnPrevCell_Callback(hObject, eventdata, handles)
% hObject    handle to pushBtnPrevCell (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

handles.cellIdx = handles.cellIdx - 1;
if handles.cellIdx < 1
    if handles.imgIdx > 1
        handles.imgIdx = handles.imgIdx - 1;
        handles = updateIntensities(hObject, handles);
        handles = updateSpots(handles);
        handles.cellIdx = numel(handles.spotResults(handles.imgIdx).objectLabels);
    else
        handles.cellIdx = handles.cellIdx + 1;
        h = warndlg('Reached the first cell of the first image', ...
            'End reached', 'modal');
        uiwait(h);
    end
end

updateAxes(hObject, handles);
updateTextFields(handles);
guidata(hObject, handles);

% --- Executes on button press in pushBtnGotoCell.
function pushBtnGotoCell_Callback(hObject, eventdata, handles)
% hObject    handle to pushBtnGotoCell (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
nCells = numel(handles.spotResults(handles.imgIdx).objectLabels);
if nCells == 0
    h = errordlg(['No cells in current image to go to'], ...
        'Invalid value', 'modal');
    uiwait(h);
else
    try
        answer = inputdlg('Enter Cell No:', 'Go to cell #');
        newCell = str2double(answer{1});
        attr = {'scalar', 'integer', 'positive', '<=', nCells};
        validateattributes(newCell, {'numeric'}, attr);
        handles.cellIdx = newCell;
        
        updateAxes(hObject, handles);
        updateTextFields(handles);
        guidata(hObject, handles);
        
    catch
        
        h = errordlg(['Cell no. must be a integer value between 1 and ', ...
            sprintf('%d', nCells), '(Total #cells in current image)'], ...
            'Invalid value', 'modal');
        uiwait(h);
    end
end

% --- Executes on button press in pushBtnNextImg.
function pushBtnNextImg_Callback(hObject, eventdata, handles)
% hObject    handle to pushBtnNextImg (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if handles.imgIdx < handles.nImgs
    handles.imgIdx = handles.imgIdx + 1;
    handles.cellIdx = 1;
    handles = updateIntensities(hObject, handles);
    handles = updateSpots(handles);
    updateAxes(hObject, handles);
    updateTextFields(handles);
    guidata(hObject, handles);
else
    h = warndlg('Reached the final image', 'End reached', 'modal');
    uiwait(h);
end

% --- Executes on button press in pushBtnPrevImg.
function pushBtnPrevImg_Callback(hObject, eventdata, handles)
% hObject    handle to pushBtnPrevImg (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if handles.imgIdx > 1
    handles.imgIdx = handles.imgIdx - 1;
    handles.cellIdx = 1;
    handles = updateIntensities(hObject, handles);
    handles = updateSpots(handles);
    updateAxes(hObject, handles);
    updateTextFields(handles);
    guidata(hObject, handles);
else
    h = warndlg('Reached the final image', 'End reached', 'modal');
    uiwait(h);
end

% --- Executes on button press in pushBtnGotoImage.
function pushBtnGotoImage_Callback(hObject, eventdata, handles)
% hObject    handle to pushBtnGotoImage (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
answer = inputdlg('Enter Image No:', 'Go to image #');
nImgs = handles.nImgs;
try 
    newImg = str2double(answer{1});
    attr = {'scalar', 'integer', 'positive', '<=', nImgs};
    validateattributes(newImg, {'numeric'}, attr);
    
    handles.imgIdx = newImg;
    handles.cellIdx = 1;
    
    handles = updateIntensities(hObject, handles);
    handles = updateSpots(handles);
    updateAxes(hObject, handles);
    updateTextFields(handles);
    guidata(hObject, handles);

catch
    h = errordlg(['Image no. must be a integer value between 1 and ', ...
        sprintf('%d', nImgs), '(Total #images)'], ...
        'Invalid value', 'modal');
    uiwait(h);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Control buttons

% --- Executes on button press in pushBtnExecuteAnalysis.
function pushBtnExecuteAnalysis_Callback(hObject, eventdata, handles)
% hObject    handle to pushBtnExecuteAnalysis (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.output = true;
guidata(hObject, handles);
close(handles.figMainGUI);

% --- Executes on button press in pushBtnQuit.
function pushBtnQuit_Callback(hObject, eventdata, handles)
% hObject    handle to pushBtnQuit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

close(handles.figMainGUI)


% --- Executes on button press in pushBtnFilter.
function pushBtnFilter_Callback(hObject, eventdata, handles)
% hObject    handle to pushBtnFilter (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%set(handles.objToDisable,'Enable','off');
newFilterOpt = filterOptionsGUI(handles.FilterOptions);
if ~isempty(newFilterOpt)
    handles.FilterOptions = newFilterOpt;
    handles = updateIntensities(hObject, handles);
    handles = updateSpots(handles);
    updateAxes(hObject, handles);
    updateTextFields(handles);
    guidata(hObject, handles);
end
%set(handles.objToDisable,'Enable','on');

% --- Executes on button press in pushBtnPeakFinder.
function pushBtnPeakFinder_Callback(hObject, eventdata, handles)
% hObject    handle to pushBtnPeakFinder (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%set(handles.objToDisable,'Enable','off');
newFinderOpt = [];
newWidthOpt = [];
try
    if ischar(handles.PeakOptions.GUI)
        peakGUI = str2func(handles.PeakOptions.GUI);
    elseif isa(handles.PeakOptions.GUI, 'function_handle')
        peakGUI = handles.PeakOptions.GUI;
    end
    [newFinderOpt, newWidthOpt] = peakGUI(handles.PeakOptions.FinderOptions, ...
        handles.PeakOptions.WidthOptions);
catch
    h = errordlg('GUI for peak detection is not supplied or failed to initialize', ...
        'Invalid value', 'modal');
    uiwait(h);
end
if ~isempty(newFinderOpt) && ~isempty(newWidthOpt)
    handles.PeakOptions.FinderOptions = newFinderOpt;
    handles.PeakOptions.WidthOptions = newWidthOpt;
    handles = updateSpots(handles);
    updateAxes(hObject, handles);
    updateTextFields(handles);
    guidata(hObject, handles);
end
%set(handles.objToDisable,'Enable','on');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Text box callbacks

function editTxtWmeanInt_Callback(hObject, eventdata, handles)
% hObject    handle to editTxtWmeanInt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editTxtWmeanInt as text
%        str2double(get(hObject,'String')) returns contents of editTxtWmeanInt as a double
%set(handles.objToDisable,'Enable','off');
newStr = get(hObject,'String');
try 
    newVal = str2double(newStr);
    attr = {'scalar', 'real', 'finite'};
    validateattributes(newVal, {'numeric'}, attr);
    handles.polarWeights{1}(1) = newVal;
    handles = updateSpots(handles);
catch
    h = errordlg('Mean intensity weight must be a real finite scalar', ...
        'Invalid value', 'modal');
    uiwait(h);
end
updateTextFields(handles);
%set(handles.objToDisable,'Enable','on');
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function editTxtWmeanInt_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editTxtWmeanInt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit2_Callback(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit2 as text
%        str2double(get(hObject,'String')) returns contents of edit2 as a double


% --- Executes during object creation, after setting all properties.
function edit2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit3_Callback(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit3 as text
%        str2double(get(hObject,'String')) returns contents of edit3 as a double


% --- Executes during object creation, after setting all properties.
function edit3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit4_Callback(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit4 as text
%        str2double(get(hObject,'String')) returns contents of edit4 as a double


% --- Executes during object creation, after setting all properties.
function edit4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit5_Callback(hObject, eventdata, handles)
% hObject    handle to edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit5 as text
%        str2double(get(hObject,'String')) returns contents of edit5 as a double


% --- Executes during object creation, after setting all properties.
function edit5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function editTxtWwidth_Callback(hObject, eventdata, handles)
% hObject    handle to editTxtWwidth (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editTxtWwidth as text
%        str2double(get(hObject,'String')) returns contents of editTxtWwidth as a double
%set(handles.objToDisable,'Enable','off');
newStr = get(hObject,'String');
try 
    newVal = str2double(newStr);
    attr = {'scalar', 'real', 'finite'};
    validateattributes(newVal, {'numeric'}, attr);
    handles.polarWeights{1}(2) = newVal;
    handles = updateSpots(handles);
catch
    h = errordlg('Size weight must be a real finite scalar', ...
        'Invalid value', 'modal');
    uiwait(h);
end
updateTextFields(handles);
%set(handles.objToDisable,'Enable','on');
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function editTxtWwidth_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editTxtWwidth (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function editTxtWsmoothness_Callback(hObject, eventdata, handles)
% hObject    handle to editTxtWsmoothness (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editTxtWsmoothness as text
%        str2double(get(hObject,'String')) returns contents of editTxtWsmoothness as a double
%set(handles.objToDisable,'Enable','off');
newStr = get(hObject,'String');
try 
    newVal = str2double(newStr);
    attr = {'scalar', 'real', 'finite'};
    validateattributes(newVal, {'numeric'}, attr);
    handles.polarWeights{1}(3) = newVal;
    handles = updateSpots(handles);
catch
    h = errordlg('Smoothness weight must be a real finite scalar', ...
        'Invalid value', 'modal');
    uiwait(h);
end
updateTextFields(handles);
%set(handles.objToDisable,'Enable','on');
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function editTxtWsmoothness_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editTxtWsmoothness (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function editTxtWmaxInt_Callback(hObject, eventdata, handles)
% hObject    handle to editTxtWmaxInt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editTxtWmaxInt as text
%        str2double(get(hObject,'String')) returns contents of editTxtWmaxInt as a double
%set(handles.objToDisable,'Enable','off');
newStr = get(hObject,'String');
try 
    newVal = str2double(newStr);
    attr = {'scalar', 'real', 'finite'};
    validateattributes(newVal, {'numeric'}, attr);
    handles.polarWeights{1}(4) = newVal;
    handles = updateSpots(handles);
catch
    h = errordlg('Max intensity weight must be a real finite scalar', ...
        'Invalid value', 'modal');
    uiwait(h);
end
updateTextFields(handles);
%set(handles.objToDisable,'Enable','on');
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function editTxtWmaxInt_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editTxtWmaxInt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit13_Callback(hObject, eventdata, handles)
% hObject    handle to edit13 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit13 as text
%        str2double(get(hObject,'String')) returns contents of edit13 as a double


% --- Executes during object creation, after setting all properties.
function edit13_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit13 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function editTxtRatioPol_Callback(hObject, eventdata, handles)
% hObject    handle to editTxtRatioPol (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editTxtRatioPol as text
%        str2double(get(hObject,'String')) returns contents of editTxtRatioPol as a double


% --- Executes during object creation, after setting all properties.
function editTxtRatioPol_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editTxtRatioPol (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit15_Callback(hObject, eventdata, handles)
% hObject    handle to edit15 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit15 as text
%        str2double(get(hObject,'String')) returns contents of edit15 as a double


% --- Executes during object creation, after setting all properties.
function edit15_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit15 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function editTxtRatioBipol_Callback(hObject, eventdata, handles)
% hObject    handle to editTxtRatioBipol (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editTxtRatioBipol as text
%        str2double(get(hObject,'String')) returns contents of editTxtRatioBipol as a double


% --- Executes during object creation, after setting all properties.
function editTxtRatioBipol_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editTxtRatioBipol (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function editTxtImgNo_Callback(hObject, eventdata, handles)
% hObject    handle to editTxtImgNo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editTxtImgNo as text
%        str2double(get(hObject,'String')) returns contents of editTxtImgNo as a double


% --- Executes during object creation, after setting all properties.
function editTxtImgNo_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editTxtImgNo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function editTxtCellNo_Callback(hObject, eventdata, handles)
% hObject    handle to editTxtCellNo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editTxtCellNo as text
%        str2double(get(hObject,'String')) returns contents of editTxtCellNo as a double


% --- Executes during object creation, after setting all properties.
function editTxtCellNo_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editTxtCellNo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes when user attempts to close figMainGUI.
function figMainGUI_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figMainGUI (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if isequal(get(hObject, 'waitstatus'), 'waiting')
    % The GUI is still in UIWAIT, us UIRESUME
    uiresume(hObject);
else
    % The GUI is no longer waiting, just close it
    delete(hObject);
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Check boxes trigger

% --- Executes on button press in checkMeanInt.
function checkMeanInt_Callback(hObject, eventdata, handles)
% hObject    handle to checkMeanInt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkMeanInt
status = get(hObject,'Value');
if status
    handles.polarIntTypes(1) = 'S';
else
    handles.polarIntTypes(1) = 'U';
end
handles = updateSpots(handles);
updateTextFields(handles);
guidata(hObject, handles);


% --- Executes on button press in checkSmoothness.
function checkSmoothness_Callback(hObject, eventdata, handles)
% hObject    handle to checkSmoothness (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkSmoothness
status = get(hObject,'Value');
if status
    handles.polarIntTypes(2) = 'S';
else
    handles.polarIntTypes(2) = 'U';
end
handles = updateSpots(handles);
updateTextFields(handles);
guidata(hObject, handles);


% --- Executes on button press in checkMaxInt.
function checkMaxInt_Callback(hObject, eventdata, handles)
% hObject    handle to checkMaxInt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkMaxInt
status = get(hObject,'Value');
if status
    handles.polarIntTypes(3) = 'S';
else
    handles.polarIntTypes(3) = 'U';
end
handles = updateSpots(handles);
updateTextFields(handles);
guidata(hObject, handles);

% --- Executes on button press in checkCorrectBaseline.
function checkCorrectBaseline_Callback(hObject, eventdata, handles)
% hObject    handle to checkCorrectBaseline (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkCorrectBaseline
handles.PeakOptions.correctBaseline = get(hObject,'Value');
handles = updateSpots(handles);
updateAxes(hObject, handles);
updateTextFields(handles);
guidata(hObject, handles);





