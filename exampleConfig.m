%% Input and Output settings:
%%% Necessary:
resultFolder = 'testResults/';
signalFilepath = 'testData/Signal/*.tif';
segmentationFilepath = 'testData/Segmentation/*.tif';
% Outputs are grouped by the combination of all supplied tokens. Hence
% supplied metadata tokens should capture all information for different
% conditions
searchExpr = 'Test_\d{8}_(\d)_(\d+)_(.*)_(\d\d)_.*.tif';
tokenKeys = {'Timepoint', 'Strain No', 'Treatment', 'Temperature'};
% 
%% Optional output settings:
% 
% % Basic filename for output tables:
% outputTablesFilename = 'Output_tables'; % [char vector]
% 
% % Ratio for which cells are classified as polarized
% thresholdRatioPolarization = 0.8; % [scalar, 0-1]
% 
% % Map to convert numeric entries in tokens to strings
% tokenKeyNumbering = {{}, {}, {'nodrug', 'DMSO', 'BFA'}, {}};
% 
% % Additional output complementary to the '.mat' files
% additionalOutput = 'csv'; % ['none' (default), 'csv', 'xls', 'xls_all',
% 'xls_split', 'xls_split_sheets']
% 
% % Addtitional parameters passed to the output generating function:
% % See documentation of xlwrite for 'xls' files and writetable for 'csv' files
% additionaOutputOptions = {'Delimiter', '\t'};
% 
% How to treat existing result files
% config.treatDuplicates = 'dialogue'; % ['dialogue', 'overwrite', 'skip']

%% Intensity results configuration:
% 
% % Basic filename for intensity results
% intensityResFilename = 'intensity_results'; % [char vector]
% 
% % Estimated pixel width of membrane: 
% widthMembrane = 3; % [scalar integer]
% 
% % If individual images should be rescaled from 0 to 1
% rescaleIntensities = false; % [false, true]
% 
%  % Direction of intensity detection relative to segmentation:
% detectionDirection = 'inside'; % ['inside', 'outside', 'both']

%% Peak detection configuration:
%%% Optional (with defaults):
% % Basic filename for spot results
% spotResFilename = 'spot_results';
% 
%%% General peak detection configuration:
% PeakOptions.function =  'ybt.findpeaksWrapper';
% PeakOptions.GUI = 'findpeaksWrapperGUI';
% PeakOptions.correctBaseline = true;
% 
%%% Peak finder configuration:
% % Options for the findpeaks function, Options can be added and deleted at
% % will, see: doc findpeaks
% FinderOptions.MinPeakHeight = 100;
% FinderOptions.MinPeakProminence = 10;
% FinderOptions.MinPeakDistance = 20;
% FinderOptions.MinPeakWidth = 20;
% 
%%% Peak width measurement configuration:
% % Base for width computation:
% WidthOptions.reference = 'HeightCorr'; % ['Height', 'Prominence',
% 'HeightCorr']
% WidthOptions.refLevel = 0.8; [scalar, 0-1];
% WidthOptions.significantLocalDepth = 20; [scalar]
% WidthOptions.significantMaxHeight = 3; [scalar]
% 
% % Options to plot intensities: 
% plotSmoothedIntensities = 'none'; % ['xy', 'heatmap', 'none']
% 
%%% Smoothing filter configuration
% % Implemented filters are:
% % 'average' with the parameter: length
% % 'gaussian' with the parameters: length, sigma
% % 'lowpass' with the parameter: frequencyCutoff
% % 'none': no filtering
% FilterOptions.type = 'lowpass';
% frequency cutoff (0 to 1), the lower the number the more smoothing
% FilterOptions.frequencyCutoff = 0.025; [scalar, 0-1];
% 
%%% Polarization classification characteristics
% % Polarization weights as a cell array of 4-element vectors.
% % First weight reflects importance of mean intensity
% % Second weight reflects importance of size of spots
% % Third weight reflects importance of smoothness of spots
% % Fourth weight reflects importance of max intensity
% % Weights can any positive number or zero for ignoring the feature
% % Values smaller than 1 correspond to diminishing returns, while values
% % larger than 1 correspond to increasing returns
% polarizationWeights = [1,1,0, 0]; [1x4 vector]
%
% % Supports multiple parameter combinations for screening (as cell arayy):
% polarizationWeights = {[1,1,0, 0], [1,1,1,0], [0,1,1,1]}; [cell with 1x4
% vectors]
% 
% % polarizationIntensityTypes: Type of intensity used for calculating the
% % polarization ratio, values: 'S' or 'U'
% % 'U' triggers unsmoothed intensity values, 'S' triggers smoothed intensity
% % values. The first character corresponds to the intensity values used for
% % the mean computation the second character corresponds to the intensity
% % values used for the smoothness computation and the third for the one used
% % for the max intensity values.
% polarizationIntensityTypes = 'SUS';
% 
%%% GUI flag:
% % Bool to choose whether to use the GUI or not
% useGUI = true;
% 