%% Prepare workspace and read config mat file.
ybt.prepareWorkspace('ver');
ybt.prepareWorkspace('paths');
% User can generate confFile variable with path for scripting
if exist('confFile', 'var')
    config = ybt.readConfig(confFile);
else
    config = ybt.readConfig();
end
config = ybt.parseConfig(config);
ybt.prepareWorkspace('parpool');

%% Compute intensity results
% Check if intensity results file already exists and handle according to
% config
intFileFound = isfile(fullfile(config.resultFolder, [config.intensityResFilename, '.mat']));
if intFileFound 
    if strcmp(config.treatDuplicates, 'dialogue')
        computeInt = ybt.overwriteFileDialogue('intensity');
    elseif strcmp(config.treatDuplicates, 'overwrite')
        computeInt = true;
    else
        computeInt = false;
    end
else
    computeInt = true;
end

% Compute intensities or read results
if computeInt
    intInputs = { config.segmentationFilepath, config.signalFilepath, ...
        fullfile(config.resultFolder, config.intensityResFilename), ...
        config.widthMembrane};
    if isfield(config, 'rescaleIntensities')
        intInputs = [intInputs, {config.rescaleIntensities}];
    end
    if isfield(config, 'detectionDirection')
        intInputs = [intInputs, {config.detectionDirection}];
    end
    intensityResults = ybt.computeBorderIntensities(intInputs{:});
else
    mfile = matfile(fullfile(config.resultFolder, config.intensityResFilename));
    intensityResults = mfile.intensResults;    
end

%% Compute spot results
% If the spot result file exists and we did not recompute the intensity
% results, we compute the spot results otherwise we read previous results
spotFileFound = isfile(fullfile(config.resultFolder, [config.spotResFilename, '.mat']));
if spotFileFound && ~computeInt
    if strcmp(config.treatDuplicates, 'dialogue')
        computeSpots = ybt.overwriteFileDialogue('spot');
    elseif strcmp(config.treatDuplicates, 'overwrite')
        computeSpots = true;
    else
        computeSpots = false;
    end
else
    computeSpots = true;
end

if computeSpots
    spotInputs = {intensityResults, config.FilterOptions, config.PeakOptions, ...
        fullfile(config.resultFolder, config.spotResFilename), config.plotSmoothedIntensities, ...
        config.polarizationWeights, config.polarizationIntensityTypes, ...
        config.useGUI};
    [spotResults, FilterOptions, PeakOptions, polarWeights, polarIntTypes ] = ...
        ybt.computeSpotResults( spotInputs{:} );
else
    % Load results from spot result file and parameters from accompanying
    % file
    mfile = matfile(fullfile(config.resultFolder, config.spotResFilename));
    spotResults = mfile.spotResults;  
    mfile = matfile(fullfile(config.resultFolder, [config.spotResFilename, '_parameters']));
    FilterOptions = mfile.FilterOptions;
    PeakOptions = mfile.PeakOptions;
    polarWeights = mfile.polarWeights;
    polarIntTypes = mfile.polarIntTypes;
end

%% Create grouped output based on metadata conditions
% Contrary to intensity results and spot results the output tables are
% always overwritten

tableInputs = {intensityResults, spotResults, config.searchExpr, config.tokenKeys, ...
    fullfile(config.resultFolder, config.outputTablesFilename), ...
    config.thresholdRatioPolarization};
if isfield(config, 'tokenKeyNumbering')
    tableInputs = [tableInputs, {config.tokenKeyNumbering}];
end
if isfield(config, 'additionalOutput')
    tableInputs = [tableInputs, {config.additionalOutput}];
end
if isfield(config, 'additionaOutputOptions')
    tableInputs = [tableInputs, {config.additionaOutputOptions}];
end

[outputSpots, outputObjects] = ybt.manageOutput(tableInputs{:});



