function [overwriteFlag] = overwriteFileDialogue(type)
%OVERWRITEFILEDIALOGUE creates a command line dialogue to check wheter
%existing files should be overwritten

narginchk(1,1);
validateattributes(type, {'char', 'string'}, {'scalartext'}, mfilename, 'type', 1);
prompt = ['A file for the ' type, ' results already exists, overwrite file(o) or skip computation(s)? o/s [o]'];
str = input(prompt,'s');

if isempty(str)
    str = 'o';
else
    str = validatestring(str, {'o', 's'});
end
if strcmpi(str, 'o')
    overwriteFlag = true;
else
    overwriteFlag = false;
end

end

