function [segmImg] = parseSegmentationImg(segmImg)
%PARSESEGMENTATIONIMG valdiates type of segmentation image and converts if
%necessary

validateattributes(segmImg, {'numeric', 'logical'}, {'nonnegative', ...
        'integer', '2d'}, mfilename, 'Segmentation image');

if islogical(segmImg)
    return
else
    maxInt = intmax(class(segmImg));    
    zeroOnes = all(segmImg(:) == 0 | segmImg(:) == 1);
    zeroMax = all(segmImg(:) == 0 | segmImg(:) == maxInt);
    
    if all(segmImg(:) == 0) || zeroOnes || zeroMax
        segmImg = logical(segmImg);
    end
end



end

