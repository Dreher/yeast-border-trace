function [ intensResults ] = computeBorderIntensities( segmentFilepath, signalFilepath, ...
    intensResFilepath, widthMembrane, varargin)
%computeBorderIntensities batch executes measureBorderIntensities on a
%given set of image files
%% Syntax
%   intensResults = computeBorderIntensities(segmentFilepath, signalFilepath, intensResFilepath, widthMembrane)
%   intensResults = computeBorderIntensities(___, rescaleIntens)
%
%% Description
%   intensResults = computeBorderIntensities( segmentFilepath,
%   signalFilepath, intensResFilepath, widthMembrane) batch executes
%   measureBorderIntensities on all images identified by the two given
%   filepaths. measureBorderIntensities computes the intensites along the
%   border of objects identified by a set of segmentation images and it's
%   results based on a corresponding set of signal images and reports those
%   intensities and other associated image derived measurements.
%
%   intRes = computeBorderIntensities(___, rescaleIntens) optionally
%   rescales the intensity of each image according to the maximum intensity
%   value.
%   
%   Progress of the computation is reported to the console and results are
%   saved to given folder.
%
%% Example
%   Setup input and output paths with image and segmentation data
%       segmentFilepath = 'testData/Segmentation/*.tif';
%       signalFilepath = 'testData/Signal/*.tif';
%       intensResFilepath = 'testResults/intensity_results';
%       widthMembrane = 3;
%       [ intensResults ] = computeBorderIntensities( segmentFilepath, ...
%           signalFilepath, intensResFilepath, widthMembrane)
%   Plot an exemplary cell and the corresponding border intensity profile
%       figure;
%       subplot(2,1,1);
%       imshow(intensResults(1).RegionStats(1).signalImage, []);
%       subplot(2,1,2);
%       plot(intensResults(1).borderIntensity(1).maxIntensities)
%
%% Input
%   segmentFilepath -- filepath of segmentation images
%       character vector
%       Filepath identifying all input segmentation images. Allows for '*' as
%       a wildcard. Filenames in this and signalFilepath must be
%       corresponding in alphabetical order.
%
%   signalFilepath -- filepath of segmentation images
%       character vector
%       Filepath identifying all input signal images. Allows for '*' as
%       a wildcard. Filenames in this and segmentFilepath must be
%       corresponding in alphabetical order.
%
%   intensResFilepath -- filepath to saving location with base name
%       character vector
%       Filepath to the disk location where results should be stored, given
%       as path with a basic filename. Execution parameters will be saved
%       in a second file with the same basic filename and _parameters
%       appended.
%
%   widthMembrane -- estimated width of the membrane 
%       scalar positive integer
%       Estimation of membrane width in pixel values inwards from the
%       segmentation.
% 
%   rescaleIntens -- optional flag to trigger rescaling
%       false (default) | bool scalar
%       If set to true each image will be rescaled so that 1 corresponds to
%       the image maximum intensity.
%
%   direction -- optional flag to change direction of membrane expansion
%       character vector - 'inside' | 'outside' | 'both'
%       Controls the direction of the expansion of the border as a ring
%       with a given thickness. 'inside' keepts the expansion limited to
%       the segmentation; 'outside' excludes the segmentation; 'both' is
%       symmetric in regards to the outline of the segmentation.
%
%% Output
%   intensResults -- Nx1 result structure for N files containing the
%   following fields:
%   
%   Fields                  Description
%   signalImageName         full path of the processed signal image file
%   segmentationImageName   full path of the processed segmentation
%                           image file
%   settings                vector with [widhtMembrane, pixels used for fit, 
%                           rescaleIntens]
%   borderIntensity         Mx1 strcuture for M objects in the image and
%                           the mean and max intensities perpendicular to 
%                           the middle ring in the fields: 
%                           'meanIntensities' and 'maxIntensities'
%   ConnectedComponents     Connected Components structure as returned by
%                           bwconncomp
%   RegionStats             Mx1 measurement structure for the found objects
%                           with the fields:%
%                           'Area': Area of the object
%                           'Centroid': Centroid of the object
%                           'BoundingBox': Bounding box of the object
%                           'MajorAxisLength': Major axis of inertia ellipse
%                           'MinorAxisLength': Minor axis of inertia ellipse
%                           'Eccentricity': eccentricity of inertia ellipse
%                           'Orientation': Angle of major axis of inertia ellipse
%                           'Image': Subsampled image of object segmentation
%                           'PixelValues': Intensity values of object
%                           'MeanIntensity': Mean intensity of whole object
%                           'overlayImage': Overlay image of outer and middle border ring
%                           'majorPoints': Intersection of the inertia ellipse axis with the
%                               estimted middle line of the membrane as a 4x2 matrix. Rows are the
%                               points in the order 2 intersections with the major axis and then 2
%                               intersections with the minor axis. Colums are X and Y coordinates.
%                           'majorPointsPerimeter': Intersection of the inertia ellipse axis with
%                               the outer perimeter of the object, in the same form as above.
%                           'signalImage': Subsampled signal image of the object
%                           'skipped': String identifying the reason if an object has been
%                               skipped, is empty otherwise
%                           'boundary': X-Y coordinates of the estimated middle ring of the
%                               membrane
%                           'boundaryNpixels': Number of pixels that constitue the middle ring
%                           'boundaryLength': Euclidean length of the middle ring
% 
%
%% See Also
%   measureBorderIntensities

% Copyright (C) David Dreher 2017

pxlsToFit = 10; % Number of pixels for parametric curve fit

narginchk(4,6)
validateattributes(segmentFilepath, {'string', 'char'}, {'nonempty'}, ...
    mfilename, 'Segmentation file path', 1);
validateattributes(signalFilepath, {'string', 'char'}, {'nonempty'}, ...
    mfilename, 'Signal file path', 2);
validateattributes(intensResFilepath, {'string', 'char'}, {'nonempty'}, ...
    mfilename, 'Intensity result file path', 3);
validateattributes(widthMembrane, {'numeric'}, {'scalar', 'integer', 'positive'},  ...
    mfilename, 'Estimated membrane width', 4);
[rescaleIntens, direction] = parseOptInputs(varargin{:});

% Find image directory and contents and sort file names alphabetically
signalFiles = dir(signalFilepath);
segmenFiles = dir(segmentFilepath);

[~, signalSortIdx] = sort_nat({signalFiles.name});
[~, segmenSortIdx] = sort_nat({segmenFiles.name});

signalFiles = signalFiles(signalSortIdx);
segmenFiles = segmenFiles(segmenSortIdx);


% Check for problems with reading the images
if numel(signalFiles) == 0 
    error([mfilename, ':noSignalImages'], 'Could not find any signal images')
elseif numel(segmenFiles) == 0
    error([mfilename, ':noSegmentImages'], 'Could not find any segmentation images')
elseif numel(signalFiles) ~= numel(segmenFiles)
    error([mfilename, ':mismatchNumberImages'], 'Number of signal images does not match number of segmentation images')
end

nFiles = numel(signalFiles);
% Pre-allocate result structure
intensResults(nFiles, 1) = struct('signalImageName', [],'segmentationImageName',[], ...
    'settings',[],'borderIntensity',[],'ConnectedComponents',[],'RegionStats',[]);

displayProgress('init', 'Starting computation of border intensities');
% Iterate through each image set and compute the intensities
for i = 1:nFiles
    % Write input parameters into result structure for inspection purposes
    intensResults(i).segmentationImageName = ...
        fullfile(segmenFiles(i).folder, segmenFiles(i).name);
    intensResults(i).signalImageName = ...
        fullfile(signalFiles(i).folder, signalFiles(i).name);
    intensResults(i).settings = [widthMembrane, pxlsToFit, rescaleIntens];
    
    % Read the images
    segmenImg = imread(intensResults(i).segmentationImageName);
    validateattributes(segmenImg, {'numeric', 'logical'}, {'nonnegative', ...
        'integer', '2d'}, mfilename, 'Segmentation image');
%     if size(segmenImg, 3)>1
%         error([mfilename, ':RGBor3D'], ...
%             'Segmentation image has more than two dimensions, RGB label images or 3D images are not a valid input. Convert first')
%     end
%     if ~isa(segmenImg, 'logical')
%         segmenImg = segmenImg>0;
%     end
    
    signalImg = imread(intensResults(i).signalImageName);
    % Call the trace function to compute intensity results
    [intensResults(i).borderIntensity, intensResults(i).ConnectedComponents, ...
        intensResults(i).RegionStats, warnState]= ybt.measureBorderIntensities( ...
        segmenImg, signalImg, widthMembrane, pxlsToFit, rescaleIntens, direction );
    if warnState
        dispstat('', 'init');
    end
    displayProgress(i/nFiles);
end
displayProgress('end', 'Finished computation of border intensities, saving results ...');

[resFolder, filename, ext] = fileparts(intensResFilepath);
% Save results
if ~isdir(resFolder)
    mkdir(resFolder);
end
if ~isempty(ext)
    intensResFilepath = [resFolder, filename];
end
save(intensResFilepath, 'intensResults', '-v7.3');
% delete(gcp('nocreate'));

disp(['Border intensity results saved to: ', intensResFilepath]);

save([intensResFilepath, '_parameters'], 'segmentFilepath', 'signalFilepath', ...
    'intensResFilepath', 'widthMembrane', 'rescaleIntens', '-v7.3');

disp(['Border intensity parameters saved to: ', ...
    intensResFilepath, '_parameters']);

end


function [rescaleIntens, direction] = parseOptInputs(varargin)
rescaleIntens = false;
direction = 'inside';
rescalePos = 5;
directionPos = 5;
if isempty(varargin)
    return;
elseif numel(varargin) == 1 && (islogical(varargin{1}) || isnumeric(varargin{1}))
    rescaleIntens = varargin{1};
elseif numel(varargin) == 1
    direction = varargin{1};
else
    rescaleIntens = varargin{1};
    direction = varargin{2};
    directionPos = 6;
end

validateattributes(rescaleIntens, {'logical', 'numeric'}, {'scalar', 'binary'}, ...
    mfilename, 'Intensity rescale flag', rescalePos);
validateattributes(direction, {'string', 'char'}, {'nonempty', 'scalartext'}, ...
    mfilename, 'Membrane extruding direction', directionPos);
direction = validatestring(direction, {'inside', 'outside', 'both'}, ...
    mfilename, 'Membrane extruding direction', directionPos);
end
