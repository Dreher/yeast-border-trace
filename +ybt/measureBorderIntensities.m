function [ borderIntensity, ConnComp, RegionStats, warnState] = ...
    measureBorderIntensities( segmenImg, signalImg, widthMembrane, ...
    pxlsToFit, rescaleFlag, direction)
%measureBorderIntensities computes average intensities across the border of
%roundish shapes
%% Syntax
%   borderIntensity = measureBorderIntensities(segmenImg, signalImg, widthMembrane, pxlsToFit)
%   borderIntensity = measureBorderIntensities(___, rescaleFlag)
%   [borderIntensity, ConnComp] = measureBorderIntensities(___)
%   [borderIntensity, ConnComp, RegionStats] = measureBorderIntensities(___)
%   [borderIntensity, ConnComp, RegionStats, warnState] = measureBorderIntensities(___)
%
%% Description
%   [intensRes, ConnComp, RegionStats, warnState] =
%   measureBorderIntensities( segmenImg, signalImg, widthMembrane,
%   pxlsToFit, rescaleFlag ) computes the intensities along a membrane
%   derived from a segmentation image.
%   From the object segmentation a membrane image is computed based on
%   pixel estimate of the membrane width. From this membrane image a middle
%   ring is computed. For each pixel on this middle ring a parameter curve
%   is fitted to a small part of the ring. Then a line perpendicular to
%   this curve going through the pixel of interest is computed and
%   intensities across this line are measured and inscribed as result for
%   this point. Both mean and max intensities across this line are saved.
%   For additional analysis different measurements like the major points on
%   the middle ring are available in the output.
%   The function expects objects that can be sufficiently fitted with an
%   ellipse and have a smooth, i.e. not jagged, border.
%   A larger segmentation can be accomodated by increasing the membrane
%   with, but the funtion will fail with smaller segmentations.
%   
%   This function utilizes the Parallel Processing Toolbox when available
%   for parallelisation across objects.
%
%% Example
%   Read in signal and segmentation image
%
%       segmenImg = imread('testData/Segmentation/Test_20170101_1_007_nodrug_25_f0.tif');
%       signalImg = imread('testData/Signal/Test_20170101_1_007_nodrug_25_f0.tif');
%       widthMembrane = 3;
%       pxlsToFit = 4;
%       rescaleIntens = false;
%       [intensRes, CC, RegionStats] = measureBorderIntensities(segmenImg, signalImg, widthMembrane, pxlsToFit)
%
%   Plot an example cell and its intensity profile
%
%       figure;
%       subplot(1,2,1);
%       imshow(RegionStats(5).signalImage);
%       subplot(1,2,2);
%       plot(intensRes(5).maxIntensities)
%
%% Input
%   segmenImg -- segmentation image
%       binary 2D image
%       2 dimensional array with values 0 for background and >0 for objects.
%       Dimensions of both images must be identical.
%       Data Types: single | double | int16 | uint8 | uint16 | uint32
%
%   signalImg -- signal image
%       grayscale 2D image
%       Image with the intensity values that should be measured.
%       Dimensions of both images must be identical.
%       Data Types: single | double | int16 | uint8 | uint16 | uint32
%
%   widthMembrane -- estimated width of the membrane
%       scalar positive integer
%       Estimation of membrane width in pixel values inwards from the
%       segmentation.
%       Data Types: single | double
%
%   pxlsToFit -- pixels for parametric curve fit
%       scalar positive integer
%       number of pixels to the left and right of the pixel
%       of interest for which the parametric curve is fitted.
%       Data Types: single | double
%
%   rescaleFlag -- optional flag to trigger rescaling
%       false (default) | bool scalar
%       If set to true each image will be rescaled so that 1 corresponds to
%       the image maximum intensity.
%       Data Types: bool | single | double
%
%   direction -- optional flag to change direction of membrane expansion
%       'inside' (default) | 'outside' | 'both'
%       Controls the direction of the expansion of the border as a ring
%       with a given thickness. 'inside' keepts the expansion limited to
%       the segmentation; 'outside' excludes the segmentation; 'both' is
%       symmetric in regards to the outline of the segmentation.
%       Data Types: character vector
%
%% Output
%   borderIntensity -- Mx1 structure for M objects their intensity measurements
%       struct
%       For each of the M objects in the image and the structure holds the
%       measured mean and max intensities perpendicular to the middle ring
%       in the two fields:
%       'meanIntensities' and 'maxIntensities'
%
%   ConnComp -- connected components
%       struct 
%       Connected components structure as returned from BWCONNCOMP
%
%   RegionStats -- Mx1 structure for M objects their region properties
%       struct
%       For eacht fo the M objects in the image the function computes the
%       following region properties and stores them as fields in the
%       RegionStats structure.
%           'Area': Area of the object
%           'Centroid': Centroid of the object
%           'BoundingBox': Bounding box of the object
%           'MajorAxisLength': Major axis of inertia ellipse
%           'MinorAxisLength': Minor axis of inertia ellipse
%           'Eccentricity': eccentricity of inertia ellipse
%           'Orientation': Angle of major axis of inertia ellipse
%           'Image': Subsampled image of object segmentation
%           'PixelValues': Intensity values of object
%           'MeanIntensity': Mean intensity of whole object
%           'overlayImage': Overlay image of outer and middle border ring
%           'majorPoints': Intersection of the inertia ellipse axis with
%               the estimted middle line of the membrane as a 4x2 matrix.
%               Rows are the points in the order 2 intersections with the
%               major axis and then 2 intersections with the minor axis.
%               Colums are X and Y coordinates.
%           'majorPointsPerimeter': Intersection of the inertia ellipse
%               axis with the outer perimeter of the object, in the same
%               form as above.
%           'signalImage': Subsampled signal image of the object
%           'skipped': String identifying the reason if an object has been
%               skipped, is empty otherwise
%           'boundary': X-Y coordinates of the estimated middle ring of the
%               membrane
%           'boundaryNpixels': Number of pixels that constitue the middle ring
%           'boundaryLength': Euclidean length of the middle ring
%   
%   warnState -- Warning indicator
%       bool
%       Indicator whether a warning has been generated, useful for proper
%       progress report when batch executing.
%
%% See Also
%   computeBorderIntensities
%
%%

% Copyright (C) David Dreher 2017

areaThreshold = 0.5; % threshold to detect area collapse of cells

%% Input checking
narginchk(4,6);
validateattributes(segmenImg, {'numeric', 'logical'}, {'2d', 'integer', 'nonempty',}, ...
    mfilename, 'segmentation image', 1)
validateattributes(signalImg, {'numeric'}, {'2d', 'nonnegative', 'nonempty'}, ...
    mfilename, 'signal image', 2)
if ~isequal(size(segmenImg), size(signalImg))
    error([mfilename, ':mismatchImageSizes'], 'The sizes of the segmentation and signal image must be identical')
end
validateattributes(widthMembrane, {'numeric'}, {'scalar', 'integer', 'positive'},  ...
    mfilename, 'width membrane', 3);
validateattributes(pxlsToFit, {'numeric'}, {'scalar', 'integer', 'positive'},  ...
    mfilename, 'pixels for fit', 4);
if ~exist('rescaleFlag', 'var') || isempty(rescaleFlag)
    rescaleFlag = false;
end
validateattributes(rescaleFlag, {'numeric', 'logical'}, {'scalar', 'binary'},  ...
    mfilename, 'rescale flag', 5);
if ~exist('direction', 'var') || isempty(direction)
    direction = 'inside';
end
validateattributes(direction, {'string', 'char'}, {'nonempty'}, mfilename, 'Membrane extruding direction', 6);
direction = validatestring(direction, {'inside', 'outside', 'both'}, mfilename, 'Membrane extruding direction', 6);

%% Prepare images

segmenImg = imclearborder(segmenImg); % Clear border touching cells
segmenImg = ybt.parseSegmentationImg(segmenImg);
if islogical(segmenImg)
    ConnComp = bwconncomp(segmenImg); % Find connected components = cells
else
    disp('Segmentation is not binary image, but integer valued, assuming label image');
    ConnComp = ybt.labelconncomp(segmenImg);
end
%label_img = labelmatrix(CC_segment_img);

% Find region properties of connected components
if ~isa(signalImg,'double')
    signalImg = double(signalImg);
end      
if rescaleFlag
   signalImg = signalImg./(max(signalImg(:))); 
end

% RegionStats = regionprops(ConnComp, signalImg,'Centroid', 'Orientation', ...
%     'Image', 'BoundingBox', 'MajorAxisLength', 'MinorAxisLength', 'MeanIntensity', ...
%     'PixelValues', 'Area', 'Eccentricity');
% [RegionStats(:).overlayImage] = deal([]);
% [RegionStats(:).majorPoints] = deal([]);
% [RegionStats(:).majorPointsPerimeter] = deal([]);
% [RegionStats(:).signalImage] = deal([]);
% [RegionStats(:).skipped] = deal([]);
% [RegionStats(:).boundary] = deal([]);
% [RegionStats(:).boundaryNpixels] = deal([]);
% [RegionStats(:).boundaryLength] = deal([]);

RegionStats = repmat(struct('Centroid',nan(1,2),'Orientation', nan, ...
    'Image', [], 'BoundingBox', nan(1,4), 'MajorAxisLength', nan, ...
    'MinorAxisLength', nan, 'MeanIntensity', nan, 'PixelValues', [], ...
    'Area', nan, 'Eccentricity', nan, 'overlayImage', [], ...
    'majorPoints', nan(4,2), 'majorPointsPerimeter', nan(4,2), 'signalImage', [], ...
    'skipped', false, 'boundary', [], 'boundaryNpixels', nan, 'boundaryLength', nan), ...
    [ConnComp.NumObjects, 1]);

%% Find cells perimeter and count intensities

% Pre-allocation and internal constants
nObjs = ConnComp.NumObjects;
warnState = false;
if nObjs > 0
    borderIntensity(nObjs,1) = struct('meanIntensities', [],'maxIntensities',[]);
else
    borderIntensity(1) = struct('meanIntensities', [],'maxIntensities',[]);
end

hasDistribComp = license('test', 'Distrib_Computing_Toolbox');
if ~hasDistribComp
    disp('No parallel processing toolbox available, using serial processing')
    useParallel = false;
    warnState = true;
elseif isempty(gcp('nocreate'))
    disp('No matlab pool found, using serial processing')
    useParallel = false;
    warnState = true;
else
    useParallel = true;
end

tmpWarn = false(nObjs,1);
if useParallel
    pixelList = ConnComp.PixelIdxList;
    parfor i = 1:nObjs
        [borderIntensity(i), RegionStats(i), pixelList{i}, ...
            tmpWarn(i)] = loopFunct(pixelList{i}, signalImg, widthMembrane, ...
            pxlsToFit, areaThreshold, direction, i);
    end
    ConnComp.PixelIdxList = pixelList;
else
    for i = 1:nObjs
        [borderIntensity(i), RegionStats(i), ConnComp.PixelIdxList{i}, ...
            tmpWarn(i)] = loopFunct(ConnComp.PixelIdxList{i}, signalImg, widthMembrane, ...
            pxlsToFit, areaThreshold, direction, i);
    end
end

if any(tmpWarn)
    warnState = true;
end
end

%% Subfunctions


function [ currBordInt, currRS, currPxlList, currWarn] = loopFunct(currPxlList, ...
        signImg, widthMembrane, pxlsToFit, areaThreshold, direction, i)

currBordInt = struct('meanIntensities', [],'maxIntensities',[]);
currRS = struct('Centroid',nan(1,2),'Orientation', nan, ...
    'Image', [], 'BoundingBox', nan(1,4), 'MajorAxisLength', nan, ...
    'MinorAxisLength', nan, 'MeanIntensity', nan, 'PixelValues', [], ...
    'Area', nan, 'Eccentricity', nan, 'overlayImage', [], ...
    'majorPoints', nan(4,2), 'majorPointsPerimeter', nan(4,2), 'signalImage', [], ...
    'skipped', false, 'boundary', [], 'boundaryNpixels', nan, 'boundaryLength', nan);
currWarn = false;

profilePoints = zeros(2,2);

fullCellImg = false(size(signImg));
fullCellImg(currPxlList) = true;
% offset = floor(currRS.BoundingBox(1:2)); % Find offset of reduced image
% angle = currRS.Orientation; % Read out angle of ellipse fit
% cellSegmenImg = currRS.Image; % Read out reduced image of just the cell of interest


% Compute image with segmented membrane
perimImg = bwperim(fullCellImg);

switch direction
    case 'inside'
        membraneImg = imdilate(perimImg,strel('disk',widthMembrane, 0)) & fullCellImg;
    case 'outside'
        membraneImg = imdilate(perimImg,strel('disk',widthMembrane, 0) ) & ~fullCellImg;
    case 'both'
        membraneImg = imdilate(perimImg,strel('disk', round(widthMembrane/2), 0) );
    otherwise
        error([mfilename, ':unknownDirection'], 'The given direction is unknown');
end
fullCellImg = membraneImg | fullCellImg;
currPxlList = find(fullCellImg);
warning('off', 'catstruct:DuplicatesFound')
currRS = catstruct(currRS, regionprops(fullCellImg, signImg, ...
    'Centroid', 'Orientation', 'Image', 'BoundingBox', 'MajorAxisLength', ...
    'MinorAxisLength', 'MeanIntensity', 'PixelValues', 'Area', 'Eccentricity'));
warning('on', 'catstruct:DuplicatesFound');

cellSegmenImg = currRS.Image;
cellSignalImg = zeros(size(cellSegmenImg));
cellSignalImg(cellSegmenImg) = currRS.PixelValues;% Take reduced signal image
offset = floor(currRS.BoundingBox(1:2)); % Find offset of reduced image
angle = currRS.Orientation; % Read out angle of ellipse fit
centroid = currRS.Centroid - offset; % Compute centroid
currRS.signalImage = cellSignalImg; % Save reduced signal image

membraneImg = ybt.imCrop(membraneImg, ceil(currRS.BoundingBox));
perimImg = ybt.imCrop(perimImg, ceil(currRS.BoundingBox));
cellSignalImg(~membraneImg) = -1; % Set signal image outside of membrane to -1 to identify them
skelImg = bwmorph(membraneImg,'thin',Inf); % Thin the membrane image down to middle ring
skelImg([1, end], :) = 0; % Set border elements to zeros to improve skeletonization
skelImg(:, [1, end]) = 0;
skelImg = bwmorph(skelImg,'spur',Inf); % Remove spur artifacts
currRS.overlayImage = 100*uint8(perimImg) + 200*uint8(skelImg);

% In case the segmented cell is smaller than expected by membrane width
% then the cell area will collapse and the cell is excluded from the
% analysis
if bwarea(skelImg)< areaThreshold*bwarea(perimImg)
    currRS.skipped = 'area collapse';
    disp(['Cell number: ', num2str(i), ' has been skipped due to area collapse\n']);
    currWarn = true;
    return
end

% Compute the pixel ID list of boundary pixels
boundaryTraced = bwboundaries(skelImg);
perimTraced = bwboundaries(perimImg,'noholes');

if length(boundaryTraced)>2
    currRS.skipped = 'intersection with itself';
    disp(['Cell number: ', num2str(i), ' has been skipped due to intersection with itself']);
    currWarn = true;
    return
end


if length(perimTraced)>1
    currRS.skipped = 'no clean perimeter found';
    disp(['Cell number: ', num2str(i), ' has been skipped since no clean perimeter could be found']);
    currWarn = true;
    return
end

% Convert cell to array and eliminate the last point which is identical to the first point
% Also swap columns to have consistent X-Y coordinate system and not
% row, column indices.
boundaryTraced = boundaryTraced{1}(1:end-1, [2,1]);
perimTraced = perimTraced{1}(1:end-1, [2,1]);

% Determine how many boundary elements exist
boundaryLength = size(boundaryTraced,1);

% Compute and save the major point intersections
majorPoints = ybt.computeMajorPoints(centroid, angle, boundaryTraced);
currRS.majorPoints = majorPoints + repmat(offset,4,1);
currRS.majorPointsPerimeter = ybt.computeMajorPoints(centroid, angle, perimTraced) + repmat(offset,4,1);

% Find starting point and shift boundary pixel list so the starting
% point is the first point in the list
if isnan(majorPoints(1,:))
    currRS.skipped = 'no valid starting point';
    disp(['Cell number: ', num2str(i), ' has been skipped because no valid starting point could be found']);
    currWarn = true;
    return
end
traceIdx = find(ismember(boundaryTraced,majorPoints(1,:),'rows'));
boundaryTraced = circshift(boundaryTraced,-1*traceIdx+1);
currRS.boundary = boundaryTraced;
currRS.boundaryNpixels = size(boundaryTraced, 1);
currRS.boundaryLength = sum(sqrt(diff(boundaryTraced(:,1)).^2 + diff(boundaryTraced(:,2)).^2));

currBordInt.meanIntensities = zeros(boundaryLength,1); % Pre-allocate intensity vector
currBordInt.maxIntensities = zeros(boundaryLength,1);

% Walk along the middle ring and determine for each point on the middle
% ring the average intensity orthogonal to the middle ring.
for k=1:1:boundaryLength
    
    % Find coordinates of boundary points around the current point
    bound = boundaryTraced(wrapIdx(k-pxlsToFit:1:k+pxlsToFit,boundaryLength), :);
    
    % Fit a quadratic parametric curve along the this segment
    t = linspace( 0, 1, size(bound,1) )'; % Define the parameter t
    polyX = polyfit(t , bound(:,1) , 2); % Fit x-component
    polyY = polyfit(t , bound(:,2) , 2); % Fit y-component
    % Compute curve points for parametric values of t
    fitX = polyval(polyX,t);
    fitY = polyval(polyY,t);
    
    % Find the t-value where the parametric curve is closest to the
    % current starting point.
    distFit = hypot(fitX-boundaryTraced(k,1), fitY-boundaryTraced(k,2));
    [~, minDistIdx] = min(distFit);
    tPoint = t(minDistIdx);
    
    % Compute the derivative of the polynomial fit
    polyderX = polyder(polyX);
    polyderY = polyder(polyY);
    
    % Compute the two components of the tangential vector at the
    % t-point
    profileSlope(1) = polyval(polyderX,tPoint);
    profileSlope(2) = polyval(polyderY,tPoint);
    profileSlope = profileSlope./norm(profileSlope); % Normalize vector
    profileSlope = null(profileSlope)'; % Compute normal vector from null space
    
    % Compute start and end points of line segment that's orthogonal to
    % the perimeter
    profilePoints(1,1) = boundaryTraced(k,1) - widthMembrane*profileSlope(1);
    profilePoints(1,2) = boundaryTraced(k,1) + widthMembrane*profileSlope(1);
    profilePoints(2,1) = boundaryTraced(k,2) - widthMembrane*profileSlope(2);
    profilePoints(2,2) = boundaryTraced(k,2) + widthMembrane*profileSlope(2);
    
    % Compute intensity profile along the perpendicular line segment
    intensityProfile = improfile(cellSignalImg,profilePoints(1,:),profilePoints(2,:));
    % Average the computed intensities excluding the elements not on
    % the membrane (tagged above with -1)
    
    result = mean(intensityProfile(intensityProfile>=0));
    if isnumeric(result) && ~isempty(result)
        currBordInt.meanIntensities(k) = result;
    else
        currBordInt.meanIntensities(k) = NaN;
    end
    result = max(intensityProfile(intensityProfile>=0));
    if isnumeric(result) && ~isempty(result)
        currBordInt.maxIntensities(k) = result;
    else
        currBordInt.maxIntensities(k) = NaN;
    end
    
end
end
