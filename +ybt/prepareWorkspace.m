function prepareWorkspace(option)
%PREPAREWORKSPACE prepares the workspace for execution of yeast polar state
%analyzer
narginchk(0,1);
if nargin < 1
    option = 'all';
else
    validateattributes(option, {'char', 'string'}, {'scalartext', 'nonempty'}, mfilename, 'option', 1);
    option = validatestring(option, {'all', 'paths', 'parpool', 'ver'}, mfilename, 'option', 1);
end

if any(strcmp(option, {'all', 'ver'}))
    if verLessThan('matlab', '9.2')
        error('Matlab 2017a is required.')
    end
end

if any(strcmp(option, {'all', 'paths'}))
    ownDir = fileparts( mfilename('fullpath'));
    ownDir = split(ownDir, filesep);
    ownDir = join(ownDir(1:end-1), filesep);
    addpath(genpath(fullfile(ownDir{1}, 'dependencies/')));
    addpath(fullfile(ownDir{1}, 'GUI/'));
    % addpath(fullfile(ownDir{1}, 'src/'));
end

if any(strcmp(option, {'all', 'parpool'}))
    if license('test', 'Distrib_Computing_Toolbox') && isempty(gcp('nocreate'))
        parpool;
    end
end
end

