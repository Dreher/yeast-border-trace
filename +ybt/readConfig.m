function [config] = readConfig(varargin)
%READCONFIG reads a config file with the filepath either as input or chosen
%via GUI.
narginchk(0,1);
persistent lastLocation
if isempty(lastLocation)
    ownDir = fileparts(mfilename('fullpath'));
    lastLocation = fullfile(ownDir, '../exampleConfig.m');
end
if nargin == 0
    [confFile, confFilepath] = uigetfile('*.m','Select the YBT config file', ...
        lastLocation);
    assert(~isnumeric(confFile), [mfilename, ':noFileSelected'], 'No file selected');
    confFile = fullfile(confFilepath, confFile);
    lastLocation = confFile;
else
    confFile = varargin{1};
    validateattributes(confFile, {'char', 'string'}, {'scalartext'}, mfilename, ...
        'Config file path');
    assert(exist(confFile, 'file')==2 , [mfilename, ':noConfigFileFound'], ...
        'Filepath does not correspond to a valid ''.m'' file');
end
% clearvars varargin ownDir confFilepath;
previousWsVars = who;
previousWsVars = setdiff(previousWsVars, {'confFile'});
previousWsVars = [previousWsVars; {'previousWsVars'}];
run(confFile);
config = ws2struct();
config = rmfield(config, previousWsVars);


end

