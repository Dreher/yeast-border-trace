function out = getNoObjectImage()
%GETNOOBJECTIMAGE Summary of this function goes here
%   Detailed explanation goes here

persistent img;
if isempty(img)
    sz = 500;
    img  = insertText(zeros(sz,sz, 'uint8'), round([sz/2,sz/2]), 'No objects', 'FontSize', round(sz/10), ...
        'TextColor', 'white', 'AnchorPoint', 'Center');
end

out = img;
end

