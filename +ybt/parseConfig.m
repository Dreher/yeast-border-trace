function config = parseConfig(config)
%PARSECONFIG parses the given input struct to complement default values

validateattributes(config, {'struct'}, {'nonempty', 'scalar'}, mfilename, 'config', 1);
defaultPeakOpts.function =  'ybt.findpeaksWrapper';
defaultPeakOpts.GUI = 'findpeaksWrapperGUI';
defaultPeakOpts.correctBaseline = true;

defaultFinderOpts.MinPeakHeight = 100;
defaultFinderOpts.MinPeakProminence = 10;
defaultFinderOpts.MinPeakDistance = 20;
defaultFinderOpts.MinPeakWidth = 20;
defaultPeakOpts.FinderOptions = defaultFinderOpts;

defaultWidthOpts.reference = 'HeightCorr';
defaultWidthOpts.refLevel = 0.8;
defaultWidthOpts.significantLocalDepth = 20;
defaultWidthOpts.significantMaxHeight = 3;
defaultPeakOpts.WidthOptions = defaultWidthOpts;

defaultFiltOpts.type = 'lowpass';
defaultFiltOpts.frequencyCutoff = 0.025;

defaultConfig = struct( 'widthMembrane', 3, 'intensityResFilename', 'intensity_results',...
    'spotResFilename', 'spot_results', 'PeakOptions', defaultPeakOpts, ...
    'plotSmoothedIntensities', 'none', 'FilterOptions', defaultFiltOpts, ...
    'polarizationWeights', [1,1,0,0], 'polarizationIntensityTypes', 'SUS', ...
    'useGUI', false, 'outputTablesFilename', ['Output_tables_', datestr(now, 30)], ...
    'thresholdRatioPolarization', 0.8, 'treatDuplicates', 'dialogue');

requiredFields = {'resultFolder', 'signalFilepath', 'segmentationFilepath', ...
    'searchExpr', 'tokenKeys'};
defaultFields = {'confFile', 'widthMembrane', 'intensityResFilename', ...
    'spotResFilename', 'PeakOptions', 'plotSmoothedIntensities', 'FilterOptions', ...
    'polarizationWeights', 'polarizationIntensityTypes', 'useGUI', 'outputTablesFilename', ...
    'thresholdRatioPolarization', 'treatDuplicates'};
optionalFields = {'rescaleIntensities', 'detectionDirection', 'tokenKeyNumbering', ...
    'additionalOutput', 'additionaOutputOptions'};

peakSubFields = {'FinderOptions', 'WidthOptions'};

confFields = fieldnames(config);
reqProvided = ismember(requiredFields, confFields);
missTxt = join(requiredFields(~reqProvided), ', ');
assert(all(reqProvided), [mfilename, ':missingRequiredConfigs'], ...
    ['The following required config inputs are missing: ', missTxt{1}]);

unknownFields = ~ismember(confFields, [requiredFields, defaultFields, ...
    optionalFields, peakSubFields]);
if any(unknownFields)
    unknownTxt = join(confFields(unknownFields), ', ');
    warning([mfilename, ':unknownConfigParameters'], ...
        ['The following unknown config parameters were found in the config file: ', unknownTxt{1}]);
end

for iOpt = 1:numel(defaultFields)
    if ~isfield(config, defaultFields{iOpt})
        config.(defaultFields{iOpt}) = defaultConfig.(defaultFields{iOpt});
    end
end

for iPS = 1:numel(peakSubFields)
    if isfield(config, peakSubFields{iPS})
        config.PeakOptions.(peakSubFields{iPS}) = config.(peakSubFields{iPS});
        config = rmfield(config, peakSubFields{iPS});
    end
end

validateattributes(config.treatDuplicates, {'char', 'string'}, {'scalartext'}, ...
    mfilename, 'treatDuplicates');
    

config.treatDuplicates = validatestring(config.treatDuplicates, ...
    {'dialogue', 'overwrite', 'skip'}, mfilename, 'treatDuplicates');

end