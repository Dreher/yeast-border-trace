function [ CC ] = labelconncomp( L )
%LABELCONNCOMP Summary of this function goes here
%   Detailed explanation goes here

labels = unique(L(:));
labels = labels(labels>0);
CC = struct('Connectivity', nan, 'ImageSize', size(L), ...
    'NumObjects', numel(labels), 'PixelIdxList', {cell(1,numel(labels))});

for iL = 1:numel(labels)
    CC.PixelIdxList{iL} = find(L==labels(iL));
end

end

