function [spotResults, FilterOptions, PeakOptions, polarWeights, polarIntTypes] = ...
    computeSpotResults( intensResults, FilterOptions, ...
    PeakOptions, resultFilepath, plotIntensFlag, polarWeights, ...
    polarIntTypes, useGUI )
%computeSpotResults batch executes smoothStretchIntensities and spotFinder
%on a set of computed border intensity values.
%% Syntax
%   spotResults = computeSpotResults(intensResults, FilterOptions, PeakOptions, resultFilepath, plotIntensFlag, polarWeights)
%   spotResults = computeSpotResults(___, polarIntTypes)
%   spotResults = computeSpotResults(___, polarIntTypes, useGUI)
%   [spotResults, FilterOptions] = computeSpotResults(___)
%   [spotResults, FilterOptions, PeakOptions] = computeSpotResults(___)
%   [spotResults, FilterOptions, PeakOptions, polarWeights, polarIntTypes] = computeSpotResults(___)
%   [spotResults, FilterOptions, PeakOptions, polarWeights, polarIntTypes] = computeSpotResults(___)
%
%% Description
%   [spotResults, FilterOptions, PeakOptions, polarWeights, polarIntTypes] =
%   resultFilepath, plotIntensFlag, polarWeights, polarIntTypes, useGUI)
%   computes the spot classification for one to multiple images. It saves
%   its results automatically to the given result folder. If the user wants
%   an interactive GUI can be started to tune the spot detection.
%
%   This function utilizes the Parallel Processing Toolbox when available
%   for parallelisation across images.
%
%% Example
%   Load previously generated intensity results and parameters
%
%       testData = load('../testData/testData.mat');
%       intRes = testData.intensityResults;
%
%   Set filter to lowpass and and a cutoff frequency between 0 and 1
%
%       filtOpt = struct('type', 'lowpass', 'frequencyCutoff', 0.025);
%
%   Set peak detection algorith to supplied findpeaks wrapper and use the
%   GUI which lets us pick parameters
%
%       peakOpt = testData.PeakOptions;
%       useGUI = true;
%
%   Set output parameters
%
%       resFile = 'testResults/spotResults';
%       plotF = 'none';
%       polarWeights = [1,1,0,0];
%       intTypes = 'SUS';
%
%   Start processing with interactive GUI to change parameters
%
%       computeSpotResults( intRes, filtOpt, peakOpt, resFile, plotF, polarWeights, intTypes, useGUI )
%
%% Input
%   intensResults -- result structure from computeBorderIntensities
%       struct
%       Nx1 result structure for each of N files containing the results from
%       computeBorderIntensities: border intensities and metadata of objects.
%
%   FilterOptions -- parameters struct for filtering of raw intensity signal. 
%       struct
%       Struct with parameters for the filtering of the raw intensity
%       signal. It must contain a field 'type' to identify the kind of
%       filter used, types currently supported: 'gaussian', 'average',
%       'lowpass' and 'none'. Further fields can be supplied to pass
%       additional parameters depending on the filter type, see the
%       documentation of smoothStretchIntensities for more information.
%
%   PeakOptions --  parameters struct for peak detection
%       struct
%       Struct with parameters to to detect the spots or peaks in the
%       signal intensity profile. It must contain the fields:
%       'function': Name of the detection function that must accept three
%           arguments: the intensity profile, the detection parameters and
%           the width measurement parameters and returns: [ the maximum
%           value for each spot, the location of each spot, the width of
%           each spot and the baselevel of each spot for which the width
%           was computed]. See findpeaksWrapper for an example.
%       'FinderOptions': Parameters struct for spot detection
%       'WidthOptions': Parameters struct for spot size measurement
%       Optional fields are:
%       'correctBaseline': Bool flag for normalization of each intensity
%           profile so that min(I)=0
%       'GUI': The name of a GUI for the peak detetion function, to allow
%           displaying a user interface for parameter tuning. The GUI must
%           return two outputs: the FinderOptions and the WidthOptions, if
%           it returns empty arrays, it will be interpreted as a cancel of
%           change of parameters and the previous parameters are retained.
%   
%   resultFilepath -- filepath to saving location with base name
%       character vector
%       Filepath to the disk location where results should be stored, given
%       as path with a basic filename. Execution parameters will be saved
%       in a second file with the same basic filename and _parameters
%       appended.
%
%   plotIntensFlag -- plot type
%       character vector 
%       String identifying whether intermediate plots of intensities should
%       be saved to disk. Possible entries are 'xy' for an xy-plot,
%       'heatmap' for a heatmap with the intensities or 'none' for no plot.
%
%   polarWeights -- weight vector or cell array of weight vectors
%       4-element vector | cell array with 4-element vectors
%       Either a 4-element vector or cell array of 4-element vectors. In
%       case of a cell array polarozation and bipolarity ratios are
%       computed for each weight vector. Each describe an exponential
%       weight for the following measurements:
%       1. Size of the spot
%       2. Mean intensity of the spot
%       3. Smoothness of the spot
%       4. Max intensity of the spot
%
%   polarIntTypes -- string identifying type of intensity for scoring
%       'SUS' (default) | 3-element character vector of 'S' or 'U'
%       A 3-element string conisting of 'S's or 'U's identifying the type
%       of intensity that should be used for the different measurements.
%       'S' refers to smoothed/filtered intensities, 'U' refers to
%       unsmoothed/unfiltered intensities. The first letter corresponds to
%       the intensity values used for mean intensity computation, the
%       second one to intensity values for smoothness and the third for max
%       intensity.
%
%   useGUI -- start GUI
%       false (default) | bool scalar
%       Bool flag whether the interactive GUI should be started for
%       parameter tuning.
%
%% Output
%
%   spotResults -- Nx1 structure for N image files with M objects in each
%   containing the following fields:
%   
%   Fields          Description
%   ySmoothed       Filtered/smoothed intensity values as a Mx1000 matrix
%   yUpsampled      Non filtered, only upsampled intensity values as Mx1000
%                   matrix
%   objectLabels    Mx1 vector with the IDs of the cells for which a proper
%                   spot detection could be computed (For degenerated cells
%                   a spot computation may fail resulting in a skipped row
%                   but to allow for later identification in the
%                   corresponding image this ID-vector can be used)
%   totalCount      Total number of spots detected
%   scaling         Ratio of actual actual border length and the shared
%                   interpolation length as Mx1 vector
%   spots           Result structure from spotFinder, see its documentation
%                   for detailed information. Contains the following
%                   fields: polarSpots, sideSpots, totalCount,
%                   ratioPolarization, ratioBipolarity, label and
%                   additional fields reporting polarization and bipolarity
%                   ratios in case more than one weight vector was
%                   supplied.
%   
%   Additionally to its results it also outputs the actual used parameters
%   for FilterOptions, PeakOptions, polarWeights, polarIntTypes, since the
%   user has the possibility to change them through the GUI after executing
%   the function. Also the parameters are stored in  a MAT-file
%   for documentation purposes.
%   
%% See Also
%   computeBorderIntensities, smoothStretchIntensities, spotFinder

% Copyright (C) David Dreher 2017



intensType = 'max';

%% Input checking
narginchk(6,8)
validateattributes(intensResults, {'struct'}, {'nonempty'}, mfilename, 'Intensity result structure', 1);
validateattributes(FilterOptions, {'struct'}, {'nonempty'}, mfilename, 'FilterOptions structure', 2);
validateattributes(PeakOptions, {'struct'}, {'nonempty'}, mfilename, 'PeakOptions structure', 3);
validateattributes(resultFilepath, {'string', 'char'}, {'nonempty'}, mfilename,'result file path', 4);
validateattributes(plotIntensFlag, {'string', 'char'}, {'nonempty'}, mfilename, 'plot intensities flag', 5);

if ~exist('useGUI', 'var') || isempty(useGUI)
    useGUI = false;
end
validateattributes(useGUI, {'numeric', 'logical'}, {'scalar', 'binary'}, ...
    mfilename, 'GUI mode flag', 8);

if iscell(polarWeights)
    if numel(polarWeights) > 1 && useGUI
        error([mfilename, ':guiAndMultipleWeights'], ...
            'The GUI currently does not support more than one weight tuple');
    end
    validateattributes(polarWeights, {'cell'}, {'nonempty'}, mfilename, ...
        'cell array of polarization weights', 6);
    validateWeights = @(x) validateattributes(x, {'numeric'}, {'numel', 4, 'real', ...
        'finite', 'vector'}, mfilename, 'weights in cell');
    cellfun(validateWeights, polarWeights);
else
    validateattributes(polarWeights,  {'numeric'}, {'numel', 4, 'real', ...
        'finite', 'vector'}, mfilename, 'vector of polarWeights', 6);
    polarWeights = {polarWeights(:)};
end

if ~exist('polarIntTypes', 'var') || isempty(polarIntTypes)
    polarIntTypes = 'SUS';
end
validateattributes(polarIntTypes, {'string', 'char'}, {'nonempty', 'numel', 3}, ...
    mfilename, 'polarization intensity types', 7);
if isempty(regexpi(polarIntTypes, '[SU]{3}'))
    error('computeSpotResults:polarIntTypes', ...
        'Polar intensity types must be a 3 element string with either ''S'' or ''U''')
end




%% Main
resultFolder = fileparts(resultFilepath);
% Check for result folder and if needed create it.
if ~isdir(resultFolder)
    mkdir(resultFolder)
end

nImgs = numel(intensResults);
% Pre-allocate spot_results structure
spotResults(nImgs, 1) = struct('ySmoothed', [], 'yUpsampled', [], 'spots', [], ...
    'objectLabels', [], 'totalCount', [], 'scaling', []);
% Generate path information for output pdf
outIntensFilePath = [resultFilepath, '_intFigs_'];

if useGUI
    [executeAnalysis, FilterOptions, PeakOptions, polarWeights, polarIntTypes] = ...
        yeastPolarGUI(intensResults, intensType, FilterOptions, PeakOptions, ...
        polarWeights, polarIntTypes);
else
    executeAnalysis = true;
end

if executeAnalysis
    % For each image
    if ~isempty(gcp('nocreate'))
        ppm =  ParforProgMon('Spot detection in parallel running', nImgs);
        parfor currImgIdx = 1:nImgs
            spotResults(currImgIdx) = loopSpotDetect(intensResults(currImgIdx), ...
                outIntensFilePath, FilterOptions, intensType, plotIntensFlag, ...
                PeakOptions, polarWeights, polarIntTypes);
            ppm.increment();
        end
    else
        displayProgress('init', 'Starting spot detection');
        for currImgIdx = 1:nImgs
            spotResults(currImgIdx) = loopSpotDetect(intensResults(currImgIdx), ...
                outIntensFilePath, FilterOptions, intensType, plotIntensFlag, ...
                PeakOptions, polarWeights, polarIntTypes);
            displayProgress(currImgIdx / nImgs);
        end
        displayProgress('end', 'Finished spot detection, saving results...');
    end
    
    % Save the numeric results.
    save(resultFilepath, 'spotResults');
    
    disp(['Spot detection results saved to: ', resultFilepath]);
    
    save([resultFilepath, '_parameters'], 'PeakOptions', 'plotIntensFlag', ...
        'FilterOptions', 'polarWeights', 'polarIntTypes', 'intensType');
    
    disp(['Spot detection parameters saved to: ', resultFilepath, '_parameters']);
else
    error([mfilename, ':hitQuitButton'], 'User hit quit button');
end


end

%% Loop subfunction

function partSpotResults = loopSpotDetect(intensResults, outIntensFilePath, ...
    FilterOptions, intensType, plotIntensFlag, PeakOptions,  polarWeights, ...
    polarIntTypes)

[~, fname] = fileparts(intensResults.signalImageName);
outFigName = [outIntensFilePath , fname];
    
[ ySm, yUps, objectLabels, scaling ] = ybt.smoothStretchIntensities(...
    intensResults, FilterOptions, intensType, plotIntensFlag, outFigName);

% IF statements should not be necessary anymore but are left intact to
% be sure.
if ~isempty(ySm)
    % Call the classification function for the spots.
    spots = ybt.spotFinder( ySm, yUps, objectLabels, PeakOptions, polarWeights, polarIntTypes);
else
    spots = [];
end

% Add the results to the output structure.
partSpotResults.ySmoothed = ySm;
partSpotResults.yUpsampled = yUps;
partSpotResults.spots = spots;
partSpotResults.objectLabels = objectLabels;
partSpotResults.scaling = scaling;
if ~isempty(spots)
    partSpotResults.totalCount = sum([spots.totalCount]);
else
    partSpotResults.totalCount = 0;
end

end

