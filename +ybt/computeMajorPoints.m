function [majorPoints] = computeMajorPoints(centroid, angle, boundary)
%computeMajorPoints computes the intersection of the inertia ellipse axis
%with the given boundary
%
%% Syntax
%   majorPoints = computeMajorPoints(centroid, angle, boundary)
%
%% Description
%   majorPoints = computeMajorPoints(centroid, angle, boundary) computes
%   the 4 intersections of 2 perpendicular axis with a given boundary. If
%   no intersection can be found for an axis corresponding values are NaN.
% 
%% Example
%   Create a circular boundary
%
%       centroid = [0,0];
%       angle = 45;
%       circAng=0:0.01:2*pi;
%       xb = sin(circAng) + centroid(1);
%       yb = cos(circAng) + centroid(2);
%       boundary = [xb(:), yb(:)];
%
%   Detect the intersection of the given axis and its perpendicular
%   counterpart with the given boundary and plot the result:
%
%       majorPoints = computeMajorPoints(centroid, angle, boundary);
%       figure;
%       plot(xb,yb);
%       hold on;
%       axis equal;
%       set(gca,'Ydir','reverse')
%       plot(majorPoints(1,1), majorPoints(1,2), 'ro');
%       plot(majorPoints(2,1), majorPoints(2,2), 'rx');
%       plot(majorPoints(3,1), majorPoints(3,2), 'bo');
%       plot(majorPoints(4,1), majorPoints(4,2), 'bx');
%       legend('Circle data', '1st axis intersection RHP', ...
%           '1st axis intersection LHP', '2nd intersection RHP', ...
%           '2nd intersection LHP', 'Location', 'best');
%
%% Input
%   centroid -- center of axis/ellipse
%       2-element real vector
%       centroid is a 2 element vector with the Y-X coordinates of the
%       centroid (following the row - column convention of the image
%       processing toolbox).
%   
%   angle -- angle of major axis (in image coordinate system)
%       real scalar
%       angle is a scalar indicating the angle between the first axis and
%       the horizontal axis. The value is in degrees, ranging from -90 to
%       90 degrees. Note that it expects an image based coordinate system
%       with origin at the top left corner. So for a normal cartesian
%       coordinate system the angle must be multiplied with -1.
%
%   boundary -- boundary for which intersection should be computed
%       Nx2 real matrix
%       boundary is a Nx2 matrix where each row corresponds to the Y-X
%       coordinatesof one boundary point (following the row - column
%       convention of the image processing toolbox).
%   
%
%% Output
%   majorPoints -- intersection points of axis
%       4x2 matrix
%       majorPoints is a 4x2 matrix where each row corresponds to the X-Y
%       coordinates of the intersection of the axis with the boundary. The
%       order is the following:
%       1. Right half plane intersection of the first axis (given by the angle)
%       2. Left half plane intersection of the first axis (given by the angle)
%       3. Right half plane intersection of the second axis (perpendicular
%          to the first)
%       4. Left half plane intersection of the second axis (perpendicular
%          to the first)
%
%%
% Copyright (C) David Dreher 2015

narginchk(3,3);
validateattributes(centroid, {'numeric'}, {'numel', 2, 'real'}, mfilename, ...
    'Centroid coordinates', 1);
validateattributes(angle, {'numeric'}, {'scalar', 'real'}, mfilename, ...
    'Major axis angle in degree', 2);
validateattributes(boundary, {'numeric'}, {'size', [nan, 2], 'real'}, mfilename, ...
    'Boundary coordinates', 3);

startPntMajor = zeros(1,2); 
endPntMajor = zeros(1,2);
endPntMinor = zeros(1,2);
majorPoints = zeros(4,2);
boundaryLength = size(boundary,1); % Determine how many boundary elements exist
distMajor = zeros(boundaryLength,1); % Pre-allocate distance vector
distMinor = zeros(boundaryLength,1); % Pre-allocate distance vector

% Find start and endpoints of Major Axis Vector
startPntMajor(1) = centroid(1);
startPntMajor(2) = centroid(2);
endPntMajor(1) = centroid(1) + cosd(angle)*100;
endPntMajor(2) = centroid(2) - sind(angle)*100;



% Compute distances for each point from the major axis vector
for l=1:boundaryLength
    testPnt = boundary(l,:); % Take a point on the middle ring
    % Compute the distance from the major axis vector
    distMajor(l) = abs(det([endPntMajor - startPntMajor;startPntMajor - testPnt]))/norm(endPntMajor-startPntMajor);
end

% Find the starting point of the boundary trace that lies on the right
% half plane and on the major axis
rhpIdx = find(boundary(:,1) >= startPntMajor(1)); % Find indices of all pixels that lie in the right half plane
[~, minIdx] = min(distMajor(rhpIdx)); % Find indices of point closest to the major axis
tmpPnts = boundary(rhpIdx(minIdx), :);

if isnumeric(tmpPnts) && ~isempty(tmpPnts)
    majorPoints(1,:) = tmpPnts;
else
    majorPoints(1,:) = NaN;
end

lhpIdx = find(boundary(:,1) < startPntMajor(1)); % Find indices of all pixels that lie in the left half plane
[~, minIdx] = min(distMajor(lhpIdx)); % Find indices of point closest to the major axis
tmpPnts = boundary(lhpIdx(minIdx), :);

if isnumeric(tmpPnts) && ~isempty(tmpPnts)
    majorPoints(2,:) = tmpPnts;
else
    majorPoints(2,:) = NaN;
end

startPntMinor = mean(majorPoints(1:2,:));
endPntMinor(1) = startPntMinor(1) + cosd(angle+90)*100;
endPntMinor(2) = startPntMinor(2) - sind(angle+90)*100;

for l = 1:boundaryLength
    testPnt = boundary(l,:); % Take a point on the middle ring
    % Compute the distance from the major axis vector
    distMinor(l) = abs(det([endPntMinor - startPntMinor;startPntMinor - testPnt]))/norm(endPntMinor-startPntMinor);
end

rhpIdx = find(boundary(:,1) >= startPntMinor(1)); % Find indices of all pixels that lie in the right half plane
[~, minIdx] = min(distMinor(rhpIdx)); % Find indices of point closest to the major axis
tmpPnts = boundary(rhpIdx(minIdx), :);

if isnumeric(tmpPnts) && ~isempty(tmpPnts)
    majorPoints(3,:) = tmpPnts;
else
    majorPoints(3,:) = NaN;
end

lhpIdx = find(boundary(:,1) < startPntMinor(1)); % Find indices of all pixels that lie in the left half plane
[~, minIdx] = min(distMinor(lhpIdx)); % Find indices of point closest to the major axis
tmpPnts = boundary(lhpIdx(minIdx), :);

if isnumeric(tmpPnts) && ~isempty(tmpPnts)
    majorPoints(4,:) = tmpPnts;
else
    majorPoints(4,:) = NaN;
end

end
