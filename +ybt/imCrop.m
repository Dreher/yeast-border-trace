function [ Icr ] = imCrop( I, bb )
%IMCROP Summary of this function goes here
%   Detailed explanation goes here

nD = ndims(I);
validateattributes(I, {'numeric', 'logical', 'gpuArray'}, {'nonempty'}, mfilename, 'I', 1);
validateattributes(bb, {'numeric', 'gpuArray'}, {'vector', 'integer', 'numel', 2*nD}, mfilename, 'Bounding Box', 2);

Isz = size(I);
[bb([1,3]), bb([2,4])] = deal(bb([2,4]), bb([1,3]));
idx = arrayfun(@(start, length, lims) max(start,1):min(start+length-1, lims), ...
    bb(1:nD), bb(nD+1:end), Isz, 'UniformOutput', false);

Icr = I(idx{:});

end

