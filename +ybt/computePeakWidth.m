function [idx, base] = computePeakWidth(y, loc, value, prom, WidthOptions)
%computePeakWidth is a function to measure peak width based on horizontal
%intersection at a given value but restricted by local minima
%
%% Syntax
%   idx = computePeakWidth(y, loc, value, prom, WidthOptions)
%   [idx, base] = computePeakWidth(y, loc, value, prom, WidthOptions)
%
%% Description
%   [idx, base] = computePeakWidth(y, loc, value, prom, WidthOptions)
%   detects the width of a peak by finding the intersection of the curve
%   with a horizontal line at a specific value. The width can then be
%   further restricted in case of strong local minima between intersection
%   and peak.
%
%% Example
%   Create example signal of two overlapping sin waves
%
%       x = linspace(0,2*pi,100);
%       y = sin(x*2) + sin(x*4);
%
% Set width parameters so that the width is measured at 25% of the
% corrected height.
%
%       widthOpt.reference = 'heightCorr';
%       widthOpt.refLevel = 0.75;
%       widthOpt.significantLocalDepth = 0.2;
%       widthOpt.significantMaxHeight = 0.1;
%
%   A peak is at index 58 with a prominence of 3.5145 (use findoeaks to
%   detect peaks)
%
%       loc = 58;
%       prom = 3.5145
%       val = y(loc);
%       [idx, base] = computePeakWidth(y, loc, val, prom, widthOpt)
%
%   Plot the results and mark the detected peak and its width
%
%       plot(x,y);
%       hold on;
%       plot(x(loc), val, 'ro');
%       plot(x(idx), repmat(rowVect(val - base.*widthOpt.refLevel), [2,1]));
%       legend('y', 'peak values', 'width restricted by local minima', 'Location', 'best');
%
%% Input
%   y -- input signal
%       double vector
%       y is the vector for which peaks should be measured.
%
%   loc -- peak index
%       scalar positive integer
%       loc is the scalar location index of the peak
%
%   value -- peak value
%       double scalar
%       value is the scalar peak value
%   
%   prom -- peak prominence
%       double scalar
%       prom is the scalar prominence of the peak
%
%   widthOptions -- width measurement parameter struct
%       struct
%       widthOptions is structure with the following fields:
%       'reference': a string identifying the type of reference for the
%           width measurement, possible values are height, prominence, or
%           heightCorr for the height above the minimum of y.
%   	'refLevel': a scalar indicating the height level of the reference
%           for which the width is measured, from the top position
%           downward. Example: the reference type is height and the
%           detected peak has a height of 100. A reference level of 0.5
%           would measure the width at half height or 50, a reference level
%           of 0.75 would measure the width at quarter height or 25.
%   	'significantLocalDepth': the width measurement is restricted by
%           strong local minima. This scalar corresponds to the depth, i.e.
%           the difference in value between the minima and its smallest
%           neighboring maxima. Minima shallower than this value are not
%           restricting the width measurement.
%       'significantMaxHeight': a scalar indicating the minimum height
%           difference for a neighboring local maxima to properly define
%           depth in noisy conditions. Must be smaller than
%           significantLocalDepth.
%
%% Ouput
%   idx -- left and right starting index of peaks
%       2-element vector
%       idx is 1x2 vector with the left and right start of the width
%       measurement
%   
%   base -- reference value
%       double scalar
%       base is the computed reference value of the peak
%
%% See Also
%   findpeaksWrapper, findpeaks
%%
% Copyright (C) David Dreher 2017


narginchk(5,5)
validateattributes(y, {'numeric'}, {'vector'}, mfilename, 'y', 1);
validateattributes(loc, {'numeric'}, {'scalar', 'integer', 'positive'}, ...
    mfilename, 'loc', 2);
validateattributes(value, {'numeric'}, {'scalar'}, mfilename, 'peak value', 3);
validateattributes(prom, {'numeric'}, {'scalar'}, mfilename, 'prominence', 4);
validateattributes(WidthOptions, {'struct'}, {'nonempty'}, mfilename, 'WidthOptions structure', 5);

reqFields = {'significantLocalDepth', 'significantMaxHeight', 'refLevel', 'reference'};
reqFieldAvail = isfield(WidthOptions, reqFields);
if any(reqFieldAvail==0)
    error([mfilename, ':missingWidthOption'], 'A necessary WidthOptions field is missing')
end
if numel(reqFields) ~= numel(fieldnames(WidthOptions))
    error([mfilename, ':unknownWidthOption'], 'A WidthOptions field is unknown')
end
if WidthOptions.significantMaxHeight >= WidthOptions.significantLocalDepth
    error([mfilename, ':maxHeightLargerThanDepth'], ...
        'The significant maximum height can''t be larger than the significant depth');
end

widthFactor = WidthOptions.refLevel;
if strcmpi(WidthOptions.reference, 'height')
    base = max(value, 0);
elseif strcmpi(WidthOptions.reference, 'prominence')
    base = prom;
elseif strcmpi(WidthOptions.reference, 'heightCorr')
    base = value - min(y);
else
    error([mfilename ':WidthOptions.referenceUnknown'], 'Unknown identifier for base level')
end

yWidth = value - base*widthFactor;

x = 1:numel(y);
% Compute intersections of horizontal line at y_width value with intensity
% curve
idxLeft = find(y(1:loc)<yWidth,1,'last');
idxRight = find(y(loc:end) < yWidth, 1, 'first') + loc - 1;

if isempty(idxLeft)
    idxLeft = 1;
end
if isempty(idxRight)
    idxRight = numel(x);
end


% Find regional minimas inbetween these two points
localMins = imregionalmin(y(idxLeft:idxRight));
localMins([1, end]) = false;
localMax = find(imregionalmax(y));
% Find the corresponding x values for these regional minimas
idxMins = find(localMins) + idxLeft - 1;

% Compute depth, i.e. difference to lowest surrounding maxima
localMinDepth = nan(size(idxMins));
for i = 1:numel(idxMins)
    allLeftMax = localMax(localMax < idxMins(i));
    allLeftMaxHeight = abs(y(allLeftMax) - y(idxMins(i)));
    leftMax = max( allLeftMax(allLeftMaxHeight > WidthOptions.significantMaxHeight) );
    allRightMax = localMax(localMax > idxMins(i));
    allRightMaxHeight = abs(y(allRightMax) - y(idxMins(i)));
    rightMax = min( allRightMax(allRightMaxHeight > WidthOptions.significantMaxHeight) );
    
    leftRight = nan(1,2);
    if ~isempty(leftMax)
        leftRight(1) = y(leftMax);
    end
    if ~isempty(rightMax)
        leftRight(2) = y(rightMax);
    end
    localMinDepth(i) = min( abs(leftRight - [y(idxMins(i)), y(idxMins(i))]) );
end


% Get the regional minimas closed to the current location that are deeper
% than the given threshold
closestLeft = max(idxMins(localMinDepth > WidthOptions.significantLocalDepth & idxMins < loc));
closestRight = min(idxMins(localMinDepth > WidthOptions.significantLocalDepth & idxMins > loc));

% Set the output vector to the found indices
if isempty(closestLeft)
    idx(1) = idxLeft;
else
    idx(1) = closestLeft;
end
if isempty(closestRight)
    idx(2) = idxRight;
else
    idx(2) = closestRight;
end

end