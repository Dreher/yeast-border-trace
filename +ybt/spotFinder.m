function spots = spotFinder( ySmoothed, yUpsampled, objectLabels, PeakOptions, ...
    polarWeights, polarIntTypes)
%spotFinder Identifies and classifies spots, i.e. high intensity regions on
%the membrane using a custom supplied detection algorithm and computes
%polarization and bipolarity ratios.
%
%% Syntax
%   spots = spotFinder(ySmoothed, yUpsampled, objectLabels, PeakOptions, polarWeights)
%   spots = spotFinder(___, polarIntTypes)
%
%% Description
%   spots = spotFinder( ySmoothed, yUpsampled, objectLabels, PeakOptions,
%   polarWeights, polarIntTypes) identifies and classifies spots into two
%   classes depending on their relative location on the membrane: polar/tip
%   located and side spots.
%   Spots are identified as high intensity regions via a peak detection
%   algorithm. A wrapper for the Matlab built-in findpeaks function is
%   supplied but any custom generated function can be supplied to adapt the
%   detection to your experiment conditions. Additionally to the normal
%   findpeaks operation this function supplies a custom peak width
%   measurement function to allow for more differentiated with measurements
%   than only half-height and half-prominence as in findpeaks.
%   Each spot has measurements associated with it: Size, location, mean and
%   max intensity, roughness (approximated by the standard deviation of the
%   derivative). With these measurements an importance score is calculated
%   for each spot depending on the weights.
%   With mu as mean intensity, L as size, R as roughness, M as max
%   intensity and w_mu, w_L, w_R, w_M as corresponding weights the
%   score is computed as:
%
%   S = mu^w_mu * L^w_L * 1/R^w_R * M^w_M
%
%   Polarization ratio is then computed as the ratio of scores of polar
%   spots and total score of all spots.
%   Bipolarity ratio is defined as ratio of difference in scores between
%   both polar spots normlized to 0-1 range, with 1 being equal scores and
%   0 being only one spot.
%
%% Example
%   Load prepared intensity results and parameters
%
%       testData = load('testData/testData.mat');
%       peakOpt = testData.PeakOptions;
%       ySm = testData.ySmoothed;
%       yUp = testData.yUpsampled;
%
%   Create dummy object labels
%
%       objectLabels = 1:size(ySm,1);
%
%   Set weights
%
%       polarWeights = [1,1,0,0];
%
%   Detect spots and plot histogram of polarization ratios
%
%       spots = spotFinder( ySm, yUp, objectLabels, peakOpt, polarWeights)
%       histogram([spots.ratioPolarization], 25);
%
%% Input
%   ySmoothed -- smoothed intensity values
%       Mx1000 double matrix
%       A Mx1000 matrix with the smoothed and interpolated intensity
%       values. Columns correspond to position on the membrane and rows to
%       the M objects found.
%   
%   yUpsampled -- unsmoothed intensity values
%       Mx1000 double matrix
%       A Mx1000 matrix with the upsampled but unsmoothed intensity values.
%       Columns correspond to position on the membrane and rows to the M
%       objects found.
%
%   objectLabels -- object IDs
%       Mx1 double vector
%       A M-element vector holding the original object ID
%
%   PeakOptions --  parameters struct for peak detection
%       scalar struct
%       Struct with parameters to to detect the spots or peaks in the
%       signal intensity profile. It must contain the fields:
%       'function': Name of the detection function that must accept three
%           arguments: the intensity profile, the detection parameters and
%           the width measurement parameters and returns: [ the maximum
%           value for each spot, the location of each spot, the width of
%           each spot and the baselevel of each spot for which the width
%           was computed]. See findpeaksWrapper for an example.
%       'FinderOptions': Parameters struct for spot detection
%       'WidthOptions': Parameters struct for spot size measurement
%       Optional fields are:
%       'correctBaseline': Bool flag for normalization of each intensity
%           profile so that min(I)=0
%       'GUI': The name of a GUI for the peak detetion function, to allow
%           displaying a user interface for parameter tuning. The GUI must
%           return two outputs: the FinderOptions and the WidthOptions, if
%           it returns empty arrays, it will be interpreted as a cancel of
%           change of parameters and the previous parameters are retained.
%
%   polarWeights -- weight vector or cell array of weight vectors
%       4-element vector | cell array with 4-element vectors
%       Either a 4-element vector or cell array of 4-element vectors. In
%       case of a cell array polarization and bipolarity ratios are
%       computed for each weight vector. Each describe an exponential
%       weight for the following measurements:
%       1. Size of the spot
%       2. Mean intensity of the spot
%       3. Smoothness of the spot
%       4. Max intensity of the spot
%
%   polarIntTypes -- string identifying type of intensity for scoring
%       'SUS' (default) | 3-element character vector of 'S' or 'U'
%       A 3-element string conisting of 'S's or 'U's identifying the type
%       of intensity that should be used for the different measurements.
%       'S' refers to smoothed/filtered intensities, 'U' refers to
%       unsmoothed/unfiltered intensities. The first letter corresponds to
%       the intensity values used for mean intensity computation, the
%       second one to intensity values for smoothness and the third for max
%       intensity.
%
%% Output
%
%   spots -- a Mx1 structure holding for each of the M objects the following
%   fields:
%
%   Fields              Description
%   polarSpots          structure with the measurements for P spots classified
%                       as polar spots
%   sideSpots           structure with the measurements for P spots classified
%                       as side spots
%
%       Each of these holds the following measurements as fields in the
%       structure:
%       Fields              Description
%       count               number of spots of this type on this object
%   	sizes               vector with the measured spot sizes
%   	meanSm              vector with mean intensities of each spot
%                           computed from the smoothed intensity values
%       meanUp              vector with mean intensities of each spot
%                           computed from the unsmoothed intensity values
%   	leftMins            vector with left boundaries of spots
%   	rightMins           vector with right boundaries of spots
%       peakLocs            vector with locatiosns of peak values of spots
%       intensityValuesSm   vector with the smoothed intensity values
%       intensityValuesUp   vector with the unsmoothed intensity values
%       peakBase            vector of values from which the base reference
%                           for width measurements is computed
%       peakVal             vector of peak values of spots
%   
%
%   Further fields in spots structure are:
%
%   Fields              Description
%   totalCount          total count of spots for each object
%   ratioPolarization   Ratio of accumulated scores of polar spots and
%                       total score of all spots. If multiple weight
%                       vectors are supplied the result of the first one
%                       will be reported here, while the others will be
%                       reported in custom fields that follow the following
%                       convention:
%                       ratioPolarization_(w_mu)_(w_L)_(w_R)_(w_M) in
%                       case of non-integer values the digit (.) will be
%                       replaced by a 'd' to allow for valid variablenames.
%   ratioBipolarity     Imbalance between the two polar spots scores
%                       normalized to 0-1, with 1 being perfectly balanced
%                       and 0 being imbalanced completely to one side. If
%                       multiple weight vectors are supplied the result of
%                       the first one will be reported here, while the
%                       others will be reported in custom fields that
%                       follow the same convention as for the polarization
%                       ratio.
%   label               Original label of the object for which spots where
%                       identified.
%   
%% See Also
% computeSpotResults, findpeaksWrapper

%%
% Copyright (C) David Dreher 2017


narginchk(5,6)
validateattributes(ySmoothed, {'numeric'}, {'2d', 'nonempty'}, mfilename, 'ySmoothed', 1)
validateattributes(yUpsampled, {'numeric'}, {'2d', 'nonempty'}, mfilename, 'yUpsampled', 2)
if ~isequal(size(ySmoothed), size(yUpsampled))
    error([mfilename, ':mismatch_Y-Sizes'], 'The sizes of ySmoothed and yUpsampled must be identical')
end
validateattributes(objectLabels, {'numeric'}, {'vector', 'integer', 'positive'}, ...
    mfilename, 'objectLabels', 3)
validateattributes(PeakOptions, {'struct'}, {'nonempty'}, mfilename, 'PeakOptions structure', 4);
if iscell(polarWeights)
    validateattributes(polarWeights, {'cell'}, {'nonempty'}, mfilename, ...
        'cell array of polarization weights', 5);
    validateWeights = @(x) validateattributes(x, {'numeric'}, {'numel', 4, 'real', ...
        'finite', 'vector'}, mfilename, 'weights in cell');
    cellfun(validateWeights, polarWeights);
else
    validateattributes(polarWeights,  {'numeric'}, {'numel', 4, 'real', ...
        'finite', 'vector'}, mfilename, 'vector of polarWeights', 5);
    polarWeights = {polarWeights(:)};
end

ratioPolName = cell(numel(polarWeights), 1);
ratioBipolName = cell(numel(polarWeights), 1);
for iW = 1:numel(polarWeights)
    weights = polarWeights{iW};
    ratioPolName{iW} = sprintf('ratioPolarization_%g_%g_%g_%g', ...
        weights(1), weights(2), weights(3), weights(4));
    ratioPolName{iW} = strrep(ratioPolName{iW}, '.', 'd');
    ratioPolName{iW} = strrep(ratioPolName{iW}, '+', '');
    
    ratioBipolName{iW} = sprintf('ratioBipolarity_%g_%g_%g_%g', ...
        weights(1), weights(2), weights(3), weights(4));
    ratioBipolName{iW} = strrep(ratioBipolName{iW}, '.', 'd');
    ratioBipolName{iW} = strrep(ratioBipolName{iW}, '+', '');
end

if nargin < 6
    polarIntTypes = 'SUS';
end
validateattributes(polarIntTypes, {'string', 'char'}, {'numel' , 3}, ...
    mfilename, 'polarization intensity type', 6);

%   Padding is now always same size, meaning the whole profile gets duplicated
%   prior to detecting peaks
padding = size(ySmoothed, 2);
polarPoint1 = round(size(ySmoothed, 2)/2);
polarPoint2 = size(ySmoothed, 2);
polarPoint3 = polarPoint1 + polarPoint2;

%   Turn the warning that no peak could be found from the FINDPEAKS function
%   off
warning('off','signal:findpeaks:largeMinPeakHeight')

%   Get number of cells
noCells = size(ySmoothed, 1);

%   Pre-allocate the two result structures
spotFeatures = struct('count', 0, 'sizes', [], 'meanSm', [], 'meanUp', [], ...
    'leftMins', [], 'rightMins', [], 'peakLocs', [], 'intensityValuesSm', [],...
    'intensityValuesUp', [], 'peakBase', [], 'peakVal', []);
spotFeatures.intensityValuesSm = {[]};
spotFeatures.intensityValuesUp = {[]};
spots(noCells, 1) = struct('polarSpots',[], 'sideSpots', [], 'totalCount', [], ...
    'ratioPolarization', [], 'ratioBipolarity', [], 'label', []);


%   Insert feature structure into the result structure
[spots(:).polarSpots] = deal(spotFeatures);
[spots(:).sideSpots] = deal(spotFeatures);

%   FOR each cell
for currCell = 1:1:noCells
    currCellLabel = objectLabels(currCell);
    
    %Get the smoothed and stretched intensity values
    ysm = ySmoothed(currCell, :);
    yup = yUpsampled(currCell, :);
    %   Pad the array to avoid border problems for FINDPEAKS
    ysm = padarray(ysm, [0 padding], 'circular', 'post');
    yup = padarray(yup, [0 padding], 'circular', 'post');
    if isfield(PeakOptions, 'correctBaseline') && PeakOptions.correctBaseline
        yCorr = ysm - min(ysm(:));
    else
        yCorr = ysm;
    end
    
    %   Set corresponding x-vector
    x = 1:1:size(ysm, 2);
    
    if ischar(PeakOptions.function)
        peakFinderFunct = str2func(PeakOptions.function);
    elseif isa(PeakOptions.function, 'function_handle')
        peakFinderFunct = PeakOptions.function;
    else
        error([mfilename, ':PeakOptions:Invalid_function'], ...
            'PeakOptions.function must be either a string or a function handle')
    end
    [ maxVal, maxLoc, maxWidth, maxBase ] = peakFinderFunct(yCorr, ...
        PeakOptions.FinderOptions, PeakOptions.WidthOptions);
    
    %   Get number of maxima
    noPeaksFound = numel(maxLoc);
    
    %   Pre-allocate feature structure. Actual number of spots per region is
    %   unknown, so I over allocate. I allocate for each region vectors the
    %   same length as the total number of spots found.
    [spots(currCell).polarSpots.sizes, ...
        spots(currCell).polarSpots.meanSm, ...
        spots(currCell).polarSpots.meanUp, ...
        spots(currCell).polarSpots.leftMins, ...
        spots(currCell).polarSpots.rightMins, ...
        spots(currCell).polarSpots.peakLocs, ...
        spots(currCell).polarSpots.peakBase, ...
        spots(currCell).polarSpots.peakVal] = deal( nan(1, noPeaksFound));
    
    [spots(currCell).sideSpots.sizes, ...
        spots(currCell).sideSpots.meanSm, ...
        spots(currCell).polarSpots.meanUp, ...
        spots(currCell).sideSpots.leftMins, ...
        spots(currCell).sideSpots.rightMins, ...
        spots(currCell).sideSpots.peakLocs, ...
        spots(currCell).sideSpots.peakBase, ...
        spots(currCell).sideSpots.peakVal] = deal( nan(1, noPeaksFound));
    
    %   FOR each spot
    for peakIdx = 1:1:length(maxLoc)
        currLoc = maxLoc(peakIdx);
        currWidth = maxWidth(peakIdx, :);
        currVal = maxVal(peakIdx);
        currBase = maxBase(peakIdx);
        
        isPolar1 = currWidth(1) <= polarPoint1 && currWidth(2) >= polarPoint1;
        isPolar2 = currWidth(1) <= polarPoint2 && currWidth(2) >= polarPoint2;
        isPolar3 = currWidth(1) <= polarPoint3 && currWidth(2) >= polarPoint3;
        isBetween =polarPoint1 < currLoc && currLoc < polarPoint3;
        
        skip = false;
        if isPolar1 || isPolar2
            ident = 'polarSpots';
        elseif isBetween &&  ~isPolar1 && ~isPolar2 && ~isPolar3
            ident = 'sideSpots';
        else
            skip = true;
        end
        
        if ~skip
            spots(currCell).(ident).count = spots(currCell).(ident).count + 1;
            spotCount = spots(currCell).(ident).count;
            
            spots(currCell).(ident).sizes(spotCount) = currWidth(2) - currWidth(1);
            spots(currCell).(ident).meanSm(spotCount) = mean(ysm(currWidth(1) : currWidth(2)));
            spots(currCell).(ident).meanUp(spotCount) = mean(yup(currWidth(1) : currWidth(2)));
            spots(currCell).(ident).leftMins(spotCount) = mod(currWidth(1)-1, polarPoint2) + 1;
            spots(currCell).(ident).rightMins(spotCount) = mod(currWidth(2)-1, polarPoint2) + 1;
            spots(currCell).(ident).peakLocs(spotCount) = mod(currLoc-1, polarPoint2) +1;
            spots(currCell).(ident).intensityValuesSm{spotCount} =  ysm(currWidth(1) : currWidth(2));
            spots(currCell).(ident).intensityValuesUp{spotCount} =  yup(currWidth(1) : currWidth(2));
            spots(currCell).(ident).peakBase(spotCount) = currBase;
            spots(currCell).(ident).peakVal(spotCount) = currVal;
            
            if spots(currCell).(ident).leftMins(spotCount) > spots(currCell).(ident).rightMins(spotCount)
                spots(currCell).(ident).leftMins(spotCount) = spots(currCell).(ident).leftMins(spotCount) - polarPoint2;
            end
        end

    end
    
    spots(currCell) = cleanNans(spots(currCell));
    spots(currCell).totalCount = spots(currCell).polarSpots.count + spots(currCell).sideSpots.count;
    spots(currCell).label = currCellLabel;
    
    if spots(currCell).polarSpots.count == 0
        spots(currCell).ratioPolarization = 0;
        spots(currCell).ratioBipolarity = nan;
        for iW = 1:numel(polarWeights)
            spots(currCell).(ratioPolName{iW}) = 0;
            spots(currCell).(ratioBipolName{iW}) = nan;
        end
    else
        %   The ratio of polarization is defined as the summed ratio of
        %   intensity of polar spots compared to intensities of side spots
        %   and polar spots. Large and intense polar spots will accumulate
        %   most of the total intensities, while strong and/or large side
        %   spots will accumulate more intensities and hence reduce this
        %   ratio towards 0.
        if strcmpi(polarIntTypes(1), 'S')
            intPmean = spots(currCell).polarSpots.intensityValuesSm;
            intSmean = spots(currCell).sideSpots.intensityValuesSm;
        elseif strcmpi(polarIntTypes(1), 'U')
            intPmean = spots(currCell).polarSpots.intensityValuesUp;
            intSmean = spots(currCell).sideSpots.intensityValuesUp;
        else
            error([mfilename ':polIntTypeCharUnknown'], 'The char for determing polarization intensity type is unknown');
        end
        if strcmpi(polarIntTypes(2), 'S')
            intPsmoothness = spots(currCell).polarSpots.intensityValuesSm;
            intSsmoothness = spots(currCell).sideSpots.intensityValuesSm;
        elseif strcmpi(polarIntTypes(2), 'U')
            intPsmoothness = spots(currCell).polarSpots.intensityValuesUp;
            intSsmoothness = spots(currCell).sideSpots.intensityValuesUp;
        else
            error([mfilename ':polIntTypeCharUnknown'], 'The char for determing polarization intensity type is unknown');
        end
        if strcmpi(polarIntTypes(3), 'S')
            intPmax = spots(currCell).polarSpots.intensityValuesSm;
            intSmax = spots(currCell).sideSpots.intensityValuesSm;
        elseif strcmpi(polarIntTypes(2), 'U')
            intPmax = spots(currCell).polarSpots.intensityValuesUp;
            intSmax = spots(currCell).sideSpots.intensityValuesUp;
        else
            error([mfilename ':polIntTypeCharUnknown'], 'The char for determing polarization intensity type is unknown');
        end
        
      
        for iW = 1:numel(polarWeights)
            weights = polarWeights{iW};
            
            scorePolar = cellfun(@(x,y,z) (mean(x) ^ weights(1)) * ...
                        (numel(x) ^ weights(2)) / ...
                        (std(diff(y)) ^ weights(3)) * ...
                        (max(z) ^ weights(4)), ...
                        intPmean, intPsmoothness, intPmax);
                    
            %   In case of no side spots intensity values the corresponding will be set to zero.
            if spots(currCell).sideSpots.count == 0
                scoreSide = 0;
            else
                scoreSide = cellfun(@(x,y,z) (mean(x) ^ weights(1)) * ...
                    (numel(x) ^ weights(2)) / ...
                    (std(diff(y)) ^ weights(3)) * ...
                    (max(z) ^ weights(4)), ...
                    intSmean, intSsmoothness, intSmax);
            end
            ratioPol = sum(scorePolar) / (sum(scorePolar) + sum(scoreSide));
            spots(currCell).(ratioPolName{iW}) = ratioPol;
            
            if spots(currCell).polarSpots.count == 1
                ratioBipol = 0;
            elseif spots(currCell).polarSpots.count == 2
                %   Bipolarity is measured as the summed ratio of intensity
                %   values of both polar spots. When bot spots are equal size and
                %   intensity this ratio equals to 0.5, if one spots get weaker
                %   and or smaller this ratio approaches 0 or 1 depending which
                %   spots is changing. The ratio of bipolarity is the distance
                %   from 0.5 resampled to the range 0-1.
                ratioBipol = scorePolar(1) / (scorePolar(1) + scorePolar(2));
                ratioBipol = 1-abs(ratioBipol - 0.5)*2;
            else
                ratioBipol = nan;
            end
            spots(currCell).(ratioBipolName{iW}) = ratioBipol;
            if iW == 1
                spots(currCell).ratioPolarization = ratioPol;
                spots(currCell).ratioBipolarity = ratioBipol;
            end
                
        end

    end
end

%   Turn the warning back on
warning('on','signal:findpeaks:largeMinPeakHeight')
end


%%   Subfunctions

%   For cleaning out the pre allocated vector that still have Nans
function cleanedStruct = cleanNans(structWithNaNs)

cleanedStruct = structWithNaNs;
identSpots = fieldnames(structWithNaNs);
isSpotFeature = regexpi(identSpots, '\w*Spots');
for i=1:length(identSpots)
    if ~isempty(isSpotFeature{i})
        identFeatures = fieldnames(structWithNaNs.(identSpots{i}));
        
        if structWithNaNs.(identSpots{i}).count == 0
            cleanedStruct.(identSpots{i}).sizes = 0;
            cleanedStruct.(identSpots{i}).meanSm = 0;
            cleanedStruct.(identSpots{i}).meanUp = 0;
            cleanedStruct.(identSpots{i}).leftMins = [];
            cleanedStruct.(identSpots{i}).rightMins = [];
            cleanedStruct.(identSpots{i}).peakLocs = [];
            cleanedStruct.(identSpots{i}).intensityValuesSm = {[]};
            cleanedStruct.(identSpots{i}).intensityValuesUp = {[]};
            cleanedStruct.(identSpots{i}).peakBase = [];
            cleanedStruct.(identSpots{i}).peakVal = [];
            continue
        end
        
        for k = 1:length(identFeatures)
            currentValues = structWithNaNs.(identSpots{i}).(identFeatures{k});
            if ~iscell(currentValues)
                cleanedStruct.(identSpots{i}).(identFeatures{k}) = currentValues(~isnan(currentValues));
            end
        end
    end
end

end