function [outputSpots, outputObjects] = manageOutput(intensRes, spotRes, ...
    searchExpr, tokenKeys, outFilepath,  thresholdPolar, varargin) % tokenKeyNumbering, addOutput, addOutputOptions)
%manageOutput restructures the output of computeSpotResults by grouping
%similar conditions as identified by filenames and creating a more human
%readable format with the additional option of creating Excel sheets.
%
%% Syntax
%   [outputSpots, outputObjects] = manageOutput(intensRes, spotRes, searchExpr, tokenKeys, outFilepath, thresholdPolar)
%   [outputSpots, outputObjects] = manageOutput(___, tokenKeyNumbering)
%   [outputSpots, outputObjects] = manageOutput(___, tokenKeyNumbering, addOutput)
%   [outputSpots, outputObjects] = manageOutput(___, tokenKeyNumbering, addOutput, addOutputOptions)
%   [outputSpots, outputObjects] = manageOutput(___, addOutput)
%   [outputSpots, outputObjects] = manageOutput(___, addOutput, addOutputOptions)
%
%% Description
%   [outputSpots, outputObjects] = manageOutput(intensRes, spotRes,
%   searchExpr, tokenKeys, outFilepath, thresholdPolar,tokenKeyNumbering, 
%   addOutput, addOutputOptions) restructures the results from
%   computeBorderIntensities and computeSpotResults into two large cell
%   arrays outputSpots and outputObjects with the results for individual
%   spots and total cells respectively. The first row of the cell array
%   holds table like headers to identify the data. Metadata is extracted
%   based on tokens of a regular expression search in the filenames and
%   same metadata conditions are grouped together. 
%   Additionally Excel sheets or CSV files can be created from the output.
%
%% Example 
%   Load previously computed results from computeSpotResults and computeBorderIntensities
%   
%       testData = load('testData/testData.mat');
%       intRes = testData.intensityResults;
%       spotRes = testData.spotResults;
%   
%   Create regular expression to match metadata encoded in filenames
%
%       searchExpr = 'Test_\d{8}_(\d)_(\d+)_(.*)_(\d\d)_.*.tif';
%       tokenKeys = {'Timepoint', 'Strain No', 'Treatment', 'Temperature'};
%   
%   Setup tokenKeyNumbering for the third metadata 'Treatment'. Table
%   contents will be numbers for matches into tokenKeyNumbering instead of
%   strings encoded in filenames:
%
%       tokenKeyNumbering = {{}, {}, {'nodrug', 'DMSO', 'BFA'}, {}};
%
%   Set other parameters and create output cell arrays and plot an example
%   output (histogram of polarization ratios).
%
%       outFilepath = 'testResults/testOutput';
%       thrshPol = 0.75;
%       [outputSpots, outputObjects] = manageOutput(intRes, spotRes, ...
%           searchExpr, tokenKeys, outFilepath, thrshPol, tokenKeyNumbering)
%       histogram([outputObjects{2:end, strcmpi(outputObjects(1,:), 'Polarization ratio')}])
%   
%% Input
%   intensRes -- intensity result structure
%       struct
%       Nx1 result structure from computeBorderIntensities. It holds the
%       intensity results around the border as well as all measurements for
%       the objects for all N-files in the data set.
%
%   spotRes -- spot detection result structure
%       struct
%       Nx1 result structure from computeSpotResults. It holds the spot
%       measurements and their classification and spot based measurements
%       like polarization ratios for all N-files in the data set.
%
%   searchExpr -- regular expression for filenames metadata
%       character vector
%       Regular expression identifying metadata from the signal filenames.
%       The regular expression must generate a proper match for all
%       filenames, while the tokens are extracted as metadata. All kinds of
%       metadata is supported. The function will try to convert numerical
%       metadata to doubles and otherwise resort to string representation
%
%   tokenKeys -- Header strings for filename metadata
%       cell array of character vectors
%       Cell array with description for the type of metadata for the found
%       tokens. These are used for populating the header row of the cell
%       arrays.
%   
%   outFilepath -- filepath to saving location with base name
%       character vector
%       Filepath to the disk location where results should be stored, given
%       as path with a basic filename. 
%
%   thresholdPolar -- Threshold for polar classification
%       scalar in range 0-1
%       Scalar threshold for classification of whole objects as polar or
%       non-polar. Objects with a polarization ratio below will get a false
%       flag in their polar classification column.
%
%   tokenKeyNumbering -- Cell with comparision strings for numbering
%       cell array of character vectors
%       Cell array with strings for comparision and numbering of tokens. In
%       case the metadata in filenames is non-numeric but a numeric
%       representation is requested the comparisions strings can be
%       supplied and the corresponding metadata holds the index into the
%       tokenKeyNumbering cell array.
%
%   addOutput -- String input for desired additional output
%       char vector | string scalar
%       'none' (default) | 'csv' | 'xls' | 'xls_all' | 'xls_split' |
%       'xls_split_sheets'
%       String indicating the desired output additional to the '.mat'-file.
%       'none': no additional output files.
%       'csv': CSV files with whole table entries.
%       'xls': one large spreadsheet, same as 'xls_all'.
%       'xls_split': multiple spreadsheets for each condition.
%       'xls_sheets_split: one excel file where one sheet corresponds to
%       one condition.
%
%   addOutputOptions -- Cell with additional options for the chosen output
%       cell array
%       Cell array with any additional options for the output function
%       generating the chosen type of additional output. See writetable
%       documentation for possible options.
%
%% Output
%   outputSpots is a table like cell array with a row for each spot holding
%   all singular spot measurements as well as associated object
%   measurements. It containts the following columns:
%
%   Column                              Description
%   Condition/Combined metadata         Combined tokens separated by an underscore
%   Token metadata columns              Entries from the regular expression
%                                         matching
%   Object No                           Progressing counter of objects
%   Object label                        ID label of corresponding object in
%                                         original image
%   Spot No                             Progressing counter of spots
%   Position                            Scalar identifying the location of spot
%                                         (1=Polar, 2= Side)
%   Size of spot                        Size of spot in normalized coordinates 
%   Mean intensity of spot-smoothed     Mean of smoothed intensity vectors
%   Mean intensity of spot-unsmoothed   Mean of unsmoothed intensity vectors
%   Mean intensity of object            Mean of total object intensities
%   Mean intensity along the membrane   Mean intensity of whole smoothed 
%     of object-smoothed                  membrane ring intensity values
%   Mean intensity along the membrane   Mean intensity of whole unsmoothed
%     of object-unsmoothed                membrane ring intensity values
%   Major Axis length                   Distance of intersection points of
%                                         major axis with membrane ring
%   Minor Axis length                   Distance of intersection points of
%                                         minor axis with membrane ring
%   Eccentricity                        Eccentricity measurement as
%                                         reported by regionstats
%   Area                                Area of object in pixels
%   Deathscore                          Dead cells are very bright over the
%                                         whole object in comparision to
%                                         their alive counterparts. The
%                                         deathscore is the median of the
%                                         z-values of the object pixels in
%                                         regard to the total image pixel
%                                         value distribution. A large value
%                                         indicates a overall brighter
%                                         object and hence probably a dead
%                                         cell.
%   Polarization ratio                  Ratio of accumulated scores of
%                                         polar spots and total score of
%                                         all spots.
%   Polarized                           Bool to indicate whether a cell has
%                                         been flagged as polarized
%   Bipolarity ratio                    Imbalance between the two polar
%                                         spots scores normalized to 0-1,
%                                         with 1 being perfectly balanced
%                                         and 0 being imbalanced completely
%                                         to one side.
%   Number of perimeter pixels          Number of pixels that constitute
%                                         the boundary
%   Euclidean length of perimeter       Euclidean length of membrane ring
%
%   If multiple weight vectors are supplied they will be reported in
%   additional columns with headers following the convention:
%   ratioPolarization_(w_mu)_(w_L)_(w_R)_(w_M)
%   bipolarityRatio_(w_mu)_(w_L)_(w_R)_(w_M)
%
%
%   outputObjects is a table like cell array with a row for each object
%   holding all object measurements. It containts the following columns:
%
%   Column                              Description
%   Condition/Combined metadata         Combined tokens separated by an underscore
%   Token metadata columns              Entries from the regular expression
%                                         matching
%   Object No                           Progressing counter of objects
%   Object label                        ID label of corresponding object in
%                                         original image
%   No of spots                         Total number of spots on object
%                                         membrane
%   Polarization ratio                  Ratio of accumulated scores of
%                                         polar spots and total score of
%                                         all spots.
%   Polarized                           Bool to indicate whether a cell has
%                                         been flagged as polarized
%   Bipolarity ratio                    Imbalance between the two polar
%                                         spots scores normalized to 0-1,
%                                         with 1 being perfectly balanced
%                                         and 0 being imbalanced completely
%                                         to one side.
%   Major Axis length                   Distance of intersection points of
%                                         major axis with membrane ring
%   Minor Axis length                   Distance of intersection points of
%                                         minor axis with membrane ring
%   Mean intensity of object            Mean of total object intensities
%   Mean intensity along the membrane   Mean intensity of whole smoothed 
%     of object-smoothed:                 membrane ring intensity values
%   Mean intensity along the membrane   Mean intensity of whole unsmoothed
%     of object-unsmoothed:               membrane ring intensity values
%   Eccentricity                        Eccentricity measurement as
%                                         reported by regionstats
%   Area                                Area of object in pixels
%   Deathscore                          Dead cells are very bright over the
%                                         whole object in comparision to
%                                         their alive counterparts. The
%                                         deathscore is the median of the
%                                         z-values of the object pixels in
%                                         regard to the total image pixel
%                                         value distribution. A large value
%                                         indicates a overall brighter
%                                         object and hence probably a dead
%                                         cell.
%   Number of perimeter pixels          Number of pixels that constitute
%                                         the boundary
%   Euclidean length of perimeter       Euclidean length of membrane ring
%
%   If multiple weight vectors are supplied they will be reported in
%   additional columns with headers following the convention:
%   ratioPolarization_(w_mu)_(w_L)_(w_R)_(w_M)
%   bipolarityRatio_(w_mu)_(w_L)_(w_R)_(w_M)
%
%% See Also
%   computeBorderIntensities, computeSpotResults

%%
% Copyright (C) David Dreher 2017


imgResizeScale = 1.5;
displayProgress('init', 'Starting creation of output tables');

%% Input checking
narginchk(6,9)
validateattributes(intensRes, {'struct'}, {'nonempty'}, mfilename, ...
    'Intensity result structure', 1);
validateattributes(spotRes, {'struct'}, {'nonempty'}, mfilename, ...
    'Spot result structure', 2);
validateattributes(searchExpr, {'string', 'char'}, {'nonempty'}, ...
    mfilename,'Regular search expression', 3);
validateattributes(tokenKeys, {'cell'}, {'nonempty'}, mfilename,...
    'Strings identifying the tokens', 4);
validateattributes(outFilepath, {'string', 'char'}, {'nonempty'}, mfilename,'Output filepath', 5);
validateattributes(thresholdPolar, {'numeric'}, {'scalar', '>=', 0, '<=', 1}, ...
    mfilename, 'threshold for classification as polar cell', 6);
[tokenKeyNumbering, addOutExt, addOutSplits, addOutputOptions] = parseOptInputs(tokenKeys, varargin{:});

%% Main

% Get the file names from result structure.
filenames = {intensRes.signalImageName}';
nSpots = sum([spotRes.totalCount]);
nObjs = numel( vertcat( spotRes.objectLabels ) );

[resultFolder, resultFilename] = fileparts(outFilepath);
imgFolder = fullfile(resultFolder, 'resultImages');
if ~isdir(imgFolder)
    mkdir(imgFolder);
end

% Match the regular expression and sort according to their common metadata
tokens = regexp(filenames, searchExpr, 'tokens');
emptyTokens = cellfun('isempty', tokens);
if any(emptyTokens)
    errMsg = ['No tokens could be found for files: ', ...
        num2str(find(emptyTokens(:)'), '%d, '), ...
        '\b. Please verify your search expression'];
    error('manageOutput:searchExpression', errMsg)
end
conditionNames = cellfun(@(x) strjoin(x{:}, '_'), tokens, 'UniformOutput', false);
[conditionNames, sortCondIdx] = sort(conditionNames);
intensRes = intensRes(sortCondIdx);
spotRes = spotRes(sortCondIdx);
tokens = tokens(sortCondIdx);

% Set the two different counters.
objectCounter = 1; % Starting anew for each condition identifying the found objects
outputCounterSpots = 2; % The current row of the output cell.
outputCounterObj = 2;

% Identify the tokens
nTokens = numel(tokenKeys);
if numel(tokens{1}{1}) ~= nTokens
    error('manageOutput:TokenMismatch', ...
    'Found number of token number and number of token keys are not corresponding');
end

tokenHeader = cell(nTokens, 1);
for iTok = 1:nTokens
    if isempty(tokenKeyNumbering{iTok})
        tokenHeader{iTok} = tokenKeys{iTok};
    else
        nNumb = numel(tokenKeyNumbering{iTok});
        tmp = cell(3, nNumb);
        tmp(1,:) = cellfun(@(x) sprintf('%d=', x), num2cell(1:nNumb), 'UniformOutput', false);
        tmp(2,:) = tokenKeyNumbering{iTok};
        tmp(3,:) = {', '};
        tokenHeader{iTok} = [tokenKeys{iTok}, ' (' tmp{1:end-1}, ')'];
    end
end

% Find additional polarization ratios
spotStructFields = fieldnames(spotRes(1).spots);
searchStrPolarRat = 'ratioPolarization_\d+.*_\d+.*_\d+.*';
isAddPolarRat = ~cellfun('isempty', regexp(spotStructFields, searchStrPolarRat));
if sum(isAddPolarRat) > 1
    strPolarRat = spotStructFields(isAddPolarRat);
else
    strPolarRat = [];
end

searchStrBipolRat = 'ratioBipolarity_\d+.*_\d+.*_\d+.*';
isAddBipolRat = ~cellfun('isempty', regexp(spotStructFields, searchStrBipolRat));
if sum(isAddBipolRat) > 1
    strBipolRat = spotStructFields(isAddBipolRat);
else
    strBipolRat = [];
end

% Set the first row / header of the cell array
defaultMetadataSpots = {'Object No', 'Object label', 'Spot No', ...
    'Position (1=Polar, 2= Side)', 'Size of spot', 'Mean intensity of spot-smoothed', ...
    'Mean intensity of spot-unsmoothed', 'Mean intensity of object', ...
    'Mean intensity along the membrane of object-smoothed', ...
    'Mean intensity along the membrane of object-unsmoothed', ...
    'Major Axis length', 'Minor Axis length', 'Eccentricity', 'Area', ...
    'Deathscore', 'Polarization ratio', 'Polarized', 'Bipolarity ratio', ...
    'Number of perimeter pixels', 'Euclidean length of perimeter'};
nDefOutSpots = numel(defaultMetadataSpots) + 1;
nColsSpots = nDefOutSpots + numel(strPolarRat) + numel(strBipolRat) + nTokens;
outputSpots = cell(nSpots +1, nColsSpots);
outputSpots{1,1} = 'Condition/Combined metadata';
outputSpots(1,2:nTokens+1) = tokenHeader;
outputSpots(1,nTokens+2:nTokens+nDefOutSpots) = defaultMetadataSpots;
for i = 1:numel(strPolarRat)
    outputSpots{1, nDefOutSpots + nTokens + i} = strPolarRat{i};
end
for i = 1:numel(strBipolRat)
    outputSpots{1, nDefOutSpots + nTokens + numel(strPolarRat) + i} = strBipolRat{i};
end


defaultMetadataObjs = {'Object No', 'Object label', 'No of spots', 'Polarization ratio'...
    'Polarized', 'Bipolarity ratio', 'Major Axis length', 'Minor Axis length', ...
    'Mean intensity of object', 'Mean intensity along the membrane of object-smoothed', ...
    'Mean intensity along the membrane of object-unsmoothed', 'Eccentricity', ...
    'Area', 'Deathscore', 'Number of perimeter pixels', 'Euclidean length of perimeter'};

nDefOutObjs = numel(defaultMetadataObjs) +1;
nColsObjs = nDefOutObjs + numel(strPolarRat) + numel(strBipolRat) + nTokens;
outputObjects = cell(nObjs +1, nColsObjs);
outputObjects{1,1} = 'Condition/Combined metadata';
outputObjects(1,2:nTokens+1) = tokenHeader;
outputObjects(1,nTokens+2:nTokens+nDefOutObjs) = defaultMetadataObjs;
for i = 1:numel(strPolarRat)
    outputObjects{1, nDefOutObjs + nTokens + i} = strPolarRat{i};
end
for i = 1:numel(strBipolRat)
    outputObjects{1, nDefOutObjs + nTokens + numel(strPolarRat) + i} = strBipolRat{i};
end

% Counter for the excel files. Since for each uniqe Condition a new excel
% file should be saved, since one excel file would be too large.
xlStartSpots = 2;
xlStartObj = 2;



% Get the total number of images.
nImgs = numel(intensRes);

% FOR each image
for iImg = 1:nImgs
    
    
    % Read the current image
    currImg = double(imread(intensRes(iImg).signalImageName));
    resultImg = imresize(im2uint8(imadjust3(mat2gray(currImg), 0.001)), imgResizeScale);
    allPxls = vertcat( intensRes(iImg).ConnectedComponents.PixelIdxList{:});
    meanImg = mean(currImg(allPxls));
    stdImg = std(currImg(allPxls));
    
    % Get the current condition string. ( the match of the regular
    % expression)
    currCondition = conditionNames{iImg};
    
    % Get the regular expression tokens 
    currTokens = tokens{iImg};
    currTokens = currTokens{1};
    
    % Get and convert the different metadata
    currImgMetadata = cell(nTokens, 1);
    for iTok = 1:nTokens
        currImgMetadata{iTok} = str2double( currTokens{1,iTok});
        if isnan(currImgMetadata{iTok})
            if isempty(tokenKeyNumbering{iTok})
                currImgMetadata{iTok} = currTokens{1, iTok};
            else
                currImgMetadata{iTok} = find(strcmp(currTokens{1,iTok}, tokenKeyNumbering{iTok}), 1);
            end
        end
    end
    
    % IF we have a new condition, then restart the object counter and set
    % the start of the excel table to the current cell row.
    if iImg>1 && ~strcmp(conditionNames{iImg}, conditionNames{iImg -1})
        objectCounter = 1;
        xlStartSpots = outputCounterSpots;
        xlStartObj = outputCounterObj;
    end
    
    % FOR each found object (for which a border intensity could been found)
    for iObj = 1: numel(spotRes(iImg).objectLabels)
        
        % Get the objects label ID.
        currObjLabel = spotRes(iImg).objectLabels(iObj);
        
        % With the label ID get the MeanIntensity and Axis lengths from the
        % result structure.
        meanIntObj = intensRes(iImg).RegionStats(currObjLabel).MeanIntensity;        
        lengthObj = intensRes(iImg).RegionStats(currObjLabel).MajorAxisLength;
        widthObj = intensRes(iImg).RegionStats(currObjLabel).MinorAxisLength;
        eccObj = intensRes(iImg).RegionStats(currObjLabel).Eccentricity;
        areaObj = intensRes(iImg).RegionStats(currObjLabel).Area;
        pixlsObj = intensRes(iImg).ConnectedComponents.PixelIdxList{currObjLabel};
        centerObj = intensRes(iImg).RegionStats(currObjLabel).Centroid;
        noPerimPxlsObj = intensRes(iImg).RegionStats(currObjLabel).boundaryNpixels;
        lengthPerimObj = intensRes(iImg).RegionStats(currObjLabel).boundaryLength;
        
        intValuesObj = currImg(pixlsObj);
        zScores = (intValuesObj - meanImg)./stdImg;
        deathScore = median(zScores);
        
        % Compute the mean intensity of the objects border
        meanBorderSm = mean( spotRes(iImg).ySmoothed(iObj,:) );
        meanBorderUp = mean( spotRes(iImg).yUpsampled(iObj,:) );

        % Get the structure holding the classified spots.
        spotStruct = spotRes(iImg).spots(iObj);
        % Reset the spot_counter to 1
        spotCounter = 1;
        
        % Get the field names.
        spotFields = fieldnames(spotStruct);
        isSpotFeature = regexpi(spotFields, '\w*Spots');
        
        outputObjects{outputCounterObj, 1} = currCondition;
        outputObjects(outputCounterObj, 2:nTokens+1) = currImgMetadata;
        spotCount = spotStruct.totalCount;
        polRatio = spotStruct.ratioPolarization;
        isPolar = spotStruct.ratioPolarization >= thresholdPolar;
        if isPolar
            bipolRatio = spotStruct.ratioBipolarity;
        else
            bipolRatio = nan;
        end
        currObjMetadata = {objectCounter, currObjLabel, spotCount, ...
            polRatio, isPolar, bipolRatio, lengthObj, widthObj, meanIntObj, ...
            meanBorderSm, meanBorderUp, eccObj, areaObj, deathScore, ...
            noPerimPxlsObj, lengthPerimObj};
        outputObjects(outputCounterObj, nTokens+2:nTokens+nDefOutObjs) = currObjMetadata;
        
        for i = 1:numel(strPolarRat)
            outputObjects{outputCounterObj, nDefOutObjs + nTokens + i} = spotStruct.(strPolarRat{i});
        end
        for i = 1:numel(strBipolRat)
            outputObjects{outputCounterObj, nDefOutObjs + nTokens + numel(strPolarRat) + i} = spotStruct.(strBipolRat{i});
        end
        
        outputCounterObj = outputCounterObj + 1;
        
        resultImg = insertText(resultImg, centerObj .*imgResizeScale, ...
            sprintf('L:%d\nrP:%0.1f\nrB:%0.1f', currObjLabel, ...
            polRatio, bipolRatio), ...
            'FontSize', 12, 'BoxOpacity', 0, 'AnchorPoint', 'Center', ...
            'TextColor', 'cyan'); 
        
        
        
        % If all fields are empty, i.e. no spots detected, the current row
        % will be filled with zeros for the spot measurements but still
        % report the object measurements.
        % This is important for proper statistics of how many cells do
        % actually have spots and the object measurements are valuable for
        % example to identify dead cells ( cells with very strong overall
        % signal)
        if spotStruct.totalCount == 0
            outputSpots{outputCounterSpots,1} = currCondition;
            outputSpots(outputCounterSpots ,2:nTokens+1) = currImgMetadata;
            spotNo = nan;
            spotPos = nan;
            spotSize = nan;
            spotMeanIntSm = nan;
            spotMeanIntUp = nan;
            
            currSpotMetadata = {objectCounter, currObjLabel, spotNo, spotPos, ...
                spotSize, spotMeanIntSm, spotMeanIntUp, meanIntObj, ...
                meanBorderSm, meanBorderUp, lengthObj, widthObj, eccObj, ...
                areaObj, deathScore, polRatio, isPolar, bipolRatio, ...
                noPerimPxlsObj, lengthPerimObj};
            outputSpots(outputCounterSpots, nTokens+2:nTokens+nDefOutSpots) = currSpotMetadata;
            
            for i = 1:numel(strPolarRat)
                outputSpots{outputCounterSpots, nDefOutSpots + nTokens + i} = spotStruct.(strPolarRat{i});
            end
            for i = 1:numel(strBipolRat)
                outputSpots{outputCounterSpots, nDefOutSpots + nTokens + numel(strPolarRat) + i} = spotStruct.(strBipolRat{i});
            end
            
            % Increase row count
            outputCounterSpots = outputCounterSpots + 1;
            
        %IF any of fields is not empty, i.e. if spots were found           
        elseif spotStruct.totalCount > 0
            
            % Iterate over each field
            for iField = 1: length(spotFields)
                % If the current field is not empty
                if ~isempty(isSpotFeature{iField})
                    %Iterate over all spots found in the current location
                    %(field name)
                    for iSpot = 1:spotStruct.(spotFields{iField}).count
                        outputSpots{outputCounterSpots,1} = currCondition;
                        outputSpots(outputCounterSpots ,2:nTokens+1) = currImgMetadata;
                        % Get the spot measurements
                        spotNo = spotCounter;
                        spotPos = iField;
                        spotSize = spotStruct.(spotFields{iField}).sizes(iSpot);
                        spotMeanIntSm = spotStruct.(spotFields{iField}).meanSm(iSpot);
                        spotMeanIntUp = spotStruct.(spotFields{iField}).meanUp(iSpot);
                        
                        % And transfer them into the table
                        currSpotMetadata = {objectCounter, currObjLabel, spotNo, spotPos, ...
                            spotSize, spotMeanIntSm, spotMeanIntUp, meanIntObj, ...
                            meanBorderSm, meanBorderUp, lengthObj, widthObj, eccObj, ...
                            areaObj, deathScore, polRatio, isPolar, bipolRatio, ...
                            noPerimPxlsObj, lengthPerimObj};
                        outputSpots(outputCounterSpots, nTokens+2:nTokens+nDefOutSpots) = currSpotMetadata;
                        
                        for i = 1:numel(strPolarRat)
                            outputSpots{outputCounterSpots, nDefOutSpots + nTokens + i} = spotStruct.(strPolarRat{i});
                        end
                        for i = 1:numel(strBipolRat)
                            outputSpots{outputCounterSpots, nDefOutSpots + nTokens + numel(strPolarRat) + i} = spotStruct.(strBipolRat{i});
                        end
                        % Increase both spot count and row count
                        spotCounter = spotCounter + 1;
                        outputCounterSpots = outputCounterSpots + 1;
                        
                    end
                end
                
            end
        else
            error([mfilename, ':spotCountUnknown'], 'Unknown spot count')
        end
        % Increase object ID counter
        objectCounter = objectCounter + 1;
        
    end
    
    % IF it is the last image OR the next condition is a different one AND
    % split excel files are requested THEN
    condChange = (iImg==nImgs || ~strcmp(conditionNames{iImg}, conditionNames{iImg +1}));
    splitOutput = any(strcmp(addOutSplits, {'sheets_split', 'file_split'}));
    if  condChange && splitOutput
        disp(['Generating split ', addOutExt, ' files for condition:' currCondition]);
        % Set the end of the excel table to the previous row
        xlEndSpots = outputCounterSpots - 1;
        xlEndObj = outputCounterObj - 1;
        if strcmp(addOutSplits, 'file_split')
            % Generate the file name for the current condition
            xlSpotFilename = [resultFilename, '_', currCondition, '.', addOutExt];
            xlObjFilename = [resultFilename, '_', currCondition, '.', addOutExt];
            xlSpotFolder = fullfile(resultFolder, 'excel_Spots');
            xlObjFolder = fullfile(resultFolder, 'excel_Objects');
            if ~isdir(xlSpotFolder)
                mkdir(xlSpotFolder);
            end
            if ~isdir(xlObjFolder)
                mkdir(xlObjFolder);
            end
        elseif strcmp(addOutSplits , 'sheets_split')
            xlSpotFilename = [resultFilename, '_spots.', addOutExt];
            xlObjFilename = [resultFilename, '_objects.', addOutExt];
            xlSpotFolder = resultFolder;
            xlObjFolder = resultFolder;
            addOutputOptions = horzcat(addOutputOptions, {'Sheet', currCondition}); %#ok<AGROW>
        else
            error([mfilename, ':unknownSplitOption'], 'An unknown split option was passed')            
        end
        % Save the excel file (always with the first row with the header
        % strings)
        splitSpotTable = cell2table(vertcat( outputSpots(1,:), outputSpots(xlStartSpots:xlEndSpots, :)));
        splitObjTable = cell2table(vertcat( outputObjects(1,:), outputObjects(xlStartObj:xlEndObj, :)));
        writetable(splitSpotTable, fullfile(xlSpotFolder, xlSpotFilename), addOutputOptions{:}); 
        writetable(splitObjTable, fullfile(xlObjFolder, xlObjFilename), addOutputOptions{:});
        disp(['Finished writing split ', addOutExt, ' files for condition:' currCondition]);
    end
    
    [~, fname] = fileparts(intensRes(iImg).signalImageName);
    imgFilename = [fname '_ratios.tif'];
    imwrite(resultImg, fullfile(imgFolder, imgFilename), 'Compression', 'none');
    
    displayProgress(iImg/nImgs);
end

displayProgress('end', 'Finished creation of output tables, saving tables ...')
% Save the output mat file
matFilename = [resultFilename, '.mat'];
save(fullfile(resultFolder, matFilename), 'outputSpots', 'outputObjects', 'thresholdPolar', '-v7.3');
disp(['Output tables saved to: ', fullfile(resultFolder, matFilename)]);

if strcmp(addOutSplits, 'no_split')
    disp(['Generating ', addOutExt, ' files for all conditions']);
    spotTable = cell2table(outputSpots);
    objectTable = cell2table(outputObjects);
    spotFilename = fullfile(resultFolder, [resultFilename, '_spots.', addOutExt]);
    objFilename = fullfile(resultFolder, [resultFilename, '_objects.', addOutExt]);
    writetable(spotTable, spotFilename, addOutputOptions{:});
    writetable(objectTable, objFilename, addOutputOptions{:});
    disp(['Finished writing ', addOutExt, ' files for all conditions']);
end

end


function [tokenMap, addOutExt, addOutSplits, outputOpts] = parseOptInputs(tokenKeys, varargin)

allowedAddOpts = {'none', 'csv', 'xls', 'xls_all', 'xls_split', 'xls_split_sheets'};

tokenMap = cell(numel(tokenKeys), 1);
addOutExt = '';
addOutSplits = '';
outputOpts = cell.empty;
addOutput = 'none';

nOptInp = numel(varargin);
if nOptInp == 0
    return;
end
if iscell(varargin{1})
    % manageOutput(___, tokenKeyNumbering)
    tokenMap = varargin{1};
    validateattributes(tokenMap, {'cell'}, {'numel', numel(tokenKeys)}, ...
        mfilename, 'Token key map for numbering', 7);
    if nOptInp >= 2
        % manageOutput(___, tokenKeyNumbering, addOutput)
        addOutput = validatestring(varargin{2}, allowedAddOpts, mfilename, ...
            'String identifier for additional output', 8);
    end
    if nOptInp >= 3
        % manageOutput(___, tokenKeyNumbering, addOutput, addOutputOptions)
        outputOpts = varargin{3};
        validateattributes(outputOpts, {'cell'}, {}, mfilename, 'Additional options for outputs', 9);
    end
else
    % manageOutput(___, addOutput)
    addOutput = validatestring(varargin{1}, allowedAddOpts, mfilename, ...
            'String identifier for additional output', 7);
    if nOptInp >= 2
        % manageOutput(___, addOutput, addOutputOptions)
        outputOpts = varargin{2};
        validateattributes(outputOpts, {'cell'}, {}, mfilename, 'Additional options for outputs', 8);
    end
end

addOutTypes = split(addOutput, '_');
switch addOutTypes{1}
    case 'none'
        addOutExt = '';
        addOutSplits = '';
    case 'csv'
        addOutExt = 'csv';
        addOutSplits = 'no_split';
    case 'xls'
        addOutExt = 'xlsx';
        addOutSplits = 'no_split';
        if any(strcmp(addOutTypes, 'split'))
            if any(strcmp(addOutTypes, 'sheets'))
                addOutSplits = 'sheets_split';
            else
                addOutSplits = 'file_split';
            end
        end
end

% We always need to ignore the variable names when writing additional
% output.
outputOpts = horzcat( outputOpts(:).', {'WriteVariableNames', false});

end


