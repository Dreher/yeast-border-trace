function [ySmooth, yUpsampled, objectLabels, scaling] = ...
    smoothStretchIntensities( intensResult, FilterOptions, intensType, ...
        plotFlag, outFigName)
%smoothStretchIntensities smoothes and stretches the intensity results
%for each cell with a given filter and into a range from [0, 999]
%% Syntax
%   ySmooth = smoothStretchIntensities( intensResult, FilterOptions, intensType)
%   ySmooth = smoothStretchIntensities(___, plotFlag)
%   ySmooth = smoothStretchIntensities(___, plotFlag, outFigName)
%   [ySmooth, yUpsampled] = smoothStretchIntensities(___)
%   [ySmooth, yUpsampled, objectLabels] = smoothStretchIntensities(___)
%   [ySmooth, yUpsampled, objectLabels, scaling] = smoothStretchIntensities(___)
%
%% Description
%   [ ySmooth, yUpsampled, objectLabels, scaling] =
%   smoothStretchIntensities( intensResult, FilterOptions, intensType,
%   plotFlag, outFigName) interpolates the measured intensity values
%   linearly to a common scale of 1000 elements and then filters them with
%   a given filter (Average, gaussian, lowpass and no filtering are
%   available). Optionally it can save figures illustrating the filtered
%   intensity results.
%
%% Example
%   Load previously generated intensity results
%
%       testData = load('../testData/testData.mat');
%       intRes = testData.intensityResults(1);
%
%   Set parameters
%
%       FilterOptions.type = 'lowpass';
%       FilterOptions.frequencyCutoff = 0.1;
%       intensType = 'max';
%       plotFlag = 'heatmap';
%       outFigName = 'testResults/heatmaps';
%
%   Interpolate and filer signal
%
%       [ySm, yUp] = smoothStretchIntensities(intRes, FilterOptions, intensType, plotFlag, outFigName);
%
%   Plot results of smoothed and non-smoothed intensity value
%
%       subplot(2,1,1);
%       plot(ySm(5,:));
%       title('Smoothed intensity values')
%       ylabel('Intensity values')
%       subplot(2,1,2);
%       plot(yUp(5,:));
%       title('Upsampled non-smoothed intensity values');
%       ylabel('Intensity values')
%       xlabel('Normalized length of cells');
%   
%% Input
%   intensResults -- result structure from computeBorderIntensities
%       scalar struct
%       Scalar result structure containing the results from
%       computeBorderIntensities of one image: border intensities and
%       metadata of objects. Necessary field for execution is
%       borderIntensity with either subfield maxIntensity or meanIntensity
%       depending on the chosen intensType. If plots are requested the
%       field signalImageName must be also supplied.
%       Note: Only a scalar entity of the structure is passed.
%
%   FilterOptions -- parameter struct for filtering
%       scalar struct
%       Structure with the settings for filtering the intensity values.
%       Commong field is:
%       'type': Type of filter: 'average', 'gaussian', 'lowpass' or 'none'
%       Additional fields depend on the filter type:
%       For average filtering:
%       'length': Filter length in integer
%       For gaussian filtering:
%       'length': Filter length in integer
%       'sigma': Standard deviation of the gaussian (Optional defaults to 0.5)
%       For lowpass filtering:
%       'filterOrder': Order of the filter (Optional defaults to 300)
%       'frequencyCutoff': Cutoff frequency (Optional defaults to 0.5)
%
%   intensType -- Type of intensity
%       character vector
%       String identifying which type of intensity should be used, either
%       'max' or 'mean'.
%   
%   plotFlag -- Type of plot
%       'none' (default) | character vector
%       String identifying whether and which type of plot should be
%       generated. Possible values are 'XY' for an X-Y plot of smoothed
%       intensity values, 'heatmap' for a heatmap like plot or 'none' (the
%       default) for no plots.
%
%   outFigName -- filepath to saving location with base name
%       [] (default) | character vector
%       String containing the basic path information for saving plots. If
%       left empty plots are generated but not saved or closed.
%
%% Output
%   ySmooth -- smoothed intensity values
%       Mx1000 double matrix
%       A Mx1000 matrix wih the upsampled and smoothed intensity values for
%       M objects of tbe current image.
%
%   yUpsampled -- non-smoothed, interpolated intensity values
%       Mx1000 double matrix
%       A Mx1000 matrix with upsampled but unsmoothed intensity values for
%       M objects of the current image.
%   
%   objectLabels --  origininal labels of objects
%       Mx1 double vector
%       A Mx1 vector with the indices of the objects for which a intensity
%       profile was measured.
%
%   scaling -- scaling factors of cell boundaries
%       Mx1 double vector
%       scaling is a Mx1 vector with the scaling factors between actual
%       size and common size.
%
%% See Also
%   computeSpotResults, filtfilt, conv, fir1, fspecial

% Copyright (C) David Dreher 2017

% MAXSIZE is the length of the smoothed intensity vectors
MAXSIZE = 1000;


narginchk(3,5);
validateattributes(intensResult, {'struct'}, {'nonempty', 'numel', 1}, ...
    mfilename, 'Intensity result structure', 1);
validateattributes(FilterOptions, {'struct'}, {'nonempty'}, mfilename, ...
    'FilterOptions structure', 2);
validateattributes(intensType, {'string', 'char'}, {'nonempty'}, ...
    mfilename, 'Intensity type', 3);

if nargin < 4 || isempty(plotFlag)
    plotFlag = 'none';
    outFigName = '';
elseif nargin == 4 
    outFigName = '';
end
validateattributes(plotFlag, {'string', 'char'}, {'nonempty'}, mfilename, ...
    'Plot intensities flag', 4);
validateattributes(outFigName, {'string', 'char'}, {}, mfilename, ...
    'Plot filepath', 5);


% Determine which filter should be used and get the corresponding
% parameters to generate the filter using FSPECIAL.
if strcmpi(FilterOptions.type, 'gaussian')
    filtLength = FilterOptions.length;
    if isfield(FilterOptions, 'sigma')
        filtSigma = FilterOptions.sigma;
        filter = fspecial('gaussian', [filtLength 1], filtSigma);
    else
        filter = fspecial('gaussian', [filtLength 1]);
    end
    filterCase = 1;
elseif strcmpi(FilterOptions.type, 'average')
    filtLength = FilterOptions.length;
    filter = fspecial('average', [filtLength 1]);
    filterCase = 1;
elseif strcmpi(FilterOptions.type, 'none') || isempty(FilterOptions.type)
    %filtLength = 1;
    %filter = 1;
    filterCase = 0;
elseif strcmpi(FilterOptions.type, 'lowpass')
    filterOrder = 300; %floor(noFrames/3.5);
    if isfield(FilterOptions, 'filterOrder')
        filterOrder = FilterOptions.filterOrder;
    end
    % Wn = (2/samplingFreq)*cutoffFreq;
    if isfield(FilterOptions, 'frequencyCutoff')
        Wn = FilterOptions.frequencyCutoff;
    else
        Wn = 0.05;
    end
    filter = fir1(filterOrder, Wn);
    filterCase = 2;
elseif strcmpi(FilterOptions.type, 'Savitzky-Golay') || strcmpi(FilterOptions.type, 'sgolay')
    if isfield(FilterOptions, 'filterOrder')
        filterOrder = FilterOptions.filterOrder;
    else
        filterOrder = 3;
    end
    % Wn = (2/samplingFreq)*cutoffFreq;
    if isfield(FilterOptions, 'framelen')
        framelen = FilterOptions.framelen;
    else
        framelen = 51;
    end
    filterCase = 3;
elseif strcmpi(FilterOptions.type, 'SWT')
    if isfield(FilterOptions, 'level')
        level = FilterOptions.level;
    else
        level = 5;
    end
    
    if isfield(FilterOptions, 'waveletType')
        waveletType = FilterOptions.waveletType;
    else
        waveletType = 'db1';
    end
    
    if isfield(FilterOptions, 'thresholds')
        thresholds = FilterOptions.thresholds;
        if ischar(thresholds) && strcmpi(thresholds, 'auto')
            thresholds = [];
            autoThrsh = true;
            FilterOptions = rmfield(FilterOptions, 'autoThrsh');
        else
            validateattributes(thresholds, {'numeric'}, ...
                {'real', 'nonnegative', 'nonnan', 'numel', level}, mfilename, ...
                'FilterOptions.thresholds', 2);
        end
    else
        thresholds = [inf(1, level-2), zeros(1,2)];
    end
    
    if isfield(FilterOptions, 'thrshType')
        thrshType = FilterOptions.thrshType;
    else
        thrshType = 'h';
    end
    
    if isfield(FilterOptions, 'autoThrsh')
        autoThrsh = FilterOptions.autoThrsh;
    elseif ~isempty(thresholds)
        autoThrsh = false;
    end
    
    if isempty(thresholds) && isfield(FilterOptions, 'autoThrshMethod')
        autoThrshMethod = FilterOptions.autoThrshMethod;
    else
        autoThrshMethod = 'sqtwolog';
    end
    
    if isempty(thresholds) && isfield(FilterOptions, 'autoThrshAlfa')
        autoThrshAlfa = FilterOptions.autoThrshAlfa;
    else
        autoThrshAlfa = 'sln';
    end
    
    %TODO hard or soft
    filterCase = 4;
else
    error([mfilename, ':FilterOptions.type'], 'Unknown type of filter given as input')
end

% Determine how many cells we have to iterate over
iter = length(intensResult.borderIntensity);

% Pre-allocate cell array for intensity vectors
intensInput = cell(iter,1);

% Get the specified intensity results
if strcmpi(intensType, 'max')
    [intensInput{:,1}] = intensResult.borderIntensity.maxIntensities;
elseif strcmpi(intensType, 'mean')
    [intensInput{:,1}] = intensResult.borderIntensity.meanIntensities;
else
    error([mfilename, ':type_intensity_unknown'], 'Unknown type of intensity given as input')
end



% Pre-allocate output matrix
ySmooth = zeros(iter, MAXSIZE);
yUpsampled = zeros(iter, MAXSIZE);
scaling = nan(iter,1);
% FOR each cell
for k = 1:1:iter
    % IF we have an intensity result
    if ~isempty(intensInput{k,1})
        % Get the current cell intensities
        y = intensInput{k,1};
        xq = linspace(1, length(y), MAXSIZE);
        yUpsampled(k,:) = interp1(y, xq, 'linear');
        scaling(k) = numel(y) / MAXSIZE;
        % Pad the array in circular fashion to allow for proper smoothing
        % behavior. This preserves behavior at the edges since the membrane
        % is circular in its intensity values. After padding smooth the
        % intensity values with the moving filter.
        switch  filterCase
            case 0 % No filtering
                yFiltered = yUpsampled(k,:);
            case 1 % Convolution
                yFiltered = padarray(colVect(yUpsampled(k,:)), filtLength, 'circular');
                yFiltered = conv(yFiltered, filter, 'same');
                % Remove padding
                yFiltered = yFiltered(filtLength+1:end-filtLength);
            case 2 % Zero phase frequency domain filtering
                padLength = round(filterOrder * 1.1);
                yFiltered = padarray(colVect(yUpsampled(k,:)), padLength, 'circular');
                yFiltered = filtfilt(filter, 1, yFiltered);
                % Remove padding
                yFiltered = yFiltered(padLength+1:end-padLength);
            case 3 %sgolayfilt
                yFiltered = padarray(colVect(yUpsampled(k,:)), framelen, 'circular');
                yFiltered = sgolayfilt(yFiltered, filterOrder, framelen);
                % Remove padding
                yFiltered = yFiltered(framelen+1:end-framelen);
            case 4 % SWT denoising
                nElem = numel(xq);
                nMult = 2^level;
                % The input size must be correspondend to a multiple of 2
                % to the power of level.
                padPre = ceil(nElem/2);
                padPost = ceil(nElem/2) + nMult - rem( (2*padPre+nElem), nMult);
                yFiltered = colVect(yUpsampled(k,:));
                % Circular padding with two different padding sizes
                yFiltered = [yFiltered(end-padPre+1:end); yFiltered; yFiltered(1:padPost)]; %#ok<AGROW>
                swc = swt(yFiltered, level, waveletType);
                swcnew = swc;
                if autoThrsh
                    thresh = wthrmngr('sw1ddenoLVL',autoThrshMethod,swc,autoThrshAlfa);
                else
                    thresh = thresholds;
                end                
                for jj = 1:level
                    swcnew(jj,:) = wthresh(swc(jj,:), thrshType, thresh(jj));
                end
                yFiltered = iswt(swcnew, waveletType);
                yFiltered = yFiltered(padPre+1:end-padPost);
        end

        ySmooth(k,:) = yFiltered;       
    end
end

% Pre-allocate OBJECT_LABELS vector with full size and and numbers from 1
% to maximum number of objects
objectLabels = colVect(1:size(ySmooth,1));
% Get the indices for which no border intensity could be found, i.e. for
% which each element in a row is equal to zero.
idxZeroElem = all(ySmooth==0,2);
% Delete these rows from both, the intensity value matrix and the object
% label vector
ySmooth(idxZeroElem, :) = [];
yUpsampled(idxZeroElem, :) = [];
objectLabels(idxZeroElem) = [];

% IF a plot is requested, allocate figure, set title.
if strcmpi('xy', plotFlag)
    figure;
    [~, filename, ext] = fileparts(intensResult.signalImageName);
    subplot('Position', [0.1, 0.55, 0.8, 0.35]);
    plot(ySmooth');
    title('Smoothed intensity values')
    subplot('Position', [0.1, 0.1, 0.8, 0.35]);
    plot(yUpsampled');
    title('Upsampled non-smoothed intensity values');
    xlabel('Normalized length of cells');

    
    ha = axes('Position',[0.08 0 0.84 1],'Xlim',[0 1],'Ylim',[0 1],'Box','off', ...
        'Visible','off','Units','normalized', 'clipping' , 'off');
    ylabel('Intensity value', 'visible', 'on')
    mainTitle = ['Signal file: ' filename ext ];
    text(0.5, 1, mainTitle, 'HorizontalAlignment' , 'center', ...
        'VerticalAlignment', 'top', 'Interpreter', 'none', 'FontWeight', ...
        'bold', 'FontSize', 12)
    drawnow;
    
    if ~isempty(outFigName)
        print(outFigName, '-dpng');
        close(gcf);
    end
    
elseif strcmpi('heatmap', plotFlag)
    figure;
    [~, filename, ext] = fileparts(intensResult.signalImageName);
    subplot('Position', [0.1, 0.55, 0.75, 0.35]);
    colormap gray;
    imagesc(ySmooth, [0, max(ySmooth(:))]);
    title('Smoothed intensity values')
    subplot('Position', [0.1, 0.1, 0.75, 0.35]);
    colormap gray;
    imagesc(yUpsampled,  [0, max(ySmooth(:))]);
    title('Upsampled non-smoothed intensity values');
    xlabel('Normalized length of cells');

    
    ha = axes('Position', [0.08 0 0.84 1],'Xlim',[0 1],'Ylim',[0 1],'Box','off', ...
        'Visible','off','Units','normalized', 'clipping' , 'off');
    ylabel('Cell label', 'visible', 'on')

    colormap gray;
    mainTitle = ['Signal file: ' filename ext ];
    text(0.5, 1, mainTitle, 'HorizontalAlignment' , 'center', ...
        'VerticalAlignment', 'top', 'Interpreter', 'none', 'FontWeight', ...
        'bold', 'FontSize', 12)
    caxis([0, max(ySmooth(:))]);
    colorbar('position',[0.9 0.05 0.025 0.90]);
    drawnow;
    
    if ~isempty(outFigName)
        print(outFigName, '-dpng');
        close(gcf);
    end
    
elseif strcmpi('none', plotFlag)

else
    error([mfilename, ':plot_intensities_unknown'], 'Unknown string identifier for plotting') 
end

end

